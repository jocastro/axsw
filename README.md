# Approximate Software for Accurate Hardware

This repository contains the work-in-progress developed as part of the OPRECOMP *Summer of Code* 2019 initiative.

### Motivation

With the need to keep increasing the performance in computing systems, and the raise of physical challenges to achieve it, has motivated the comming forth of new design paradigms and computer architectures. *Approximate* and *Transprecision Computing* have emerged as novel design paradigms suitable to applications with inherent error resilience.

By accepting *good enough* results caused by imprecise calculations, e.g., in image and video processing where the human perception plays a major role, the computational *quality* (accuracy of results) is traded-off to reduce the required computational *effort* (execution time, area, power, or energy) as techniques developed under these paradigms are applied.

Besides many appraches have proposed the adoption of inexact or precision-reduced hardware to exploit this error tolerance, many exact processors can profit performace improvement for these type of applications by executing approximate versions of them. 

This project proposes a compiler-supported methodology is to generate approximate software for exact hardware. The following image illustrate this idea.

<center><img src='./imgs/overview.png'></center>
For a given program, the idea is to generate an approximate executable that satisfies a defined quality metric. For this it is also required to provide dynamic infomation of the applications and test input data for quality estimations.

Currently, individual approximate techniques are beeing developed to later integrate and define a methodology to use techniques implemented. The following picture depicts how this techniques are implemented.

<center><img src='./imgs/technique.png'></center>
As depicted, the code of the program is converted into intermediate representation (IR) using LLVM. Then, an approximate computing software technique is applied at the IR level to produce an approximate version.  Later, the compiler backend is used to generate the executable.



### Content

The following describes the content of this repository.

* `presentation`: contains the slides of the presentation given during the *NiPS Summer School* 2019, in Perugia, Italy [Link](https://www.nipslab.org/architectures-and-algorithms-for-energy-efficient-iot-and-hpc-applications-2019/). It presents preliminary results of the work developed during the *Summer of Code* initiative.
* `llvm-gcc-sifive`: for this project, LLVM is used to implement the approximate computing techniques at software level. Currently, the HiFive1 Rev B, from SiFive, is used as a testing platform. However, the LLVM-based toolchain for RISC-V does not properly works for this platform. For this, a modified flow has been added using the SiFive provided framework. This modification allow to generate LLVM IR from the code, and once tha technique is applied, to generate the assembly representation of the code, using the LLVM backend. Then, to produce a compatible executable, the gcc-based backend is used.
* `sifive`: contains a set of ready-to-try examples. The code of two image processing filters, Gaussian and Sobel, are ready to test on the HiFive1 Rev B board. These examples test the variable substitution and the loop perforation techniques. The code has been instrumented with `clock()` function, from the `time.h` library, to estimate the execution time, and hence estimate the improvement due to approximations. Follow instructions from the SiFive framework to compile these examples and test them in the HiFive1 Rev B board.
* `opencv`: similar to the ready-to-test sifive examples, these examples have been prepared using OpenCV, particularly to estimate the impact of the approximate techniques in the quality of the resulting images. PSNR and SSIM image metrics are generated, as well as the exact and approximate processed images. The folder with a set of well-known testing images is also provided, so quality estimations can be performed. Also, a CSV file is provided with the error distribution (as a probabilistic mass function, PMF) of each approximation case.
* `techniques`: here, the current under development approximate techniques are found. A high level description of each tehnique is provided.