## OpenCV examples

Examples for Gaussian and Sobel filters applying loop perforation and variable subsitution approximate techniques.

These examples have been prepared using OpenCV to estimate the impact of the approximate techniques in the quality of the resulting images. PSNR and SSIM image metrics are generated, as well as the exact and approximate processed images. Also, a CSV file is provided with the error distribution (as a probabilistic mass function, PMF) of each approximation case.

 The `img` folder provides a set of well-known testing images to be used with the examples.

