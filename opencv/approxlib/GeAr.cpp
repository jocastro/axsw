/**
 * \file GeAr.cpp
 * \author Jorge Castro-Godinez <jorge.castro-godinez@kit.edu>
 * Chair for Embedded Systems (CES)
 * Karlsruhe Institute of Technology (KIT), Germany
 * Prof. Dr. Joerg Henkel
 *
 * \brief Implementation of the Generic Accuracy Configurable (GeAr) adder.
 * Reference: Muhammad Shafique, Waqas Ahmad, Rehan Hafiz, and Jörg Henkel.2015.
 * A low latency generic accuracy configurable adder. In Proceedings of the 52nd
 * Annual Design Automation Conference (DAC '15). ACM, New York, NY, USA.
 *
 */

#include "approxLibrary.h"

int GeAr(int inA, int inB, int N, int R, int P) {
  int k = 0;
  
  //For char level operation
  char* a = new char[N] ();
  char* b = new char[N] ();
  char* sum = new char[N+1] ();
  
  //For generation and propagation bits
  char* g = new char[N] ();
  char* p = new char[N] ();
  char* sumAux = new char[N] ();
  
  // Input carry-in considered as 0, array defined all as 0 by default
  char* carry = new char[N] ();
  
  // Conversion of input numbers into arrays of bits
  int2bit(inA, a, N);
  int2bit(inB, b, N);
  
  // k, requiered number of sub-adders
  // P, number of prediction bits
  // R, number of sum bits for each sub-adder
  // L=R+P, bit width of each sub-adder
  k = ((N - (R+P))/R) + 1;

  // Calculation of generation and propagation signals
  for (int i = 0; i < N; i++) {
    g[i] = a[i] & b[i];
    p[i] = a[i] ^ b[i];
  }
  
  // Sub-addder 0 (first sub-adder), all bits are part of sum
  for (int i = 0; i < (R+P); i++) {
    // carry array calculation
    carry[i+1] = g[i] | (p[i] & carry[i]);
    // sum calculation
    sum[i] = p[i] ^ carry[i];
  }

	// Sub-adders from 1 to k-1
  for (int i = 1; i < k; i++) {
    carry[R*i] = 0;
    for (int j= (R*i); j < (R*(i+1)+P); j++) {
      // carry array calculation
      carry[j+1] = g[j] | (p[j] & carry[j]);
      // sum calculation
      sumAux[j] = p[j] ^ carry[j];
    }
    
    // Copy the R bits to sum
    for (int j = ((R*i)+P); j < ((R*(i+1))+P); j++) {
    	sum[j] = sumAux[j];
    }
  }
  
  sum[N] = carry[N];
  
  return bit2int(sum, N+1);
}
