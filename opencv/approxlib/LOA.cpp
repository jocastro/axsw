/**
 * \file LOA.cpp
 * \author Jorge Castro-Godinez <jorge.castro-godinez@kit.edu>
 * Chair for Embedded Systems (CES)
 * Karlsruhe Institute of Technology (KIT), Germany
 * Prof. Dr. Joerg Henkel
 *
 * \brief Implementation of the Lower-Part-OR Adder (LOA).
 * Reference: H.R. Mahdiani, A. Ahmadi, S.M. Fakhraie, and C. Lucas,
 * “Bio-Inspired Imprecise Computational Blocks for Efficient VLSI
 * Implementation of Soft-Computing Applications,” IEEE Trans. Circuits and
 * Systems I: Regular Papers, vol. 57, no. 4, pp. 850-862,
 * Apr. 2010.
 *
 */

 #include "approxLibrary.h"

int LOA(int inA, int inB, int N, int k) {

  //For char level operation
  char* a = new char[N] ();
  char* b = new char[N] ();
  char* sum = new char[N+1] ();
  
  char* carry = new char[N+1] ();

  // Conversion of input numbers into arrays of bits
  int2bit(inA, a, N);
  int2bit(inB, b, N);

	// Bit-wise OR operations
	for (int i = 0; i < k; i++) {
		sum[i] = a[i] | b[i];
	}
	carry[k] = a[k-1] & b[k-1];

	// Precise sub-adder, using RCA
	for (int i = k; i < N; i++){
		sum[i] = a[i] ^ b[i] ^ carry[i];
		carry[i+1] = (a[i] & b[i]) | (carry[i] & (a[i] ^ b[i]));
	}

	sum[N] = carry[N];
  
  return bit2int(sum, N+1);
}
