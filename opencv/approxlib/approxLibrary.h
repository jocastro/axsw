/**
 * \file approxLibrary.h
 * \author Jorge Castro-Godinez <jorge.castro-godinez@kit.edu>
 * Chair for Embedded Systems (CES)
 * Karlsruhe Institute of Technology (KIT), Germany
 * Prof. Dr. Joerg Henkel
 *
 * \brief Library of approximate adders
 * State-of-the-Art and other approximate adders implemented
 *
 */

#ifndef _APPROXLIBRARY_H_
#define _APPROXLIBRARY_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "approxUtilities.h"


/**
 * Approximate Mirror Adders (AMA) based on Ripple Carry Adders (RCA). Purdue
 * University
 *
 * @param   approxBits represent the amount of approximate FA starting from LSB
 */
//char AMA1(char a[], char b[], char sum[], int approxBits);
//char AMA2(char a[], char b[], char sum[], int approxBits);
//char AMA3(char a[], char b[], char sum[], int approxBits);
//char AMA4(char a[], char b[], char sum[], int approxBits);
//char AMA5(char a[], char b[], char sum[], int approxBits);


/**
 * Approximate XOR/XNOR-based Adders (AXA). University of Alberta, Canada
 *
 * @param   approxBits represent the amount of approximate LSB for the adder
 */
//char AXA1(char a[], char b[], char sum[], int approxBits);
//char AXA2(char a[], char b[], char sum[], int approxBits);
//char AXA3(char a[], char b[], char sum[], int approxBits);


/**
 * Error-Tolerant-Adders. Nanyang Technological University, Singapore
 *
 * @param   approxBits represent the amount of approximate FA starting from LSB
 * @param   M is the amount of blocks, M >= 2 and M < N.
 *          M should produce a integer amount of bits per block, due it should
 *          be a power of 2
 */
//char ETA1(char a[], char b[], char sum[], int approxBits);
//char ETA2(char a[], char b[], char sum[], int M);
//char ETA2M(char a[], char b[], char sum[], int M);
//char ETA3(char a[], char b[], char sum[], int M);
//char ETA4(char a[], char b[], char sum[], int M);


/**
 * Almost Correct Adder (ACA), here ACA_I. École polytechnique fédérale de
 * Lausanne
 *
 * @param   k is number of bits per block
 */
//char ACA1(char a[], char b[], char sum[], int k);


/**
 * Accuracy Configurable Adder (ACA), here ACA_II. University of California, San
 * Diego
 *
 * @param   k is number of bits per block
 */
//char ACA2(char a[], char b[], char sum[], int k);


/**
 * Generic Accuracy Configurable (GeAr) adder. Karlsruhe Institute of Technology
 * (KIT), Germany
 *
 * @param   R is the resultant bits
 * @param   P is the previous or prediction bits
 */
int GeAr(int inA, int inB, int N , int R, int P);


/**
 * Lower-Part-OR Adder (LOA). University of Tehran, Iran
 *
 * @param   k is the amount of bits for the bit-wise OR "addition"
 */
int LOA(int inA, int inB, int N, int k);


/**
 * Gracefully-degrading adder (GDA). Chinese University of Honk Kong
 *
 * @param   b is the amount of sub-adder or adder blocks
 * @param   p is the amount of carry propagation blocks used
 */
//char GDA(char a[], char b[], char sum[], int blocks, int p);


/**
 * Speculative Carry Select Addition (SCSA). Rice University
 *
 * @param   k is the amount of bits per sub adder block
 */
//char SCSA(char a[], char b[], char sum[], int k);


/**
 * Equal Segmentation Adder (ESA). Purdue University
 *
 * @param   k is bit size of the sub-adders
 */
//char ESA(char a[], char b[], char sum[], int k);


/**
* Custom made modifications to LSB in RCA
*
* @param   approxBits represent the amount of approximate FA starting from LSB
*/
//char CES1(char a[], char b[], char sum[], int approxBits);
//char CES2(char a[], char b[], char sum[], int approxBits);


/**
* Custom made multiplication with iterative additions using GeAr
*
* @param   R is the resultant bits
* @param   P is the previous or prediction bits
*/
//char multGeAr(char a[], char b[], char sum[], int R, int P);


/**
* Multiplier based  on Low-Power, High Performance Approximate multiplier with
* Configurable Partial Error Recovery, proposed by Liu et al.
*
*/
//char multLiu(char a[], char b[], char sum[]);


// To ADD
/*
 * LUA
 * CSAA
 */

//char CSAA(char a[], char b[], char sum[], int k);


#endif /* APPROXLIBRARY_H_ */
