	/**
 * \file approxUtilities.cpp
 * \author Jorge Castro-Godinez <jorge.castro-godinez@kit.edu>
 * Chair for Embedded Systems (CES)
 * Karlsruhe Institute of Technology (KIT), Germany
 * Prof. Dr. Joerg Henkel
 *
 * \brief Funcitions to convert integer values to bit representations and
 * vice versa
 *
 */

#include "approxUtilities.h"

// Convert an integer value to a bit representation
void int2bit(unsigned int number, char bits[], int width) {
  for (int i = width-1; i >= 0; i--) {
    if (number & (1 << i)) {
      bits[i] = 1;
    }
    else {
      bits[i] = 0;
    }
  }
}


// Invert the values of the bit representation
void notArray(char array[], int width) {
  for (int i = 0; i < width; i++) {
    (array[i] == 1) ? (array[i] = 0) : (array[i] = 1);
  }
}


// Invert a bit value
char notBit(char data) {
  if (data == 1) {
    return 0;
  } else {
    return 1;
  }
}


// Convert a bit representation of a number into a integer
int bit2int(char array[], int width) {
  int number = 0;
  
  for (int i = 0; i < width; i++) {
    number = number + array[i]*pow(2,i);
  }
  return number;
}


// Convert a signed bit representation of a number into a integer
int sbit2int(char array[], int width) {
  int number = 0;
  
  notArray(array, width);
  
  for (int i=0; i<width+1; i++) {
    number = number + array[i]*pow(2,i);
  }
  
  return -(number+1);
}


// Shift left an specific amount of bits
void shiftLeft(char in[], int shiftBits, int width) {
  for (int i = (width-1); i >= shiftBits; i--) {
    in[i] = in[i-shiftBits];
    in[i-shiftBits] = 0;
  }
}


// Apply bitwise OR operation to 2 arrays
void arrayOR(char a[], char b[], char orArray[], int width) {
  for (int i = 0; i < width; i++) {
    orArray[i] = a[i] | b[i];
  }
}


// Apply bitwise AND operation to 2 arrays
void arrayAND(char a[], char b[], char andArray[], int width) {
  for (int i = 0; i < width; i++) {
    andArray[i] = a[i] & b[i];
  }
}


// Power of two a value
int isPowerOfTwo (unsigned int x) {
	while (((x & 1) == 0) && x > 1) /* While x is even and > 1 */
		x >>= 1;
	return (x == 1);
}
