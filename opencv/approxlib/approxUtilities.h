/**
 * \file approxUtilities.h
 * \author Jorge Castro-Godinez <jorge.castro-godinez@kit.edu>
 * Chair for Embedded Systems (CES)
 * Karlsruhe Institute of Technology (KIT), Germany
 * Prof. Dr. Joerg Henkel
 *
 * \brief Funcitions to convert integer values to bit representations and 
 * vice versa
 *
 */

#ifndef _APPROXUTILITIES_H_
#define _APPROXUTILITIES_H_

#include <math.h>

#ifndef max
#define max(a, b) (((a) > (b)) ? (a) : (b))
#endif


/**
 * Convert an integer number to a binary representation (char array of 1's and
 * 0's) of WIDTH bits.
 * @see http://en.wikipedia.org/wiki/Bitwise_operations_in_C
 */
void int2bit(unsigned int number, char bits[], int WIDTH);


/**
 * Invert the values of the bit representation
 */
void notArray(char array[], int WIDTH);


/**
 * Invert a bit value
 */
char notBit(char data);


/**
 * Convert a binary representation of a number of WIDTH bits (char array of 1's
 * and 0's) into a integer number.
 */
int bit2int(char bits[], int WIDTH);


/**
 * Convert a signed binary representation of a number of WIDTH bits (char array
 * of 1's and 0's) into a integer number.
 * @see http://www.ugrad.cs.ubc.ca/~cs121/2009W1/Handouts/signed-binary
 * -decimal-conversions.html
 */
int sbit2int(char array[], int WIDTH);


/**
 * Shift left an specific amount of bits
 */
void shiftLeft(char in[], int shiftBits, int WIDTH);


/**
 * OR AND operations to arrays
 */
void arrayOR(char a[], char b[], char orArray[], int width);
void arrayAND(char a[], char b[], char andArray[], int width);


/**
 * http://www.exploringbinary.com/ten-ways-to-check-if-an-integer-is
 * a-power-of-two-in-c/
 */
int isPowerOfTwo (unsigned int x);


#endif /* _APPROXUTILITIES_H_ */
