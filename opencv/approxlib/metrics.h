#ifndef _METRICS_H
#define _METRICS_H

#include <sstream>                    // String to number conversion

#include <opencv2/core/core.hpp>      // Basic OpenCV structures
#include <opencv2/imgproc/imgproc.hpp>// Image processing methods for the CPU
#include <opencv2/highgui/highgui.hpp>// Read images

double getPSNR(const cv::Mat& I1, const cv::Mat& I2);

cv::Scalar getMSSIM(const cv::Mat& i1, const cv::Mat& i2);

#endif /* _METRICS_H */