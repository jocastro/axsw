#include <stdio.h>
#include <iostream>
#include <fstream>
#include <opencv2/opencv.hpp>

#include "../approxlib/approxLibrary.h"
#include "../approxlib/metrics.h"

#define	ABS(A)	((A)<(0) ? (-(A)):(A)) //ABS macro, faster than procedure

std::map<int,int> pmf;
std::map<int,int>::iterator it;

//Exact
void gaussian(int* out, int a, int b, int c, int d, int e, int f, int g, int h, 
    int i) {

	int b2 = b * 2; //8 -> 9
	int d2 = d * 2; //8 -> 9
  int e4 = e * 4; //8 -> 10
	int f2 = f * 2; //8 -> 9
	int h2 = h * 2; //8 -> 9

	int s1 = a + b2; //9 -> 10
	int s2 = c + d2; //9 -> 10
	int s3 = e4 + f2; //10 -> 11
	int s4 = g + h2; //9 -> 10

	int s5 = s1 + s2; //10 -> 11
	int s6 = s3 + s4; //11 -> 12

	int s7 = s5 + s6; // 12 -> 13

	int s8 = s7 + i; //13 -> 14

	int res = s8 >> 4;

  if (res < 0) res = 0;
  if (res > 255) res = 255;
  
  *out = res;
}

//Approximate 1, d = b
void gaussianSW1(int* out, int a, int b, int c, int e, int f, int g, int 
    h, int i) {

	int b2 = b * 2; //8 -> 9
  int e4 = e * 4; //8 -> 10
	int f2 = f * 2; //8 -> 9
	int h2 = h * 2; //8 -> 9

	int s1 = a + b2; //9 -> 10
	int s2 = c + b2; //9 -> 10
	int s3 = e4 + f2; //10 -> 11
	int s4 = g + h2; //9 -> 10

	int s5 = s1 + s2; //10 -> 11
	int s6 = s3 + s4; //11 -> 12

	int s7 = s5 + s6; // 12 -> 13

	int s8 = s7 + i; //13 -> 14

	int res = s8 >> 4;

  if (res < 0) res = 0;
  if (res > 255) res = 255;
  
  *out = res;
}

//Approximate 2, d = b, no i
void gaussianSW2(int* out, int a, int b, int c, int e, int f, int g, int h) {

	int b2 = b * 2; //8 -> 9
  int e4 = e * 4; //8 -> 10
	int f2 = f * 2; //8 -> 9
	int h2 = h * 2; //8 -> 9

	int s1 = a + b2; //9 -> 10
	int s2 = c + b2; //9 -> 10
	int s3 = e4 + f2; //10 -> 11
	int s4 = g + h2; //9 -> 10

	int s5 = s1 + s2; //10 -> 11
	int s6 = s3 + s4; //11 -> 12

	int s7 = s5 + s6; // 12 -> 13

	int res = s7 >> 4;

  if (res < 0) res = 0;
  if (res > 255) res = 255;
  
  *out = res;
}

//Approximate 3, d = b, no i, h = f
void gaussianSW3(int* out, int a, int b, int c, int e, int f, int g) {

	int b2 = b * 2; //8 -> 9
  int e4 = e * 4; //8 -> 10
	int f2 = f * 2; //8 -> 9

	int s1 = a + b2; //9 -> 10
	int s2 = c + b2; //9 -> 10
	int s3 = e4 + f2; //10 -> 11
	int s4 = g + f2; //9 -> 10

	int s5 = s1 + s2; //10 -> 11
	int s6 = s3 + s4; //11 -> 12

	int s7 = s5 + s6; // 12 -> 13

	int res = s7 >> 4;

  if (res < 0) res = 0;
  if (res > 255) res = 255;
  
  *out = res;
}

//Approximate 4, d = b, no i, h = f, e = f
void gaussianSW4(int* out, int a, int b, int c, int f, int g) {

	int b2 = b * 2; //8 -> 9
  int e4 = f * 4; //8 -> 10
	int f2 = f * 2; //8 -> 9

	int s1 = a + b2; //9 -> 10
	int s2 = c + b2; //9 -> 10
	int s3 = e4 + f2; //10 -> 11
	int s4 = g + f2; //9 -> 10

	int s5 = s1 + s2; //10 -> 11
	int s6 = s3 + s4; //11 -> 12

	int s7 = s5 + s6; // 12 -> 13

	int res = s7 >> 4;

  if (res < 0) res = 0;
  if (res > 255) res = 255;
  
  *out = res;
}

//https://bit.ly/2YpIoAE
void add2map(std::map<int,int> &tempMap, int diff) {
  it = tempMap.find(diff);
  if(it != tempMap.end()) {
    it->second++;
  } else {
    tempMap.insert(std::make_pair(diff, 1));
  }
}

void map2csv(std::map<int,int> &tempMap, std::string csvName) {
  std::ofstream csvFile;
  csvFile.open(csvName);

  for (std::pair<int,int> element : tempMap) {
    csvFile << element.first << ", " << element.second << "\n";
  }

  csvFile.close();
}


int main( int argc, char** argv ) {
  
  char* imageName = argv[1];
  
  cv::Mat image;
  image = cv::imread( imageName, 1 );  
  
  if( argc != 2 || !image.data  ) {
    printf( " No image data \n " );
    return -1;
  }
  
  //Convert image to grayscale
  cv::Mat grayImage;
  cvtColor( image, grayImage, cv::COLOR_BGR2GRAY );

  imwrite( "grayscale.jpg", grayImage );

  //Clone input image to create the output one fulled with zeros
  cv::Mat imageAcc = cv::Mat::zeros(grayImage.rows, grayImage.cols, CV_8UC1);
  cv::Mat imageApp = cv::Mat::zeros(grayImage.rows, grayImage.cols, CV_8UC1);

  int outApp, outAcc;
  
  for(int j = 1; j < grayImage.rows-1; j++) {
    for(int i = 1; i < grayImage.cols-1; i++) {
      
      gaussian(&outAcc, (int)grayImage.at<uchar>(j-1, i-1),
          (int)grayImage.at<uchar>(j-1, i), (int)grayImage.at<uchar>(j-1, i+1),
          (int)grayImage.at<uchar>(j, i-1), (int)grayImage.at<uchar>(j, i),
          (int)grayImage.at<uchar>(j, i+1), (int)grayImage.at<uchar>(j+1, i-1),
          (int)grayImage.at<uchar>(j+1, i), (int)grayImage.at<uchar>(j+1, i+1));
      
      imageAcc.at<uchar>(j, i) = (unsigned char)outAcc;
    }
  }


  for(int j = 1; j < grayImage.rows-1; j++) {
    for(int i = 1; i < grayImage.cols-1; i=i+2) {
      
      gaussianSW2(&outApp, (int)grayImage.at<uchar>(j-1, i-1),
          (int)grayImage.at<uchar>(j-1, i), (int)grayImage.at<uchar>(j-1, i+1),
          (int)grayImage.at<uchar>(j, i),
          (int)grayImage.at<uchar>(j, i+1), (int)grayImage.at<uchar>(j+1, i-1),
          (int)grayImage.at<uchar>(j+1, i));
      
      imageApp.at<uchar>(j, i) = (unsigned char)outApp;
      imageApp.at<uchar>(j, i+1) = (unsigned char)outApp;
    }
  }

  for(int j = 1; j < grayImage.rows-1; j++) {
    for(int i = 1; i < grayImage.cols-1; i++) {

      outAcc = (int)imageAcc.at<uchar>(j, i);
      outApp = (int)imageApp.at<uchar>(j, i);

      add2map(pmf,ABS(outAcc - outApp));
    }
  }

  map2csv(pmf, "pmf.csv");

  const float ssim = getMSSIM(imageAcc, imageApp)[0];
	const float psnr = getPSNR(imageAcc, imageApp);

	std::cout << "PSNR = " << psnr << " SSIM = " << ssim << std::endl; 

  cv::namedWindow( "Accurate", cv::WINDOW_AUTOSIZE );
  cv::namedWindow( "Approximate", cv::WINDOW_AUTOSIZE );
  
  cv::imshow( "Accurate", imageAcc );
  cv::imshow( "Approximate", imageApp );
  
  cv::waitKey(0);

  //Save image
  imwrite( "gaussianEX.jpg", imageAcc );
  imwrite( "gaussianAX.jpg", imageApp );

  return 0;
}
