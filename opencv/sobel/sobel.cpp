#include <stdio.h>
#include <iostream>
#include <fstream>
#include <map> 
#include <algorithm> 
#include <vector> 
#include <opencv2/opencv.hpp>

#include "../approxlib/approxLibrary.h"
#include "../approxlib/metrics.h"

#define	ABS(A)	((A)<(0) ? (-(A)):(A)) //ABS macro, faster than procedure

std::map<int,int> pmf;
std::map<int,int>::iterator it;

//Exact
void sobel(int* out, int a, int b, int c, int d, int f, int g, int h, int i) {

	int x1 = a + g; //8 -> 9
	int x2 = d * 2; //8 -> 9
	int x3 = x1 + x2; //9 -> 10

	int x4 = c + i; //8 -> 9
	int x5 = f * 2; //8 -> 9
	int x6 = x4 + x5; //9 -> 10

	int gx = x6 - x3; 
	int absGx = abs(gx);

	int y1 = a + c; //8 -> 9
	int y2 = b * 2; //8 -> 9
	int y3 = y1 + y2; //9 -> 10

	int y4 = g + i; //8 -> 9
	int y5 = h * 2; //8 -> 9
	int y6 = y4 + y5; //9 -> 10

	int gy = y3 - y6;
	int absGy = abs(gy);

	int res = absGx + absGy;

  if (res > 255) res = 255;
  
  *out = res;
}

//Approximate1, d = a
void sobelSW1(int* out, int a, int b, int c, int f, int g, int h, int i) 
{

	int x1 = a + g; //8 -> 9
	int x2 = a * 2; //8 -> 9 x2 = d * 2
	int x3 = x1 + x2; //9 -> 10

	int x4 = c + i; //8 -> 9
	int x5 = f * 2; //8 -> 9
	int x6 = x4 + x5; //9 -> 10

	int gx = x6 - x3; 
	int absGx = abs(gx);

	int y1 = a + c; //8 -> 9
	int y2 = b * 2; //8 -> 9
	int y3 = y1 + y2; //9 -> 10

	int y4 = g + i; //8 -> 9
	int y5 = h * 2; //8 -> 9
	int y6 = y4 + y5; //9 -> 10

	int gy = y3 - y6;
	int absGy = abs(gy);

	int res = absGx + absGy;

  if (res > 255) res = 255;
  
  *out = res;
}

//Approximate2, d = a, b = c
void sobelSW2(int* out, int a, int c, int f, int g, int h, int i) 
{

	int x1 = a + g; //8 -> 9
	int x2 = a * 2; //8 -> 9 x2 = d * 2
	int x3 = x1 + x2; //9 -> 10

	int x4 = c + i; //8 -> 9
	int x5 = f * 2; //8 -> 9
	int x6 = x4 + x5; //9 -> 10

	int gx = x6 - x3; 
	int absGx = abs(gx);

	int y1 = a + c; //8 -> 9
	int y2 = c * 2; //8 -> 9 y2 = b * 2
	int y3 = y1 + y2; //9 -> 10

	int y4 = g + i; //8 -> 9
	int y5 = h * 2; //8 -> 9
	int y6 = y4 + y5; //9 -> 10

	int gy = y3 - y6;
	int absGy = abs(gy);

	int res = absGx + absGy;

  if (res > 255) res = 255;
  
  *out = res;
}


//Approximate3, d = a, b = c, f = i
void sobelSW3(int* out, int a, int c, int g, int h, int i) 
{

	int x1 = a + g; //8 -> 9
	int x2 = a * 2; //8 -> 9 x2 = d * 2
	int x3 = x1 + x2; //9 -> 10

	int x4 = c + i; //8 -> 9
	int x5 = i * 2; //8 -> 9 x5 = f * 2
	int x6 = x4 + x5; //9 -> 10

	int gx = x6 - x3; 
	int absGx = abs(gx);

	int y1 = a + c; //8 -> 9
	int y2 = c * 2; //8 -> 9 y2 = b * 2
	int y3 = y1 + y2; //9 -> 10

	int y4 = g + i; //8 -> 9
	int y5 = h * 2; //8 -> 9
	int y6 = y4 + y5; //9 -> 10

	int gy = y3 - y6;
	int absGy = abs(gy);

	int res = absGx + absGy;

  if (res > 255) res = 255;
  
  *out = res;
}


//Approximate4, d = a, b = c, f = i, h = g
void sobelSW4(int* out, int a, int c, int g, int i) 
{

	int x1 = a + g; //8 -> 9
	int x2 = a * 2; //8 -> 9 x2 = d * 2
	int x3 = x1 + x2; //9 -> 10

	int x4 = c + i; //8 -> 9
	int x5 = i * 2; //8 -> 9 x5 = f * 2
	int x6 = x4 + x5; //9 -> 10

	int gx = x6 - x3; 
	int absGx = abs(gx);

	int y1 = a + c; //8 -> 9
	int y2 = c * 2; //8 -> 9 y2 = b * 2
	int y3 = y1 + y2; //9 -> 10

	int y4 = g + i; //8 -> 9
	int y5 = g * 2; //8 -> 9 y5 = h * 2
	int y6 = y4 + y5; //9 -> 10

	int gy = y3 - y6;
	int absGy = abs(gy);

	int res = absGx + absGy;

  if (res > 255) res = 255;
  
  *out = res;
}


//Exact
void gaussian(int* out, int a, int b, int c, int d, int e, int f, int g, int h, 
    int i) {

	int b2 = b * 2; //8 -> 9
	int d2 = d * 2; //8 -> 9
  int e4 = e * 4; //8 -> 10
	int f2 = f * 2; //8 -> 9
	int h2 = h * 2; //8 -> 9

	int s1 = a + b2; //9 -> 10
	int s2 = c + d2; //9 -> 10
	int s3 = e4 + f2; //10 -> 11
	int s4 = g + h2; //9 -> 10

	int s5 = s1 + s2; //10 -> 11
	int s6 = s3 + s4; //11 -> 12

	int s7 = s5 + s6; // 12 -> 13

	int s8 = s7 + i; //13 -> 14

	int res = s8 >> 4;

  if (res < 0) res = 0;
  if (res > 255) res = 255;
  
  *out = res;
}


//https://bit.ly/2YpIoAE
void add2map(std::map<int,int> &tempMap, int diff) {
  it = tempMap.find(diff);
  if(it != tempMap.end()) {
    it->second++;
  } else {
    tempMap.insert(std::make_pair(diff, 1));
  }
}

void map2csv(std::map<int,int> &tempMap, std::string csvName) {
  std::ofstream csvFile;
  csvFile.open(csvName);

  for (std::pair<int,int> element : tempMap) {
    csvFile << element.first << ", " << element.second << "\n";
  }

  csvFile.close();
}


int main( int argc, char** argv ) {
  
  char* imageName = argv[1];
  
  cv::Mat imageColor;
  imageColor = cv::imread( imageName, 1 );
  
  if( argc != 2 || !imageColor.data ) {
    printf( " No image data \n " );
    return -1;
  }
  
  cv::Mat grayImage;
  cvtColor( imageColor, grayImage, CV_BGR2GRAY );

  imwrite( "grayscale.jpg", grayImage );

  //Clone input image to create the output one fulled with zeros
  cv::Mat gaussianImg = cv::Mat::zeros(imageColor.rows, imageColor.cols, CV_8UC1);
  cv::Mat sobelAcc = cv::Mat::zeros(imageColor.rows, imageColor.cols, CV_8UC1);
  cv::Mat sobelApp = cv::Mat::zeros(imageColor.rows, imageColor.cols, CV_8UC1);
  
  int gaussianOut = 0;
  
  for(int j = 1; j < grayImage.rows-1; j++) {
    for(int i = 1; i < grayImage.cols-1; i++) {
      
      gaussian(&gaussianOut, (int)grayImage.at<uchar>(j-1, i-1),
          (int)grayImage.at<uchar>(j-1, i), (int)grayImage.at<uchar>(j-1, i+1),
          (int)grayImage.at<uchar>(j, i-1), (int)grayImage.at<uchar>(j, i),
          (int)grayImage.at<uchar>(j, i+1), (int)grayImage.at<uchar>(j+1, i-1),
          (int)grayImage.at<uchar>(j+1, i), (int)grayImage.at<uchar>(j+1, i+1));
      
      gaussianImg.at<uchar>(j,i) = (unsigned char)gaussianOut;
    }
  }
  
  int outApp, outAcc;

  for(int j = 1; j < gaussianImg.rows-1; j++) {
    for(int i = 1; i < gaussianImg.cols-1; i++) {
      sobel(&outAcc, (int)gaussianImg.at<uchar>(j-1, i-1), 
          (int)gaussianImg.at<uchar>(j-1, i), (int)gaussianImg.at<uchar>(j-1, i+1), 
          (int)gaussianImg.at<uchar>(j, i-1), (int)gaussianImg.at<uchar>(j, i+1), 
          (int)gaussianImg.at<uchar>(j+1, i-1), (int)gaussianImg.at<uchar>(j+1, i), 
          (int)gaussianImg.at<uchar>(j+1, i+1));

/*
      sobelSW1(&outApp, (int)gaussianImg.at<uchar>(j-1, i-1), 
          (int)gaussianImg.at<uchar>(j-1, i), (int)gaussianImg.at<uchar>(j-1, i+1), 
          (int)gaussianImg.at<uchar>(j, i+1), 
          (int)gaussianImg.at<uchar>(j+1, i-1), (int)gaussianImg.at<uchar>(j+1, i), 
          (int)gaussianImg.at<uchar>(j+1, i+1));  
      */

  /*    
      sobelSW2(&outApp, (int)gaussianImg.at<uchar>(j-1, i-1), 
          (int)gaussianImg.at<uchar>(j-1, i+1), 
          (int)gaussianImg.at<uchar>(j, i+1), 
          (int)gaussianImg.at<uchar>(j+1, i-1), (int)gaussianImg.at<uchar>(j+1, i), 
          (int)gaussianImg.at<uchar>(j+1, i+1));
      
*/
/*
      
      sobelSW3(&outApp, (int)gaussianImg.at<uchar>(j-1, i-1), 
          (int)gaussianImg.at<uchar>(j-1, i+1), 
          (int)gaussianImg.at<uchar>(j+1, i-1), (int)gaussianImg.at<uchar>(j+1, i), 
          (int)gaussianImg.at<uchar>(j+1, i+1));
      */

      
      sobelSW4(&outApp, (int)gaussianImg.at<uchar>(j-1, i-1), 
          (int)gaussianImg.at<uchar>(j-1, i+1), 
          (int)gaussianImg.at<uchar>(j+1, i-1), 
          (int)gaussianImg.at<uchar>(j+1, i+1));
          
      
      sobelApp.at<uchar>(j, i) = (unsigned char)outApp;
      sobelAcc.at<uchar>(j, i) = (unsigned char)outAcc;
      
      add2map(pmf,ABS(outAcc - outApp));
    }
  }
  
  map2csv(pmf, "pmf.csv");

  const float ssim = getMSSIM(sobelAcc, sobelApp)[0];
	const float psnr = getPSNR(sobelAcc, sobelApp);

	std::cout << "PSNR = " << psnr << " SSIM = " << ssim << std::endl; 

  cv::namedWindow( "Accurate", CV_WINDOW_AUTOSIZE );
  cv::namedWindow( "Approximate", CV_WINDOW_AUTOSIZE );
  
  cv::imshow( "Accurate", sobelAcc );
  cv::imshow( "Approximate", sobelApp );
  
  cv::waitKey(0);

  //Save image
  cv::imwrite( "sobelEX.jpg", sobelAcc );
  cv::imwrite( "sobelAX.jpg", sobelApp );

  return 0;}
