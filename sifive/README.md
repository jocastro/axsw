## HiFive1 Rev B examples

These examples allow to experiment with th effect of the approximations in the execution time, and hence the speed up achieved due to the approximations in comparison to the accurate implementation.

The code of two image processing filters, Gaussian and Sobel, are ready to test on the HiFive1 Rev B board. These examples test the variable substitution and the loop perforation techniques. These implementations are provided for different number of replaced variables, using loop perforations, and a combination of them. The code has been instrumented with `clock()` function, from the `time.h` library, to estimate the execution time, and hence estimate the improvement due to approximations. 

Please, follow instructions from the SiFive framework to compile these examples and test them in the HiFive1 Rev B board.