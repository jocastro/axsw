/* Copyright 2019 SiFive, Inc */
/* SPDX-License-Identifier: Apache-2.0 */


#include <stdio.h>
#include <time.h>
#include "image.h"

//Exact
void gaussian(int* out, int a, int b, int c, int d, int e, int f, int g, int h, 
    int i) {

	int b2 = b * 2; //8 -> 9
	int d2 = d * 2; //8 -> 9
  int e4 = e * 4; //8 -> 10
	int f2 = f * 2; //8 -> 9
	int h2 = h * 2; //8 -> 9

	int s1 = a + b2; //9 -> 10
	int s2 = c + d2; //9 -> 10
	int s3 = e4 + f2; //10 -> 11
	int s4 = g + h2; //9 -> 10

	int s5 = s1 + s2; //10 -> 11
	int s6 = s3 + s4; //11 -> 12

	int s7 = s5 + s6; // 12 -> 13

	int s8 = s7 + i; //13 -> 14

	int res = s8 >> 4;

  if (res < 0) res = 0;
  if (res > 255) res = 255;
  
  *out = res;
}

//Approximate 1, d = b
void gaussianSW1(int* out, int a, int b, int c, int e, int f, int g, int 
    h, int i) {

	int b2 = b * 2; //8 -> 9
  int e4 = e * 4; //8 -> 10
	int f2 = f * 2; //8 -> 9
	int h2 = h * 2; //8 -> 9

	int s1 = a + b2; //9 -> 10
	int s2 = c + b2; //9 -> 10
	int s3 = e4 + f2; //10 -> 11
	int s4 = g + h2; //9 -> 10

	int s5 = s1 + s2; //10 -> 11
	int s6 = s3 + s4; //11 -> 12

	int s7 = s5 + s6; // 12 -> 13

	int s8 = s7 + i; //13 -> 14

	int res = s8 >> 4;

  if (res < 0) res = 0;
  if (res > 255) res = 255;
  
  *out = res;
}

//Approximate 2, d = b, no i
void gaussianSW2(int* out, int a, int b, int c, int e, int f, int g, int h) {

	int b2 = b * 2; //8 -> 9
  int e4 = e * 4; //8 -> 10
	int f2 = f * 2; //8 -> 9
	int h2 = h * 2; //8 -> 9

	int s1 = a + b2; //9 -> 10
	int s2 = c + b2; //9 -> 10
	int s3 = e4 + f2; //10 -> 11
	int s4 = g + h2; //9 -> 10

	int s5 = s1 + s2; //10 -> 11
	int s6 = s3 + s4; //11 -> 12

	int s7 = s5 + s6; // 12 -> 13

	int res = s7 >> 4;

  if (res < 0) res = 0;
  if (res > 255) res = 255;
  
  *out = res;
}

//Approximate 3, d = b, no i, h = f
void gaussianSW3(int* out, int a, int b, int c, int e, int f, int g) {

	int b2 = b * 2; //8 -> 9
  int e4 = e * 4; //8 -> 10
	int f2 = f * 2; //8 -> 9

	int s1 = a + b2; //9 -> 10
	int s2 = c + b2; //9 -> 10
	int s3 = e4 + f2; //10 -> 11
	int s4 = g + f2; //9 -> 10

	int s5 = s1 + s2; //10 -> 11
	int s6 = s3 + s4; //11 -> 12

	int s7 = s5 + s6; // 12 -> 13

	int res = s7 >> 4;

  if (res < 0) res = 0;
  if (res > 255) res = 255;
  
  *out = res;
}

//Approximate 4, d = b, no i, h = f, e = f
void gaussianSW4(int* out, int a, int b, int c, int f, int g) {

	int b2 = b * 2; //8 -> 9
  int e4 = f * 4; //8 -> 10
	int f2 = f * 2; //8 -> 9

	int s1 = a + b2; //9 -> 10
	int s2 = c + b2; //9 -> 10
	int s3 = e4 + f2; //10 -> 11
	int s4 = g + f2; //9 -> 10

	int s5 = s1 + s2; //10 -> 11
	int s6 = s3 + s4; //11 -> 12

	int s7 = s5 + s6; // 12 -> 13

	int res = s7 >> 4;

  if (res < 0) res = 0;
  if (res > 255) res = 255;
  
  *out = res;
}


int main() {

  int A = 0;
  int B = 0;
  int C = 0;
  int E = 0;
  int F = 0;
  int G = 0;
  int H = 0;
  int i,j;
  int out = 0;

  int RESULT[HEIGHT][WIDTH] = {0};

  clock_t begin = clock();

  for (i = 1; i < HEIGHT-1; i++) {
    for (j = 1; j < WIDTH-1; j++) {
      A = IMAGE[i - 1][j - 1];
      B = IMAGE[i - 1][j];
      C = IMAGE[i - 1][j + 1];
      E = IMAGE[i][j];
      F = IMAGE[i][j + 1];
      G = IMAGE[i + 1][j - 1];
      H = IMAGE[i + 1][j];

      gaussianSW2(&out, A, B, C, E, F, G, H);

      RESULT[i][j] = out;
    }
  }

  clock_t end = clock();

  int clock_cycles = (int)(end - begin);
  printf("clock cycles: %d\n",clock_cycles);

  printf("Gaussian filtering\n");

	return 0;
}

