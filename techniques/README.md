



## Approximate Software Techniques

Each technique under implementation is been developed as a LLVM optimization pass [link](http://llvm.org/docs/WritingAnLLVMPass.html).

### Loop Perforation

The loop perforation technique is implemented as described in [1]. Due it is currently applied to image processing kernels, the inmediate previous result is used for the cases when the loop iteration is skipped. This allows to exploit the correlation between pixel values in the image, and hence use an approximate result each time a loop iteration is skipped.

The following code snippet depicts how this transformation looks like at LLVM IR level (first accurate, then approximate).

Loop index:

```c++
; <label>:110:                                    ; preds = %26
  %111 = load i32, i32* %11, align 4
  %112 = add nsw i32 %111, 1
  store i32 %112, i32* %11, align 4
  br label %23
```

```c++
; <label>:110:                                    ; preds = %26
  %111 = load i32, i32* %11, align 4
  %112 = add nsw i32 %111, 2
  store i32 %112, i32* %11, align 4
  br label %23
```



Store duplication:

```c++
 call void @sobel(i32* %12, i32 signext %95, i32 signext %96, i32 signext %97, i32 signext %98, i32 signext %99, i32 signext %100, i32 signext %101, i32 %102)
  %103 = load i32, i32* %12, align 4
  %104 = load i32, i32* %10, align 4
  %105 = sext i32 %104 to i64
  %106 = getelementptr inbounds [46 x [61 x i32]], [46 x [61 x i32]]* %13, i64 0, i64 %105
  %107 = load i32, i32* %11, align 4
  %108 = sext i32 %107 to i64
  %109 = getelementptr inbounds [61 x i32], [61 x i32]* %106, i64 0, i64 %108
  store i32 %103, i32* %109, align 4
  br label %110
```

```c++
  call void @sobel(i32* %12, i32 signext %95, i32 signext %96, i32 signext %97, i32 signext %98, i32 signext %99, i32 signext %100, i32 signext %101, i32 %102)
  %103 = load i32, i32* %12, align 4
  %104 = load i32, i32* %10, align 4
  %105 = sext i32 %104 to i64
  %106 = getelementptr inbounds [46 x [61 x i32]], [46 x [61 x i32]]* %13, i64 0, i64 %105
  %107 = load i32, i32* %11, align 4
  %108 = sext i32 %107 to i64
  %109 = getelementptr inbounds [61 x i32], [61 x i32]* %106, i64 0, i64 %108
  store i32 %103, i32* %109, align 4
  %110 = load i32, i32* %12, align 4
  %111 = load i32, i32* %10, align 4
  %112 = sext i32 %111 to i64
  %113 = getelementptr inbounds [46 x [61 x i32]], [46 x [61 x i32]]* %13, i64 0, i64 %112
  %114 = load i32, i32* %11, align 4
  %115 = add nsw i32 %114, 1
  %116 = sext i32 %115 to i64
  %117 = getelementptr inbounds [61 x i32], [61 x i32]* %113, i64 0, i64 %116
  store i32 %110, i32* %117, align 4
  br label %118
```



These images show an example of applying this technique for a Sobel filter and the *plate* test image, where every other iteration of the inner loop is skipped. For this case, the quality of the resulting image has a PSNR = 20.52 dB and a SSIM = 0.82.

Exact:

![sobelEX](../imgs/sobelEX.jpg)



Approximate:

![sobelAX](../imgs/sobelAX.jpg)



### Variable Substitution

With this technique, the main idea is to replace variables for other variables used in the function, when such variables present similar data values, as discussed in [2]. Similarly as for loop perforation in the case of image processing kernels, the idea is to exploit the correlation bewteen pixel values that are processed in one kernel iteration. By replacing variables, required memory accesses and instructions can be reduced, and then a reduction in the execution time is achieved. The more variables are substituted, more savings are obtained at a cost of quality degradation. 

The following code snippet depicts how this varible substitution looks like at LLVM IR level. In this case, 2 variables, `d` and `h` have been replaced by `b` and `f`, respectively, and not adding the variable `i`, as the `gaussian` fuction prototype is:

```c++
void gaussian(int* out, int a, int b, int c, int d, int e, int f, int g, int h, int i) {
```

where each letter *a, b, c, ..., h, i*, correspond to value pixels processed by the kernel

```c++
; Function Attrs: norecurse nounwind writeonly
define dso_local void @gaussian(i32* nocapture, i32 signext, i32 signext, i32 signext, i32 signext, i32 signext, i32 signext, i32 signext, i32, i32) local_unnamed_addr #0 {
  %11 = shl i32 %5, 2
  %12 = add i32 %4, %2
  %13 = add i32 %12, %6
  %14 = add i32 %13, %8
  %15 = shl i32 %14, 1
  %16 = add i32 %3, %1
  %17 = add i32 %16, %11
  %18 = add i32 %17, %7
  %19 = add i32 %18, %9
  %20 = add i32 %19, %15
  %21 = ashr i32 %20, 4
  %22 = icmp sgt i32 %21, 0
  %23 = select i1 %22, i32 %21, i32 0
  %24 = icmp slt i32 %23, 255
  %25 = select i1 %24, i32 %23, i32 255
  store i32 %25, i32* %0, align 4, !tbaa !2
  ret void
}
```

<center>Accurate Software</center>

```c++
define dso_local void @gaussian(i32* nocapture, i32 signext, i32 signext, i32 signext, i32 signext, i32 signext, i32 signext, i32 signext, i32, i32) local_unnamed_addr #0 {
  %11 = shl i32 %5, 2
  %12 = add i32 %4, %2
  %15 = shl i32 %12, 2
  %16 = add i32 %3, %1
  %17 = add i32 %16, %11
  %18 = add i32 %17, %7
  %20 = add i32 %18, %15
  %21 = ashr i32 %20, 4
  %22 = icmp sgt i32 %21, 0
  %23 = select i1 %22, i32 %21, i32 0
  %24 = icmp slt i32 %23, 255
  %25 = select i1 %24, i32 %23, i32 255
  store i32 %25, i32* %0, align 4, !tbaa !2
  ret void
}
```

<center>Approximate version for the described variable replacement</center>

The following images show the resulting images for this substitution example. The resulting image has a PSNR = 29.43 dB and a SSIM = 0.98.

Exact:

![gaussianEX](../imgs/gaussianEX.jpg)



Approximate:

![gaussianAX](../imgs/gaussianAX.jpg)



#### References

[1] Stelios Sidiroglou-Douskos, Sasa Misailovic, Henry Hoffmann, and Martin Rinard. 2011. Managing performance vs. accuracy trade-offs with loop perforation. In *Proceedings of the 19th ACM SIGSOFT symposium and the 13th European conference on Foundations of software engineering*(ESEC/FSE '11). ACM, New York, NY, USA, 124-134. DOI=http://dx.doi.org/10.1145/2025113.2025133

[2] Siyuan Xu and Benjamin C. Schafer, "Exposing Approximate Computing Optimizations at Different Levels: From Behavioral to Gate-Level," in *IEEE Transactions on Very Large Scale Integration (VLSI) Systems*, vol. 25, no. 11, pp. 3077-3088, Nov. 2017. DOI=http://dx.doi.org/10.1109/TVLSI.2017.2735299

