#include "ALIS.h"
#include <cmath>


ALIS::ALIS(std::map<int, std::shared_ptr<ApproxUnit>> i_map_Solution):
    m_map_Solution(i_map_Solution),
    m_i64_StatPowerAccum(0),
    m_i64_DynPowerAccum(0),
    m_i64_AreaAccum(0),
    m_i64_DelayAccum(0)
{
    std::map<int, std::shared_ptr<ApproxUnit>>::iterator it;
    for (it = m_map_Solution.begin(); it != m_map_Solution.end(); ++it)
    {
        m_i64_StatPowerAccum += it->second->m_i64_StatPower;
        m_i64_DynPowerAccum += it->second->m_i64_DynPower;
        m_i64_AreaAccum += it->second->m_i64_Area;
        m_i64_DelayAccum += it->second->m_i64_Delay;
    }
}


double ALIS::EstimateStatPower()
{
    return m_i64_StatPowerAccum;
}


double ALIS::EstimateDynPower(double i_i64_DynPowerAccumPrec)
{
    double i64_DynPowerNorm = m_i64_DynPowerAccum / i_i64_DynPowerAccumPrec;
    double i64_DynPowerNormEst = 0;

    // Apply coefficients to the equation for dynamic power estimation
    for (int i = 0;
            i < (sizeof(ai64_DYN_POWER_ESTIMATE_COEFFS) /
                sizeof(*ai64_DYN_POWER_ESTIMATE_COEFFS));
            i++)
    {
        i64_DynPowerNormEst += ai64_DYN_POWER_ESTIMATE_COEFFS[i] *
                                std::pow(i64_DynPowerNorm, i);
    }

    return i64_DynPowerNormEst;
}


double ALIS::EstimateArea()
{
    return m_i64_AreaAccum;
}


double ALIS::EstimateDelay()
{
    // Get DFG critical path nodes
    
    // Go through each critical path node and based on its APU type and
    // position, apply corresponding delay estimation rule
    return m_i64_DelayAccum;
}


double ALIS::GetDynPowerAccum()
{
    return m_i64_DynPowerAccum;
}
