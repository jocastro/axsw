#include "DFG.h"

// CHANGE
using namespace llvm;


//DFGNode(instPTR ins, int i, int sched, int errType, GETPMF getPmf, GETERRORREP getErrRep) :
DFGNode::DFGNode(instPTR ins, int i, int sched) :
	name(ins->getOpcodeName()), instr(ins), id(i), schedStep(sched), bitwidth(0), resultBW(0) {
	//utils::generatePMF(ins, apuErr, errType);
	llvm::MDNode *meta = instr->getMetadata("approx");
	if (!meta) {
		apuErr.err = 0;
		apuErr.prob = 0;
		apuErr.pmf[0] = 1;
		return;
	}
	std::string type = llvm::dyn_cast<llvm::MDString>(meta->getOperand(0))->getString();
	//int adderID = utils::str2int(type.c_str());
	//int w = llvm::dyn_cast<llvm::ConstantInt>(meta->getOperand(1))->getSExtValue();
	std::vector<int> params;
	int opCount = meta->getNumOperands();
	for (int i = 2; i < opCount; i++)
		params.push_back(llvm::dyn_cast<llvm::ConstantInt>(meta->getOperand(i))->getSExtValue());
	/*auto t = getPmf(instr->getOpcode(), adderID, w, params);
	auto tErr = getErrRep(t);
	apuErr.err = tErr.err;
	apuErr.prob = tErr.prob;
	apuErr.pmf = tErr.pmf;*/
}


void DFGNode::readApuErr() {
	std::cout << "Name: " << name << std::endl;
	std::cout << "Depends on: ";
    for (int i = 0, n = dependsOn.size(); i < n; i++) {
		std::cout  << dependsOn[i]->name << " ";
	}
	std::cout << std::endl;
	std::cout << "Err: " << apuErr.err << std::endl;
	std::cout << "Prob: " << apuErr.prob << std::endl;
	std::cout << "Pmf size: " << apuErr.pmf.size() << std::endl;
	for (PMF::iterator it = apuErr.pmf.begin(); it != apuErr.pmf.end(); ++it) {
 		std::cout << "For index: " << it->first << " -> " << it->second << std::endl;
	}
	//for (int i = 0, n = apuErr.pmf.size(); i < n ; i++) {
 	//	std::cout << "Pmf: " << apuErr.pmf[i] << std::endl;
	//}
	std::cout << std::endl;
}


void DFGNode::computeErrRec(std::vector<PMF>& parents, int64_t err, double prob, int depth/*=0*/) {
	if (name == "add") {
		if (depth + 1 == parents.size()) {
			for (PMF::iterator it = parents[depth].begin(); it != parents[depth].end(); ++it) {
				int64_t error = err + it->first;
				double probability = prob * it->second;
				propPMF[error] += probability;
			}
			return;
		}
		for (PMF::iterator it = parents[depth].begin(); it != parents[depth].end(); ++it) {
			computeErrRec(parents, it->first + err, it->second * prob, depth + 1);
		}
	}
	else if (name == "sub") {
		if (depth + 1 == parents.size()) {
			for (PMF::iterator it = parents[depth].begin(); it != parents[depth].end(); ++it) {
				int64_t error = err - it->first;
				double probability = prob * it->second;
				propPMF[error] += probability;
			}
			return;
		}
		for (PMF::iterator it = parents[depth].begin(); it != parents[depth].end(); ++it) {
			computeErrRec(parents, it->first - err, it->second * prob, depth + 1);
		}
	}
	else if (name == "mul") {
		propPMF[0] = 1;
		return;
	}
}


PMF DFGNode::computeErrorTabu() {
	if (!propPMF.empty())
		return propPMF;
	if (dependsOn.empty()) {
		if (apu != nullptr)
			propPMF = apu->pmf;
		else
			propPMF[0] = 1;
		return propPMF;
	}
	std::vector<PMF> parentPMFS;
	for (std::vector<DFGNode*>::iterator it = dependsOn.begin();
		it != dependsOn.end(); ++it) {
		DFGNode* p = *it;
		parentPMFS.push_back(p->computeErrorTabu());
	}
	propPMF.clear();
	PMF temp;
	if (apu != nullptr)
		temp = apu->pmf;
	else
		temp[0] = 1;
	for (PMF::iterator it = temp.begin(); it != temp.end(); ++it) {
		computeErrRec(parentPMFS, it->first, it->second);
	}
	return propPMF;
	//todo do some stuff here
}


std::string DFGNode::getApproName() {
	std::stringstream ss;
	if (apu == nullptr)
		return name;
	if (apu->name == "correct")
		return name;
	ss << apu->name << "_" << apu->L;
	for (std::vector<int>::iterator it = apu->params.begin();
		it != apu->params.end(); ++it) {
		ss << "_" << *it;
	}
	return ss.str();
}


int DFGNode::computeBitwidth(std::map<llvm::Value*, int>& bitwidths) {
	if (resultBW != 0)
		return resultBW;
	bitwidth = 0;
	for (llvm::User::op_iterator i = instr->op_begin(), e = instr->op_end(); i != e;
		++i) {
		// we only care about operands that are created by other instructions
		// also ignore if the dependency is an alloca
		llvm::Value *val = llvm::dyn_cast<llvm::Value>(*i);
		llvm::Instruction *inst = llvm::dyn_cast<llvm::Instruction>(*i);
		llvm::ConstantInt *cInt = llvm::dyn_cast<llvm::ConstantInt>(*i);
		int bw = 0;
		int64_t value = 0;
		if (cInt) {
			value = cInt->getSExtValue();
			bw = std::log2(value) + 1;
			//bw = cInt->getBitWidth();
			c = true;
		}
		else if (inst) {
			for (std::vector<DFGNode*>::iterator it = dependsOn.begin();
				it != dependsOn.end(); ++it) {
				if ((*it)->instr == inst) {
					bw = (*it)->computeBitwidth(bitwidths);
				} else {
					//error
				}
			}
		}
		else if (bitwidths.find(val) != bitwidths.end()) {
			bw = bitwidths[val];
		}
		parentBitwidths.push_back(bw);
		parentVals.push_back(value);
		bitwidth < bw ? bitwidth = bw : bitwidth;
	}
	switch (instr->getOpcode()) {
	case llvm::Instruction::Add:
	case llvm::Instruction::Sub:
		resultBW = bitwidth + 1;
		break;
	case llvm::Instruction::And:
	case llvm::Instruction::Or:
	case llvm::Instruction::Xor:
		resultBW = bitwidth;
		break;
	case llvm::Instruction::Shl:
		resultBW = bitwidth + parentVals[1];
		break;
	case llvm::Instruction::AShr:
	case llvm::Instruction::LShr:
		resultBW = bitwidth - parentVals[1];
		break;
	case llvm::Instruction::Mul:
		for (std::vector<int64_t>::iterator it = parentBitwidths.begin();
			it != parentBitwidths.end(); ++it)
			resultBW += *it;
		break;
	}
	return resultBW;
}


Error DFGNode::computeError(EMF emf) {
	if (propErr != nullptr)
		return *propErr;
	if (dependsOn.empty()) {
		return apuErr;
	}
	std::vector<Error> parentErrs;

	for (llvm::User::op_iterator i = instr->op_begin(), e = instr->op_end(); i != e;
		++i) {
		llvm::Instruction *inst = llvm::dyn_cast<llvm::Instruction>(*i);
		if (inst) {
			for (std::vector<DFGNode*>::iterator it = dependsOn.begin();
				it != dependsOn.end(); ++it) {
				if ((*it)->instr == inst) {
					parentErrs.push_back((*it)->computeError(emf));
				}
			}
		}
		else {
			/*PMF t;
			t[0] = 1;*/
			Error t;
			t.pmf[0] = 1;
			t.err = 0;
			t.prob = 0;
			parentErrs.push_back(t);
		}
	}
	propErr = new Error();
	assert(emf != nullptr);
	if (emf != nullptr) {
		if (c) {
			auto t = emf(apuErr, parentErrs, instr->getOpcode(), parentVals, c);
			propErr->err = t.err;
			propErr->prob = t.prob;
			propErr->pmf = t.pmf;
		}
		else {
			auto t = emf(apuErr, parentErrs, instr->getOpcode(), parentBitwidths, c);
			propErr->err = t.err;
			propErr->prob = t.prob;
			propErr->pmf = t.pmf;
		}
	}
	return *propErr;
	//todo do some stuff here
}


void DFGNode::computeLongestPath() {
    if (longestPath != 0)
    	return;
    longestPath = 0;
    if (dependsOn.empty()) {
    	if (apu != nullptr) {
    		longestPath = apu->m_i64_Delay;
    	}
    	else {
    		longestPath = 1;
    	}
    	return;
    }
    std::set<double> parentLongestPath;
    for (llvm::User::op_iterator i = instr->op_begin(), e = instr->op_end(); i != e;
    ++i) {
    	llvm::Instruction *inst = llvm::dyn_cast<llvm::Instruction>(*i);
    	if (inst) {
    		for (std::vector<DFGNode*>::iterator it = dependsOn.begin();
    			it != dependsOn.end(); ++it) {
    			if ((*it)->instr == inst) {
    				parentLongestPath.insert((*it)->getLongestPath());
    			}
    		}
    	}
    }
    if (apu != nullptr) {
    	longestPath += apu->m_i64_Delay;
    }
    else {
    	longestPath += 1;
    }
    if (!parentLongestPath.empty()) {
    	std::set<double>::iterator last = parentLongestPath.end();
    	last--;
    	longestPath += *last;
    }
}



DFG::DFG(std::string path, std::map<int, /*std::shared_ptr<llvm::Instruction> > &*/ instPTR >IIDs) {
	InstrIDs = IIDs;
}


void DFG::createNode(int id, instPTR instr, int sched, int errID) {
	//DFGNode *node = new DFGNode(instr, id, sched, errID, nullptr, nullptr);
	DFGNode *node = new DFGNode(instr, id, sched);
	nodes.insert(std::pair<int, DFGNode*>(id, node));
}


void DFG::setDependencies(DFGNode *n, std::vector<DFGNode*> deps) {
	n->addDependencies(deps);
	for (unsigned int i = 0; i < deps.size(); i++) {
		deps[i]->addUser(n);
	}
}

void DFG::setDependencies(int id, std::vector<int> deps) {

	std::map<int, DFGNode*>::iterator depIt;
	std::map<int, DFGNode*>::iterator nodeIt = nodes.find(id);
	std::vector<DFGNode*> depP;
	for (unsigned int i = 0; i < deps.size(); i++) {
		depIt = nodes.find(deps[i]);
		if (depIt != nodes.end()) {
			depP.push_back(depIt->second);
		}
	}
	nodeIt->second->addDependencies(depP);
	for (unsigned int i = 0; i < depP.size(); i++) {
		depP[i]->addUser(nodeIt->second);
	}

}


void DFG::computePropagatedError(std::vector<PMF> &errors) {
	errors.clear();
	for (std::map<int, DFGNode*>::iterator it = nodes.begin();
		it != nodes.end(); ++it) {
		if (it->second->getUsers().empty()) {
			errors.push_back(it->second->computeErrorTabu());
		}
	}
}


void DFG::computePropagatedError(std::vector<Error> &errors, EMF emf) {
	errors.clear();
	for (std::vector<DFGNode *>::iterator it = endNodes.begin();
		it != endNodes.end(); ++it) {
		errors.push_back((*it)->computeError(emf));
	}
	/*for (std::map<int, DFGNode*>::iterator it = nodes.begin();
		it != nodes.end(); ++it) {
		if (it->second->getUsers().empty()) {
			errors.push_back(it->second->computeError(emf));
		}
	}*/
}


void DFG::computeBitwidths(std::map<llvm::Value*, int>& bitwidths) {
	for (std::vector<DFGNode*>::iterator it = endNodes.begin();
		it != endNodes.end(); ++it) {
			(*it)->computeBitwidth(bitwidths);
	}
	/*for (std::map<int, DFGNode*>::iterator it = nodes.begin();
		it != nodes.end(); ++it) {
		if (it->second->getUsers().empty()) {
			it->second->computeBitwidth();
		}
	}*/

}


double DFG::getCriticalPath(bool doSim) {
	double delay = 0;
	for (std::vector<DFGNode*>::iterator it = endNodes.begin();
		it != endNodes.end(); ++it) {
		if (doSim)
			(*it)->computeLongestPath();
		double t = (*it)->getLongestPath();
		if (delay < t) delay = t;
	}
	return delay;
}


void DFG::printDFGDot(std::ofstream &out, bool approx) {

	out << "digraph {\n";
	for (std::map<int, DFGNode*>::iterator it = nodes.begin();
		it != nodes.end(); ++it) {
		DFGNode* node = it->second;
		if (approx)
			out << node->getID() << " [label=\"" /*<< node->getSchedStep() << "-"*/ << node->getApproName() << /*"/" << node->getBitwidth() <<*/ "\"] \n";
		else
			out << node->getID() << " [label=\"" /*<< node->getSchedStep() << "-"*/ << node->getName() << /*"/" << node->getBitwidth() <<*/ "\"] \n";
	}
	for (std::map<int, DFGNode*>::iterator it = nodes.begin();
		it != nodes.end(); ++it) {
		DFGNode* node = it->second;
		std::vector<DFGNode*> users = node->getUsers();
		if (!users.empty()) {
			for (std::vector<DFGNode*>::iterator it = users.begin(); it != users.end(); ++it) {
				DFGNode* n = *it;
				out << node->getID() << " -> " << n->getID() << " \n";
			}
		}
	}

	out << "}";

}


//void DFG::dfgGenerator(Function &F, int errID, GETERRORREP getErrRep, GETPMF getPmf)
void DFG::dfgGenerator(Function &F) {
	int id = 0;

	for (Function::iterator b = F.begin(), be = F.end(); b != be; b++) {
		for (BasicBlock::iterator instr = b->begin(), ie = b->end();
			instr != ie; ++instr) {
			if (isa<AllocaInst>(*instr) || isa<PHINode>(*instr)
				|| isa<LoadInst>(*instr) || isa<GetElementPtrInst>(*instr) || isa<StoreInst>(*instr)
				|| isa<ReturnInst>(*instr))
				continue;
		/*	MDNode *schedMD = instr->getMetadata("SStep");
			if (!schedMD) {
				std::cerr << "Missing the Scheduling information. ABORTING!";
				exit(1);
			}*/
			//int sStep = dyn_cast<ConstantInt>(schedMD->getOperand(0))->getSExtValue();
			//instPTR iPtr(&*instr);
			instPTR iPtr = &*instr;
			//DFGNode *iNode = new DFGNode(iPtr, id, 0, errID, getPmf, getErrRep);
			DFGNode *iNode = new DFGNode(iPtr, id, 0);
			InstrNodes[iPtr] = iNode;
			nodes[id] = iNode;
			InstrIDs[id] = iPtr;
			id++;
		}
	}

	for (Function::iterator b = F.begin(), be = F.end(); b != be; b++) {
		for (BasicBlock::iterator instr = b->begin(), ie = b->end();
			instr != ie; ++instr) {
			bool storeFound = false;
			if (isa<AllocaInst>(*instr) || isa<PHINode>(*instr)
				|| isa<LoadInst>(*instr) || isa<GetElementPtrInst>(*instr))
				continue;
			if (isa<StoreInst>(*instr)) {
				storeFound = true;
			}
			instPTR iPtr = &*instr;
			//instPTR iPtr(&*instr);
			DFGNode *node = InstrNodes[iPtr];
			for (User::op_iterator i = iPtr->op_begin(), e = iPtr->op_end(); i != e;
				++i) {
				Instruction *inst = dyn_cast<Instruction>(*i);
				// we only care about operands that are created by other instructions
				// also ignore if the dependency is an alloca
				if (!inst || isa<AllocaInst>(inst) || isa<LoadInst>(inst)) {
					continue;
				}


				//instPTR depPtr(inst);
				DFGNode *depNode = getNode(inst);

				if (storeFound) {
					if (depNode)
						endNodes.push_back(depNode);
					continue;
				}
				node->addDependencies(depNode);
				depNode->addUser(node);
			}
		}
	}
}
