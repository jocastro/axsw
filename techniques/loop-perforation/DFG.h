#ifndef DFG_H
#define DFG_H

#include <vector>
#include <map>
#include <fstream>
#include <string>
#include <iostream>
#include <sstream>
#include <memory>

#include "utils.h"
#include "ApproxUnit.h"

#include "llvm/Support/Casting.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Metadata.h"

typedef std::map<int64_t, double> PMF;
typedef llvm::Instruction* instPTR;
typedef Error(*EMF)(Error&, std::vector<Error>&, int, std::vector<int64_t>&, bool c);
typedef Error(*GETERRORREP)(PMF&);
typedef PMF(*GETPMF)(int, int, int, std::vector<int>&);



class DFGNode {
public:
	//DFGNode(instPTR ins, int i, int sched, int errType, GETPMF getPmf, GETERRORREP getErrRep) :
	DFGNode(instPTR ins, int i, int sched);

	~DFGNode();

	void readApuErr();

	void computeErrRec(std::vector<PMF>& parents, int64_t err, double prob, int depth=0);

	PMF computeErrorTabu();

	std::string getApproName();

	int computeBitwidth(std::map<llvm::Value*, int>& bitwidths);

	Error computeError(EMF emf = nullptr);

    void computeLongestPath();

    inline double getLongestPath() {
		return longestPath;
	}

	inline void resetPROPMF() {
		propPMF.clear();
	}

    inline void addDependencies(std::vector<DFGNode*> deps) {
		for (unsigned int i = 0; i < deps.size(); i++) {
			dependsOn.push_back(deps[i]);
		}
	}

    inline void writeApuErr(double err, double prob, std::map<int64_t, double> pmf) {
		apuErr.err = err;
		apuErr.prob = prob;
		apuErr.pmf = pmf;
	}

	inline void setAPU(std::shared_ptr<ApproxUnit> APU) {
		apu = APU;
	}

    inline void addDependencies(DFGNode* dep) {
		dependsOn.push_back(dep);
	}

	inline void addUser(DFGNode *user) {
		usedBy.push_back(user);
	}

	inline std::vector<DFGNode*> getDependencies() {
		return dependsOn;
	}

	inline std::vector<DFGNode*> getUsers() {
		return usedBy;
	}

	inline std::string getName() {
		return name;
	}

	inline int getID() {
		return id;
	}

	inline instPTR getInstrPtr() {
		return instr;
	}

	inline int getSchedStep() {
		return schedStep;
	}

	inline std::string getApprox() {
		return approx;
	}

	inline void setApprox(std::string a) {
		approx = a;
	}

	/*std::shared_ptr<ApproxUnit> getAPU() {
		return apu;
	}
	void setAPU(std::shared_ptr<ApproxUnit> APU) {
		apu = APU;
	}*/
	inline Error& getpropErrRef() {
		return *propErr;
	}

    inline void resetPROPERR() {
		propErr = nullptr;
	}

	inline int getBitwidth() {
		return bitwidth;
	}

    inline int getResultBW() {
		return resultBW;
	}

	inline void setBitwidth(int bw) {
		bitwidth = bw;
	}

	inline instPTR getInstr() {
		return instr;
	}

	inline int getOpcode() {
		return instr->getOpcode();
	}

	inline void addParentBW(int bw) {
		parentBitwidths.push_back(bw);
	}

private:
	std::string name;
	instPTR instr;
	int id;
	int schedStep;
	std::vector<DFGNode*> dependsOn;
	std::vector<DFGNode*> usedBy;
	std::string approx;
	Error *propErr = nullptr;
	Error apuErr;
	std::shared_ptr<ApproxUnit> apu = nullptr;
	int bitwidth;
	int resultBW;
	std::vector<int64_t> parentBitwidths;
	std::vector<int64_t> parentVals;
	bool c = false;
    double longestPath = 0;
	PMF propPMF;
};



class DFG {
public:

	DFG() {}

	DFG(std::string path, std::map<int, instPTR > IIDs);

	void createNode(int id, instPTR instr, int sched, int errID);

	void setDependencies(DFGNode *n, std::vector<DFGNode*> deps);

	void setDependencies(int id, std::vector<int> deps);

	void computePropagatedError(std::vector<Error> &errors, EMF emf);

	void computePropagatedError(std::vector<PMF> &errors);

	void computeBitwidths(std::map<llvm::Value*, int>& bitwidths);

    double getCriticalPath(bool doSim);

    void printDFGDot(std::ofstream &out, bool approx);

//	void runOnFunction(llvm::Function &f, int errID, GETERRORREP getErrRep, GETPMF getPmf);
	void dfgGenerator(llvm::Function &f);

//	Error computeError(EMF emf = nullptr);

//	int computeBitwidth(std::map<llvm::Value*, int>& bitwidths);

    inline std::vector<DFGNode*> getDependencies(DFGNode *n) {
        return n->getDependencies();
    }

    inline std::vector<DFGNode*> getUsedBy(DFGNode *n) {
	    return n->getUsers();
    }

    inline DFGNode* getNode(int id) {
        return nodes[id];
    }

	inline DFGNode* getNode(instPTR id) {
        return InstrNodes[id];
    }

	inline std::map<int, DFGNode*>& getNodeList() {
        return nodes;
    }

private:
	std::map<int, instPTR> InstrIDs;
	std::map<int, DFGNode*> nodes;
	std::map<instPTR, DFGNode*> InstrNodes;
	std::vector<DFGNode*> endNodes;

};
#endif // DFG
