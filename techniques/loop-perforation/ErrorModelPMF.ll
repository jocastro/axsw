; ModuleID = '../../Source/Errormodels/ErrorModelPMF.cpp'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

%"class.std::map" = type { %"class.std::_Rb_tree" }
%"class.std::_Rb_tree" = type { %"struct.std::_Rb_tree<long, std::pair<const long, double>, std::_Select1st<std::pair<const long, double> >, std::less<long>, std::allocator<std::pair<const long, double> > >::_Rb_tree_impl" }
%"struct.std::_Rb_tree<long, std::pair<const long, double>, std::_Select1st<std::pair<const long, double> >, std::less<long>, std::allocator<std::pair<const long, double> > >::_Rb_tree_impl" = type { %"struct.std::less", %"struct.std::_Rb_tree_node_base", i64 }
%"struct.std::less" = type { i8 }
%"struct.std::_Rb_tree_node_base" = type { i32, %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* }
%"class.std::vector" = type { %"struct.std::_Vector_base" }
%"struct.std::_Vector_base" = type { %"struct.std::_Vector_base<Error, std::allocator<Error> >::_Vector_impl" }
%"struct.std::_Vector_base<Error, std::allocator<Error> >::_Vector_impl" = type { %struct.Error*, %struct.Error*, %struct.Error* }
%struct.Error = type { double, double, %"class.std::map" }
%"class.std::vector.3" = type { %"struct.std::_Vector_base.4" }
%"struct.std::_Vector_base.4" = type { %"struct.std::_Vector_base<long, std::allocator<long> >::_Vector_impl" }
%"struct.std::_Vector_base<long, std::allocator<long> >::_Vector_impl" = type { i64*, i64*, i64* }
%"struct.std::_Rb_tree_node" = type { %"struct.std::_Rb_tree_node_base", %"struct.__gnu_cxx::__aligned_membuf" }
%"struct.__gnu_cxx::__aligned_membuf" = type { [16 x i8] }
%"struct.std::_Rb_tree<long, std::pair<const long, double>, std::_Select1st<std::pair<const long, double> >, std::less<long>, std::allocator<std::pair<const long, double> > >::_Reuse_or_alloc_node" = type { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"*, %"class.std::_Rb_tree"* }

@cutoff = global double 5.000000e-03, align 8

; Function Attrs: uwtable
define void @_Z13computeAddRecRSt3mapIldSt4lessIlESaISt4pairIKldEEES7_RSt6vectorI5ErrorSaIS9_EEldi(%"class.std::map"* dereferenceable(48) %result, %"class.std::map"* nocapture readnone dereferenceable(48) %temp, %"class.std::vector"* nocapture readonly dereferenceable(24) %parents, i64 %err, double %prob, i32 %depth) #0 {
entry:
  %error = alloca i64, align 8
  %add = add nsw i32 %depth, 1
  %conv = sext i32 %add to i64
  %_M_finish.i = getelementptr inbounds %"class.std::vector"* %parents, i64 0, i32 0, i32 0, i32 1
  %0 = load %struct.Error** %_M_finish.i, align 8, !tbaa !1
  %_M_start.i = getelementptr inbounds %"class.std::vector"* %parents, i64 0, i32 0, i32 0, i32 0
  %1 = load %struct.Error** %_M_start.i, align 8, !tbaa !7
  %sub.ptr.lhs.cast.i = ptrtoint %struct.Error* %0 to i64
  %sub.ptr.rhs.cast.i = ptrtoint %struct.Error* %1 to i64
  %sub.ptr.sub.i = sub i64 %sub.ptr.lhs.cast.i, %sub.ptr.rhs.cast.i
  %sub.ptr.div.i = ashr exact i64 %sub.ptr.sub.i, 6
  %cmp = icmp eq i64 %conv, %sub.ptr.div.i
  %conv1 = sext i32 %depth to i64
  %_M_left.i.i = getelementptr inbounds %struct.Error* %1, i64 %conv1, i32 2, i32 0, i32 0, i32 1, i32 2
  %2 = load %"struct.std::_Rb_tree_node_base"** %_M_left.i.i, align 8, !tbaa !8
  %_M_header.i.i6378 = getelementptr inbounds %struct.Error* %1, i64 %conv1, i32 2, i32 0, i32 0, i32 1
  %cmp.i7079 = icmp eq %"struct.std::_Rb_tree_node_base"* %2, %_M_header.i.i6378
  br i1 %cmp, label %for.cond.preheader, label %for.cond22.preheader

for.cond22.preheader:                             ; preds = %entry
  br i1 %cmp.i7079, label %for.end40, label %for.body30.preheader

for.body30.preheader:                             ; preds = %for.cond22.preheader
  br label %for.body30

for.cond.preheader:                               ; preds = %entry
  br i1 %cmp.i7079, label %for.end40, label %for.body.preheader

for.body.preheader:                               ; preds = %for.cond.preheader
  br label %for.body

for.body:                                         ; preds = %for.body.preheader, %for.body
  %3 = phi %"struct.std::_Rb_tree_node_base"* [ %call.i60, %for.body ], [ %2, %for.body.preheader ]
  %_M_storage.i.i67 = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %3, i64 1
  %first = bitcast %"struct.std::_Rb_tree_node_base"* %_M_storage.i.i67 to i64*
  %4 = load i64* %first, align 8, !tbaa !15
  %add11 = add nsw i64 %4, %err
  store i64 %add11, i64* %error, align 8, !tbaa !18
  %second = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %3, i64 1, i32 1
  %5 = bitcast %"struct.std::_Rb_tree_node_base"** %second to double*
  %6 = load double* %5, align 8, !tbaa !19
  %mul = fmul double %6, %prob
  %call13 = call dereferenceable(8) double* @_ZNSt3mapIldSt4lessIlESaISt4pairIKldEEEixERS3_(%"class.std::map"* %result, i64* dereferenceable(8) %error)
  %7 = load double* %call13, align 8, !tbaa !20
  %add14 = fadd double %mul, %7
  store double %add14, double* %call13, align 8, !tbaa !20
  %call.i60 = call %"struct.std::_Rb_tree_node_base"* @_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"* %3) #10
  %8 = load %struct.Error** %_M_start.i, align 8, !tbaa !7
  %_M_header.i.i63 = getelementptr inbounds %struct.Error* %8, i64 %conv1, i32 2, i32 0, i32 0, i32 1
  %cmp.i70 = icmp eq %"struct.std::_Rb_tree_node_base"* %call.i60, %_M_header.i.i63
  br i1 %cmp.i70, label %for.end40.loopexit, label %for.body

for.body30:                                       ; preds = %for.body30.preheader, %for.body30
  %9 = phi %"struct.std::_Rb_tree_node_base"* [ %call.i, %for.body30 ], [ %2, %for.body30.preheader ]
  %_M_storage.i.i55 = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %9, i64 1
  %first32 = bitcast %"struct.std::_Rb_tree_node_base"* %_M_storage.i.i55 to i64*
  %10 = load i64* %first32, align 8, !tbaa !15
  %add33 = add nsw i64 %10, %err
  %second35 = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %9, i64 1, i32 1
  %11 = bitcast %"struct.std::_Rb_tree_node_base"** %second35 to double*
  %12 = load double* %11, align 8, !tbaa !19
  %mul36 = fmul double %12, %prob
  tail call void @_Z13computeAddRecRSt3mapIldSt4lessIlESaISt4pairIKldEEES7_RSt6vectorI5ErrorSaIS9_EEldi(%"class.std::map"* dereferenceable(48) %result, %"class.std::map"* dereferenceable(48) %temp, %"class.std::vector"* dereferenceable(24) %parents, i64 %add33, double %mul36, i32 %add)
  %call.i = tail call %"struct.std::_Rb_tree_node_base"* @_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"* %9) #10
  %13 = load %struct.Error** %_M_start.i, align 8, !tbaa !7
  %_M_header.i.i = getelementptr inbounds %struct.Error* %13, i64 %conv1, i32 2, i32 0, i32 0, i32 1
  %cmp.i = icmp eq %"struct.std::_Rb_tree_node_base"* %call.i, %_M_header.i.i
  br i1 %cmp.i, label %for.end40.loopexit83, label %for.body30

for.end40.loopexit:                               ; preds = %for.body
  br label %for.end40

for.end40.loopexit83:                             ; preds = %for.body30
  br label %for.end40

for.end40:                                        ; preds = %for.end40.loopexit83, %for.end40.loopexit, %for.cond22.preheader, %for.cond.preheader
  ret void
}

; Function Attrs: uwtable
define linkonce_odr dereferenceable(8) double* @_ZNSt3mapIldSt4lessIlESaISt4pairIKldEEEixERS3_(%"class.std::map"* %this, i64* dereferenceable(8) %__k) #0 align 2 {
entry:
  %_M_parent.i.i.i = getelementptr inbounds %"class.std::map"* %this, i64 0, i32 0, i32 0, i32 1, i32 1
  %0 = load %"struct.std::_Rb_tree_node_base"** %_M_parent.i.i.i, align 8, !tbaa !21
  %_M_header.i.i.i = getelementptr inbounds %"class.std::map"* %this, i64 0, i32 0, i32 0, i32 1
  %cmp912.i.i.i = icmp eq %"struct.std::_Rb_tree_node_base"* %0, null
  br i1 %cmp912.i.i.i, label %if.then, label %while.body.lr.ph.lr.ph.i.i.i

while.body.lr.ph.lr.ph.i.i.i:                     ; preds = %entry
  %1 = load i64* %__k, align 8, !tbaa !18
  br label %while.body.lr.ph.i.i.i

while.body.lr.ph.i.i.i:                           ; preds = %if.then.i.i.i, %while.body.lr.ph.lr.ph.i.i.i
  %__x.addr.0.ph14.i.in.i.i = phi %"struct.std::_Rb_tree_node_base"* [ %0, %while.body.lr.ph.lr.ph.i.i.i ], [ %4, %if.then.i.i.i ]
  %__y.addr.0.ph13.i.i.i = phi %"struct.std::_Rb_tree_node_base"* [ %_M_header.i.i.i, %while.body.lr.ph.lr.ph.i.i.i ], [ %__x.addr.010.i.in.i.i.lcssa, %if.then.i.i.i ]
  br label %while.body.i.i.i

while.body.i.i.i:                                 ; preds = %if.else.i.i.i, %while.body.lr.ph.i.i.i
  %__x.addr.010.i.in.i.i = phi %"struct.std::_Rb_tree_node_base"* [ %__x.addr.0.ph14.i.in.i.i, %while.body.lr.ph.i.i.i ], [ %6, %if.else.i.i.i ]
  %_M_storage.i.i.i.i.i.i = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %__x.addr.010.i.in.i.i, i64 1
  %first.i.i.i.i.i = bitcast %"struct.std::_Rb_tree_node_base"* %_M_storage.i.i.i.i.i.i to i64*
  %2 = load i64* %first.i.i.i.i.i, align 8, !tbaa !18
  %cmp.i.i.i.i = icmp slt i64 %2, %1
  br i1 %cmp.i.i.i.i, label %if.else.i.i.i, label %if.then.i.i.i

if.then.i.i.i:                                    ; preds = %while.body.i.i.i
  %__x.addr.010.i.in.i.i.lcssa = phi %"struct.std::_Rb_tree_node_base"* [ %__x.addr.010.i.in.i.i, %while.body.i.i.i ]
  %3 = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %__x.addr.010.i.in.i.i.lcssa, i64 0, i32 2
  %4 = load %"struct.std::_Rb_tree_node_base"** %3, align 8, !tbaa !22
  %cmp9.i.i.i = icmp eq %"struct.std::_Rb_tree_node_base"* %4, null
  br i1 %cmp9.i.i.i, label %_ZNSt3mapIldSt4lessIlESaISt4pairIKldEEE11lower_boundERS3_.exit.loopexit38, label %while.body.lr.ph.i.i.i

if.else.i.i.i:                                    ; preds = %while.body.i.i.i
  %5 = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %__x.addr.010.i.in.i.i, i64 0, i32 3
  %6 = load %"struct.std::_Rb_tree_node_base"** %5, align 8, !tbaa !23
  %cmp.i.i.i = icmp eq %"struct.std::_Rb_tree_node_base"* %6, null
  br i1 %cmp.i.i.i, label %_ZNSt3mapIldSt4lessIlESaISt4pairIKldEEE11lower_boundERS3_.exit.loopexit, label %while.body.i.i.i

_ZNSt3mapIldSt4lessIlESaISt4pairIKldEEE11lower_boundERS3_.exit.loopexit: ; preds = %if.else.i.i.i
  %__y.addr.0.ph13.i.i.i.lcssa40 = phi %"struct.std::_Rb_tree_node_base"* [ %__y.addr.0.ph13.i.i.i, %if.else.i.i.i ]
  br label %_ZNSt3mapIldSt4lessIlESaISt4pairIKldEEE11lower_boundERS3_.exit

_ZNSt3mapIldSt4lessIlESaISt4pairIKldEEE11lower_boundERS3_.exit.loopexit38: ; preds = %if.then.i.i.i
  %__x.addr.010.i.in.i.i.lcssa.lcssa = phi %"struct.std::_Rb_tree_node_base"* [ %__x.addr.010.i.in.i.i.lcssa, %if.then.i.i.i ]
  br label %_ZNSt3mapIldSt4lessIlESaISt4pairIKldEEE11lower_boundERS3_.exit

_ZNSt3mapIldSt4lessIlESaISt4pairIKldEEE11lower_boundERS3_.exit: ; preds = %_ZNSt3mapIldSt4lessIlESaISt4pairIKldEEE11lower_boundERS3_.exit.loopexit38, %_ZNSt3mapIldSt4lessIlESaISt4pairIKldEEE11lower_boundERS3_.exit.loopexit
  %__y.addr.0.ph.lcssa.i.i.i = phi %"struct.std::_Rb_tree_node_base"* [ %__y.addr.0.ph13.i.i.i.lcssa40, %_ZNSt3mapIldSt4lessIlESaISt4pairIKldEEE11lower_boundERS3_.exit.loopexit ], [ %__x.addr.010.i.in.i.i.lcssa.lcssa, %_ZNSt3mapIldSt4lessIlESaISt4pairIKldEEE11lower_boundERS3_.exit.loopexit38 ]
  %cmp.i = icmp eq %"struct.std::_Rb_tree_node_base"* %__y.addr.0.ph.lcssa.i.i.i, %_M_header.i.i.i
  br i1 %cmp.i, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %_ZNSt3mapIldSt4lessIlESaISt4pairIKldEEE11lower_boundERS3_.exit
  %_M_storage.i.i20 = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %__y.addr.0.ph.lcssa.i.i.i, i64 1
  %first = bitcast %"struct.std::_Rb_tree_node_base"* %_M_storage.i.i20 to i64*
  %7 = load i64* %first, align 8, !tbaa !18
  %cmp.i23 = icmp slt i64 %1, %7
  br i1 %cmp.i23, label %if.then, label %if.end

if.then:                                          ; preds = %entry, %lor.lhs.false, %_ZNSt3mapIldSt4lessIlESaISt4pairIKldEEE11lower_boundERS3_.exit
  %__y.addr.0.ph.lcssa.i.i.i29 = phi %"struct.std::_Rb_tree_node_base"* [ %__y.addr.0.ph.lcssa.i.i.i, %lor.lhs.false ], [ %_M_header.i.i.i, %_ZNSt3mapIldSt4lessIlESaISt4pairIKldEEE11lower_boundERS3_.exit ], [ %_M_header.i.i.i, %entry ]
  %_M_t = getelementptr inbounds %"class.std::map"* %this, i64 0, i32 0
  %call2.i.i.i.i.i = tail call noalias i8* @_Znwm(i64 48)
  %_M_storage.i.i.i.i = getelementptr inbounds i8* %call2.i.i.i.i.i, i64 32
  %first.i.i.i.i.i.i.i = bitcast i8* %_M_storage.i.i.i.i to i64*
  %8 = load i64* %__k, align 8, !tbaa !18
  store i64 %8, i64* %first.i.i.i.i.i.i.i, align 8, !tbaa !15
  %9 = getelementptr inbounds i8* %call2.i.i.i.i.i, i64 40
  %10 = bitcast i8* %9 to double*
  store double 0.000000e+00, double* %10, align 8, !tbaa !19
  %call11.i = tail call { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* } @_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS2_ERS1_(%"class.std::_Rb_tree"* %_M_t, %"struct.std::_Rb_tree_node_base"* %__y.addr.0.ph.lcssa.i.i.i29, i64* dereferenceable(8) %first.i.i.i.i.i.i.i)
  %11 = extractvalue { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* } %call11.i, 0
  %12 = extractvalue { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* } %call11.i, 1
  %tobool.i = icmp eq %"struct.std::_Rb_tree_node_base"* %12, null
  br i1 %tobool.i, label %if.end.i, label %if.then.i

if.then.i:                                        ; preds = %if.then
  %cmp.i.i = icmp ne %"struct.std::_Rb_tree_node_base"* %11, null
  %cmp2.i.i = icmp eq %"struct.std::_Rb_tree_node_base"* %_M_header.i.i.i, %12
  %or.cond.i.i = or i1 %cmp.i.i, %cmp2.i.i
  br i1 %or.cond.i.i, label %_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE14_M_insert_nodeEPSt18_Rb_tree_node_baseSA_PSt13_Rb_tree_nodeIS2_E.exit.i, label %lor.rhs.i.i

lor.rhs.i.i:                                      ; preds = %if.then.i
  %_M_storage.i.i.i.i.i = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %12, i64 1
  %first.i.i.i.i = bitcast %"struct.std::_Rb_tree_node_base"* %_M_storage.i.i.i.i.i to i64*
  %13 = load i64* %first.i.i.i.i, align 8, !tbaa !18
  %cmp.i.i.i17 = icmp slt i64 %8, %13
  br label %_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE14_M_insert_nodeEPSt18_Rb_tree_node_baseSA_PSt13_Rb_tree_nodeIS2_E.exit.i

_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE14_M_insert_nodeEPSt18_Rb_tree_node_baseSA_PSt13_Rb_tree_nodeIS2_E.exit.i: ; preds = %lor.rhs.i.i, %if.then.i
  %14 = phi i1 [ true, %if.then.i ], [ %cmp.i.i.i17, %lor.rhs.i.i ]
  %15 = bitcast i8* %call2.i.i.i.i.i to %"struct.std::_Rb_tree_node_base"*
  tail call void @_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_(i1 zeroext %14, %"struct.std::_Rb_tree_node_base"* %15, %"struct.std::_Rb_tree_node_base"* %12, %"struct.std::_Rb_tree_node_base"* dereferenceable(32) %_M_header.i.i.i) #1
  %_M_node_count.i.i = getelementptr inbounds %"class.std::map"* %this, i64 0, i32 0, i32 0, i32 2
  %16 = load i64* %_M_node_count.i.i, align 8, !tbaa !24
  %inc.i.i = add i64 %16, 1
  store i64 %inc.i.i, i64* %_M_node_count.i.i, align 8, !tbaa !24
  br label %if.end

if.end.i:                                         ; preds = %if.then
  tail call void @_ZdlPv(i8* %call2.i.i.i.i.i) #1
  br label %if.end

if.end:                                           ; preds = %if.end.i, %_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE14_M_insert_nodeEPSt18_Rb_tree_node_baseSA_PSt13_Rb_tree_nodeIS2_E.exit.i, %lor.lhs.false
  %__y.addr.0.ph.lcssa.i.i.i27 = phi %"struct.std::_Rb_tree_node_base"* [ %__y.addr.0.ph.lcssa.i.i.i, %lor.lhs.false ], [ %11, %if.end.i ], [ %15, %_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE14_M_insert_nodeEPSt18_Rb_tree_node_baseSA_PSt13_Rb_tree_nodeIS2_E.exit.i ]
  %second = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %__y.addr.0.ph.lcssa.i.i.i27, i64 1, i32 1
  %17 = bitcast %"struct.std::_Rb_tree_node_base"** %second to double*
  ret double* %17
}

; Function Attrs: uwtable
define void @_Z13computeSubRecRSt3mapIldSt4lessIlESaISt4pairIKldEEES7_RSt6vectorI5ErrorSaIS9_EEldi(%"class.std::map"* dereferenceable(48) %result, %"class.std::map"* nocapture readnone dereferenceable(48) %temp, %"class.std::vector"* nocapture readonly dereferenceable(24) %parents, i64 %err, double %prob, i32 %depth) #0 {
entry:
  %error = alloca i64, align 8
  %add = add nsw i32 %depth, 1
  %conv = sext i32 %add to i64
  %_M_finish.i = getelementptr inbounds %"class.std::vector"* %parents, i64 0, i32 0, i32 0, i32 1
  %0 = load %struct.Error** %_M_finish.i, align 8, !tbaa !1
  %_M_start.i = getelementptr inbounds %"class.std::vector"* %parents, i64 0, i32 0, i32 0, i32 0
  %1 = load %struct.Error** %_M_start.i, align 8, !tbaa !7
  %sub.ptr.lhs.cast.i = ptrtoint %struct.Error* %0 to i64
  %sub.ptr.rhs.cast.i = ptrtoint %struct.Error* %1 to i64
  %sub.ptr.sub.i = sub i64 %sub.ptr.lhs.cast.i, %sub.ptr.rhs.cast.i
  %sub.ptr.div.i = ashr exact i64 %sub.ptr.sub.i, 6
  %cmp = icmp eq i64 %conv, %sub.ptr.div.i
  %conv1 = sext i32 %depth to i64
  %_M_left.i.i = getelementptr inbounds %struct.Error* %1, i64 %conv1, i32 2, i32 0, i32 0, i32 1, i32 2
  %2 = load %"struct.std::_Rb_tree_node_base"** %_M_left.i.i, align 8, !tbaa !8
  %_M_header.i.i6277 = getelementptr inbounds %struct.Error* %1, i64 %conv1, i32 2, i32 0, i32 0, i32 1
  %cmp.i6978 = icmp eq %"struct.std::_Rb_tree_node_base"* %2, %_M_header.i.i6277
  br i1 %cmp, label %for.cond.preheader, label %for.cond21.preheader

for.cond21.preheader:                             ; preds = %entry
  br i1 %cmp.i6978, label %for.end39, label %for.body29.preheader

for.body29.preheader:                             ; preds = %for.cond21.preheader
  br label %for.body29

for.cond.preheader:                               ; preds = %entry
  br i1 %cmp.i6978, label %for.end39, label %for.body.preheader

for.body.preheader:                               ; preds = %for.cond.preheader
  br label %for.body

for.body:                                         ; preds = %for.body.preheader, %for.body
  %3 = phi %"struct.std::_Rb_tree_node_base"* [ %call.i59, %for.body ], [ %2, %for.body.preheader ]
  %_M_storage.i.i66 = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %3, i64 1
  %first = bitcast %"struct.std::_Rb_tree_node_base"* %_M_storage.i.i66 to i64*
  %4 = load i64* %first, align 8, !tbaa !15
  %sub = sub nsw i64 %err, %4
  store i64 %sub, i64* %error, align 8, !tbaa !18
  %second = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %3, i64 1, i32 1
  %5 = bitcast %"struct.std::_Rb_tree_node_base"** %second to double*
  %6 = load double* %5, align 8, !tbaa !19
  %mul = fmul double %6, %prob
  %call12 = call dereferenceable(8) double* @_ZNSt3mapIldSt4lessIlESaISt4pairIKldEEEixERS3_(%"class.std::map"* %result, i64* dereferenceable(8) %error)
  %7 = load double* %call12, align 8, !tbaa !20
  %add13 = fadd double %mul, %7
  store double %add13, double* %call12, align 8, !tbaa !20
  %call.i59 = call %"struct.std::_Rb_tree_node_base"* @_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"* %3) #10
  %8 = load %struct.Error** %_M_start.i, align 8, !tbaa !7
  %_M_header.i.i62 = getelementptr inbounds %struct.Error* %8, i64 %conv1, i32 2, i32 0, i32 0, i32 1
  %cmp.i69 = icmp eq %"struct.std::_Rb_tree_node_base"* %call.i59, %_M_header.i.i62
  br i1 %cmp.i69, label %for.end39.loopexit, label %for.body

for.body29:                                       ; preds = %for.body29.preheader, %for.body29
  %9 = phi %"struct.std::_Rb_tree_node_base"* [ %call.i, %for.body29 ], [ %2, %for.body29.preheader ]
  %_M_storage.i.i54 = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %9, i64 1
  %first31 = bitcast %"struct.std::_Rb_tree_node_base"* %_M_storage.i.i54 to i64*
  %10 = load i64* %first31, align 8, !tbaa !15
  %add32 = add nsw i64 %10, %err
  %second34 = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %9, i64 1, i32 1
  %11 = bitcast %"struct.std::_Rb_tree_node_base"** %second34 to double*
  %12 = load double* %11, align 8, !tbaa !19
  %mul35 = fmul double %12, %prob
  tail call void @_Z13computeSubRecRSt3mapIldSt4lessIlESaISt4pairIKldEEES7_RSt6vectorI5ErrorSaIS9_EEldi(%"class.std::map"* dereferenceable(48) %result, %"class.std::map"* dereferenceable(48) %temp, %"class.std::vector"* dereferenceable(24) %parents, i64 %add32, double %mul35, i32 %add)
  %call.i = tail call %"struct.std::_Rb_tree_node_base"* @_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"* %9) #10
  %13 = load %struct.Error** %_M_start.i, align 8, !tbaa !7
  %_M_header.i.i = getelementptr inbounds %struct.Error* %13, i64 %conv1, i32 2, i32 0, i32 0, i32 1
  %cmp.i = icmp eq %"struct.std::_Rb_tree_node_base"* %call.i, %_M_header.i.i
  br i1 %cmp.i, label %for.end39.loopexit82, label %for.body29

for.end39.loopexit:                               ; preds = %for.body
  br label %for.end39

for.end39.loopexit82:                             ; preds = %for.body29
  br label %for.end39

for.end39:                                        ; preds = %for.end39.loopexit82, %for.end39.loopexit, %for.cond21.preheader, %for.cond.preheader
  ret void
}

; Function Attrs: uwtable
define void @_Z13computeMulRecRSt3mapIldSt4lessIlESaISt4pairIKldEEES7_RSt6vectorI5ErrorSaIS9_EERlSD_lldi(%"class.std::map"* dereferenceable(48) %result, %"class.std::map"* nocapture readnone dereferenceable(48) %temp, %"class.std::vector"* nocapture readonly dereferenceable(24) %parents, i64* nocapture readonly dereferenceable(8) %max1, i64* nocapture readonly dereferenceable(8) %max2, i64 %errSelf, i64 %err1, double %prob, i32 %depth) #0 {
entry:
  %error = alloca i64, align 8
  %add = add nsw i32 %depth, 1
  %conv = sext i32 %add to i64
  %_M_finish.i = getelementptr inbounds %"class.std::vector"* %parents, i64 0, i32 0, i32 0, i32 1
  %0 = load %struct.Error** %_M_finish.i, align 8, !tbaa !1
  %_M_start.i = getelementptr inbounds %"class.std::vector"* %parents, i64 0, i32 0, i32 0, i32 0
  %1 = load %struct.Error** %_M_start.i, align 8, !tbaa !7
  %sub.ptr.lhs.cast.i = ptrtoint %struct.Error* %0 to i64
  %sub.ptr.rhs.cast.i = ptrtoint %struct.Error* %1 to i64
  %sub.ptr.sub.i = sub i64 %sub.ptr.lhs.cast.i, %sub.ptr.rhs.cast.i
  %sub.ptr.div.i = ashr exact i64 %sub.ptr.sub.i, 6
  %cmp = icmp eq i64 %conv, %sub.ptr.div.i
  %conv1 = sext i32 %depth to i64
  %_M_left.i.i = getelementptr inbounds %struct.Error* %1, i64 %conv1, i32 2, i32 0, i32 0, i32 1, i32 2
  %2 = load %"struct.std::_Rb_tree_node_base"** %_M_left.i.i, align 8, !tbaa !8
  %_M_header.i.i85110 = getelementptr inbounds %struct.Error* %1, i64 %conv1, i32 2, i32 0, i32 0, i32 1
  %cmp.i93111 = icmp eq %"struct.std::_Rb_tree_node_base"* %2, %_M_header.i.i85110
  br i1 %cmp, label %for.cond.preheader, label %for.cond39.preheader

for.cond39.preheader:                             ; preds = %entry
  br i1 %cmp.i93111, label %for.end56, label %for.body47.preheader

for.body47.preheader:                             ; preds = %for.cond39.preheader
  br label %for.body47

for.cond.preheader:                               ; preds = %entry
  br i1 %cmp.i93111, label %for.end56, label %for.body.lr.ph

for.body.lr.ph:                                   ; preds = %for.cond.preheader
  %conv10 = sitofp i64 %errSelf to double
  %conv11 = sitofp i64 %err1 to double
  br label %for.body

for.body:                                         ; preds = %for.body.lr.ph, %for.body
  %3 = phi %"struct.std::_Rb_tree_node_base"* [ %2, %for.body.lr.ph ], [ %call.i82, %for.body ]
  %4 = load i64* %max2, align 8, !tbaa !18
  %_M_storage.i.i100 = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %3, i64 1
  %first = bitcast %"struct.std::_Rb_tree_node_base"* %_M_storage.i.i100 to i64*
  %5 = load i64* %first, align 8, !tbaa !15
  %sub = sub nsw i64 %4, %5
  %conv.i96 = sitofp i64 %4 to double
  %conv1.i97 = sitofp i64 %sub to double
  %call.i98 = call double @fmax(double %conv.i96, double %conv1.i97) #11
  %mul = fmul double %conv11, %call.i98
  %add14 = fadd double %conv10, %mul
  %conv17 = sitofp i64 %5 to double
  %6 = load i64* %max1, align 8, !tbaa !18
  %sub18 = sub nsw i64 %6, %err1
  %conv.i = sitofp i64 %6 to double
  %conv1.i = sitofp i64 %sub18 to double
  %call.i90 = call double @fmax(double %conv.i, double %conv1.i) #11
  %mul20 = fmul double %conv17, %call.i90
  %add21 = fadd double %add14, %mul20
  %mul24 = mul nsw i64 %5, %err1
  %conv25 = sitofp i64 %mul24 to double
  %add26 = fadd double %conv25, %add21
  %conv27 = fptosi double %add26 to i64
  store i64 %conv27, i64* %error, align 8, !tbaa !18
  %second = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %3, i64 1, i32 1
  %7 = bitcast %"struct.std::_Rb_tree_node_base"** %second to double*
  %8 = load double* %7, align 8, !tbaa !19
  %mul29 = fmul double %8, %prob
  %call30 = call dereferenceable(8) double* @_ZNSt3mapIldSt4lessIlESaISt4pairIKldEEEixERS3_(%"class.std::map"* %result, i64* dereferenceable(8) %error)
  %9 = load double* %call30, align 8, !tbaa !20
  %add31 = fadd double %mul29, %9
  store double %add31, double* %call30, align 8, !tbaa !20
  %call.i82 = call %"struct.std::_Rb_tree_node_base"* @_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"* %3) #10
  %10 = load %struct.Error** %_M_start.i, align 8, !tbaa !7
  %_M_header.i.i85 = getelementptr inbounds %struct.Error* %10, i64 %conv1, i32 2, i32 0, i32 0, i32 1
  %cmp.i93 = icmp eq %"struct.std::_Rb_tree_node_base"* %call.i82, %_M_header.i.i85
  br i1 %cmp.i93, label %for.end56.loopexit, label %for.body

for.body47:                                       ; preds = %for.body47.preheader, %for.body47
  %11 = phi %"struct.std::_Rb_tree_node_base"* [ %call.i, %for.body47 ], [ %2, %for.body47.preheader ]
  %_M_storage.i.i77 = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %11, i64 1
  %first49 = bitcast %"struct.std::_Rb_tree_node_base"* %_M_storage.i.i77 to i64*
  %12 = load i64* %first49, align 8, !tbaa !15
  %second51 = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %11, i64 1, i32 1
  %13 = bitcast %"struct.std::_Rb_tree_node_base"** %second51 to double*
  %14 = load double* %13, align 8, !tbaa !19
  %mul52 = fmul double %14, %prob
  tail call void @_Z13computeMulRecRSt3mapIldSt4lessIlESaISt4pairIKldEEES7_RSt6vectorI5ErrorSaIS9_EERlSD_lldi(%"class.std::map"* dereferenceable(48) %result, %"class.std::map"* dereferenceable(48) %temp, %"class.std::vector"* dereferenceable(24) %parents, i64* dereferenceable(8) %max1, i64* dereferenceable(8) %max2, i64 %errSelf, i64 %12, double %mul52, i32 %add)
  %call.i = tail call %"struct.std::_Rb_tree_node_base"* @_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"* %11) #10
  %15 = load %struct.Error** %_M_start.i, align 8, !tbaa !7
  %_M_header.i.i = getelementptr inbounds %struct.Error* %15, i64 %conv1, i32 2, i32 0, i32 0, i32 1
  %cmp.i = icmp eq %"struct.std::_Rb_tree_node_base"* %call.i, %_M_header.i.i
  br i1 %cmp.i, label %for.end56.loopexit115, label %for.body47

for.end56.loopexit:                               ; preds = %for.body
  br label %for.end56

for.end56.loopexit115:                            ; preds = %for.body47
  br label %for.end56

for.end56:                                        ; preds = %for.end56.loopexit115, %for.end56.loopexit, %for.cond39.preheader, %for.cond.preheader
  ret void
}

; Function Attrs: uwtable
define void @_Z9propagateR5ErrorRSt6vectorIS_SaIS_EEiS1_IlSaIlEEb(%struct.Error* noalias sret %agg.result, %struct.Error* dereferenceable(64) %nodeErr, %"class.std::vector"* nocapture readonly dereferenceable(24) %parentErrs, i32 %typeID, %"class.std::vector.3"* nocapture readonly %bw, i1 zeroext %c) #0 {
entry:
  %temp = alloca %"class.std::map", align 8
  %result = alloca %"class.std::map", align 8
  %max1 = alloca i64, align 8
  %max2 = alloca i64, align 8
  %ref.tmp105 = alloca i64, align 8
  %ref.tmp137 = alloca i64, align 8
  %0 = getelementptr inbounds %"class.std::map"* %temp, i64 0, i32 0, i32 0, i32 0, i32 0
  call void @llvm.lifetime.start(i64 48, i8* %0) #1
  %_M_header.i.i.i = getelementptr inbounds %"class.std::map"* %temp, i64 0, i32 0, i32 0, i32 1
  %_M_left.i.i.i.i = getelementptr inbounds %"class.std::map"* %temp, i64 0, i32 0, i32 0, i32 1, i32 2
  %1 = bitcast %"struct.std::_Rb_tree_node_base"* %_M_header.i.i.i to i8*
  call void @llvm.memset.p0i8.i64(i8* %1, i8 0, i64 40, i32 8, i1 false) #1
  store %"struct.std::_Rb_tree_node_base"* %_M_header.i.i.i, %"struct.std::_Rb_tree_node_base"** %_M_left.i.i.i.i, align 8, !tbaa !25
  %_M_right.i.i.i.i = getelementptr inbounds %"class.std::map"* %temp, i64 0, i32 0, i32 0, i32 1, i32 3
  store %"struct.std::_Rb_tree_node_base"* %_M_header.i.i.i, %"struct.std::_Rb_tree_node_base"** %_M_right.i.i.i.i, align 8, !tbaa !26
  %2 = getelementptr inbounds %"class.std::map"* %result, i64 0, i32 0, i32 0, i32 0, i32 0
  call void @llvm.lifetime.start(i64 48, i8* %2) #1
  %_M_header.i.i.i178 = getelementptr inbounds %"class.std::map"* %result, i64 0, i32 0, i32 0, i32 1
  %_M_left.i.i.i.i179 = getelementptr inbounds %"class.std::map"* %result, i64 0, i32 0, i32 0, i32 1, i32 2
  %3 = bitcast %"struct.std::_Rb_tree_node_base"* %_M_header.i.i.i178 to i8*
  call void @llvm.memset.p0i8.i64(i8* %3, i8 0, i64 40, i32 8, i1 false) #1
  store %"struct.std::_Rb_tree_node_base"* %_M_header.i.i.i178, %"struct.std::_Rb_tree_node_base"** %_M_left.i.i.i.i179, align 8, !tbaa !25
  %_M_right.i.i.i.i180 = getelementptr inbounds %"class.std::map"* %result, i64 0, i32 0, i32 0, i32 1, i32 3
  store %"struct.std::_Rb_tree_node_base"* %_M_header.i.i.i178, %"struct.std::_Rb_tree_node_base"** %_M_right.i.i.i.i180, align 8, !tbaa !26
  %_M_header.i.i.i.i = getelementptr inbounds %struct.Error* %agg.result, i64 0, i32 2, i32 0, i32 0, i32 1
  %_M_left.i.i.i.i.i = getelementptr inbounds %struct.Error* %agg.result, i64 0, i32 2, i32 0, i32 0, i32 1, i32 2
  %4 = bitcast %"struct.std::_Rb_tree_node_base"* %_M_header.i.i.i.i to i8*
  call void @llvm.memset.p0i8.i64(i8* %4, i8 0, i64 40, i32 8, i1 false) #1
  %5 = bitcast %struct.Error* %agg.result to i8*
  call void @llvm.memset.p0i8.i64(i8* %5, i8 0, i64 16, i32 8, i1 false) #1
  store %"struct.std::_Rb_tree_node_base"* %_M_header.i.i.i.i, %"struct.std::_Rb_tree_node_base"** %_M_left.i.i.i.i.i, align 8, !tbaa !25
  %_M_right.i.i.i.i.i = getelementptr inbounds %struct.Error* %agg.result, i64 0, i32 2, i32 0, i32 0, i32 1, i32 3
  store %"struct.std::_Rb_tree_node_base"* %_M_header.i.i.i.i, %"struct.std::_Rb_tree_node_base"** %_M_right.i.i.i.i.i, align 8, !tbaa !26
  switch i32 %typeID, label %if.else29 [
    i32 8, label %if.then
    i32 10, label %if.then9
  ]

if.then:                                          ; preds = %entry
  %_M_left.i.i = getelementptr inbounds %struct.Error* %nodeErr, i64 0, i32 2, i32 0, i32 0, i32 1, i32 2
  %6 = load %"struct.std::_Rb_tree_node_base"** %_M_left.i.i, align 8, !tbaa !8
  %_M_header.i.i = getelementptr inbounds %struct.Error* %nodeErr, i64 0, i32 2, i32 0, i32 0, i32 1
  %cmp.i309 = icmp eq %"struct.std::_Rb_tree_node_base"* %6, %_M_header.i.i
  br i1 %cmp.i309, label %if.end150, label %for.body.preheader

for.body.preheader:                               ; preds = %if.then
  br label %for.body

for.body:                                         ; preds = %for.body.preheader, %for.inc
  %7 = phi %"struct.std::_Rb_tree_node_base"* [ %call.i218, %for.inc ], [ %6, %for.body.preheader ]
  %_M_storage.i.i199 = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %7, i64 1
  %first = bitcast %"struct.std::_Rb_tree_node_base"* %_M_storage.i.i199 to i64*
  %8 = load i64* %first, align 8, !tbaa !15
  %second = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %7, i64 1, i32 1
  %9 = bitcast %"struct.std::_Rb_tree_node_base"** %second to double*
  %10 = load double* %9, align 8, !tbaa !19
  invoke void @_Z13computeAddRecRSt3mapIldSt4lessIlESaISt4pairIKldEEES7_RSt6vectorI5ErrorSaIS9_EEldi(%"class.std::map"* dereferenceable(48) %result, %"class.std::map"* dereferenceable(48) %temp, %"class.std::vector"* dereferenceable(24) %parentErrs, i64 %8, double %10, i32 0)
          to label %for.inc unwind label %_ZNSt6vectorImSaImEED2Ev.exit222.nonloopexit.nonloopexit.nonloopexit.nonloopexit.loopexit

for.inc:                                          ; preds = %for.body
  %call.i218 = call %"struct.std::_Rb_tree_node_base"* @_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"* %7) #10
  %cmp.i = icmp eq %"struct.std::_Rb_tree_node_base"* %call.i218, %_M_header.i.i
  br i1 %cmp.i, label %if.end150.loopexit319, label %for.body

_ZNSt6vectorImSaImEED2Ev.exit222.loopexit:        ; preds = %for.body102
  %lpad.loopexit = landingpad { i8*, i32 } personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*)
          cleanup
  br label %_ZNSt6vectorImSaImEED2Ev.exit222

_ZNSt6vectorImSaImEED2Ev.exit222.nonloopexit.loopexit: ; preds = %for.body134
  %lpad.loopexit286 = landingpad { i8*, i32 } personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*)
          cleanup
  br label %_ZNSt6vectorImSaImEED2Ev.exit222

_ZNSt6vectorImSaImEED2Ev.exit222.nonloopexit.nonloopexit.loopexit: ; preds = %for.body72
  %lpad.loopexit290 = landingpad { i8*, i32 } personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*)
          cleanup
  br label %_ZNSt6vectorImSaImEED2Ev.exit222

_ZNSt6vectorImSaImEED2Ev.exit222.nonloopexit.nonloopexit.nonloopexit.loopexit: ; preds = %for.body48
  %lpad.loopexit293 = landingpad { i8*, i32 } personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*)
          cleanup
  br label %_ZNSt6vectorImSaImEED2Ev.exit222

_ZNSt6vectorImSaImEED2Ev.exit222.nonloopexit.nonloopexit.nonloopexit.nonloopexit.loopexit: ; preds = %for.body
  %lpad.loopexit297 = landingpad { i8*, i32 } personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*)
          cleanup
  br label %_ZNSt6vectorImSaImEED2Ev.exit222

_ZNSt6vectorImSaImEED2Ev.exit222.nonloopexit.nonloopexit.nonloopexit.nonloopexit.nonloopexit.loopexit: ; preds = %for.body20
  %lpad.loopexit300 = landingpad { i8*, i32 } personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*)
          cleanup
  br label %_ZNSt6vectorImSaImEED2Ev.exit222

_ZNSt6vectorImSaImEED2Ev.exit222.nonloopexit.nonloopexit.nonloopexit.nonloopexit.nonloopexit.nonloopexit: ; preds = %if.end150
  %lpad.nonloopexit301 = landingpad { i8*, i32 } personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*)
          cleanup
  br label %_ZNSt6vectorImSaImEED2Ev.exit222

_ZNSt6vectorImSaImEED2Ev.exit222:                 ; preds = %_ZNSt6vectorImSaImEED2Ev.exit222.nonloopexit.loopexit, %_ZNSt6vectorImSaImEED2Ev.exit222.nonloopexit.nonloopexit.nonloopexit.loopexit, %_ZNSt6vectorImSaImEED2Ev.exit222.nonloopexit.nonloopexit.nonloopexit.nonloopexit.nonloopexit.loopexit, %_ZNSt6vectorImSaImEED2Ev.exit222.nonloopexit.nonloopexit.nonloopexit.nonloopexit.nonloopexit.nonloopexit, %_ZNSt6vectorImSaImEED2Ev.exit222.nonloopexit.nonloopexit.nonloopexit.nonloopexit.loopexit, %_ZNSt6vectorImSaImEED2Ev.exit222.nonloopexit.nonloopexit.loopexit, %_ZNSt6vectorImSaImEED2Ev.exit222.loopexit
  %lpad.phi = phi { i8*, i32 } [ %lpad.loopexit, %_ZNSt6vectorImSaImEED2Ev.exit222.loopexit ], [ %lpad.loopexit286, %_ZNSt6vectorImSaImEED2Ev.exit222.nonloopexit.loopexit ], [ %lpad.loopexit290, %_ZNSt6vectorImSaImEED2Ev.exit222.nonloopexit.nonloopexit.loopexit ], [ %lpad.loopexit293, %_ZNSt6vectorImSaImEED2Ev.exit222.nonloopexit.nonloopexit.nonloopexit.loopexit ], [ %lpad.loopexit297, %_ZNSt6vectorImSaImEED2Ev.exit222.nonloopexit.nonloopexit.nonloopexit.nonloopexit.loopexit ], [ %lpad.loopexit300, %_ZNSt6vectorImSaImEED2Ev.exit222.nonloopexit.nonloopexit.nonloopexit.nonloopexit.nonloopexit.loopexit ], [ %lpad.nonloopexit301, %_ZNSt6vectorImSaImEED2Ev.exit222.nonloopexit.nonloopexit.nonloopexit.nonloopexit.nonloopexit.nonloopexit ]
  %_M_t.i.i = getelementptr inbounds %struct.Error* %agg.result, i64 0, i32 2, i32 0
  %_M_parent.i.i.i.i = getelementptr inbounds %struct.Error* %agg.result, i64 0, i32 2, i32 0, i32 0, i32 1, i32 1
  %11 = load %"struct.std::_Rb_tree_node_base"** %_M_parent.i.i.i.i, align 8, !tbaa !21
  %12 = bitcast %"struct.std::_Rb_tree_node_base"* %11 to %"struct.std::_Rb_tree_node"*
  call void @_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E(%"class.std::_Rb_tree"* %_M_t.i.i, %"struct.std::_Rb_tree_node"* %12) #1
  %_M_t.i223 = getelementptr inbounds %"class.std::map"* %result, i64 0, i32 0
  %_M_parent.i.i.i224 = getelementptr inbounds %"class.std::map"* %result, i64 0, i32 0, i32 0, i32 1, i32 1
  %13 = load %"struct.std::_Rb_tree_node_base"** %_M_parent.i.i.i224, align 8, !tbaa !21
  %14 = bitcast %"struct.std::_Rb_tree_node_base"* %13 to %"struct.std::_Rb_tree_node"*
  call void @_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E(%"class.std::_Rb_tree"* %_M_t.i223, %"struct.std::_Rb_tree_node"* %14) #1
  %_M_t.i225 = getelementptr inbounds %"class.std::map"* %temp, i64 0, i32 0
  %_M_parent.i.i.i226 = getelementptr inbounds %"class.std::map"* %temp, i64 0, i32 0, i32 0, i32 1, i32 1
  %15 = load %"struct.std::_Rb_tree_node_base"** %_M_parent.i.i.i226, align 8, !tbaa !21
  %16 = bitcast %"struct.std::_Rb_tree_node_base"* %15 to %"struct.std::_Rb_tree_node"*
  call void @_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E(%"class.std::_Rb_tree"* %_M_t.i225, %"struct.std::_Rb_tree_node"* %16) #1
  resume { i8*, i32 } %lpad.phi

if.then9:                                         ; preds = %entry
  %_M_left.i.i227 = getelementptr inbounds %struct.Error* %nodeErr, i64 0, i32 2, i32 0, i32 0, i32 1, i32 2
  %17 = load %"struct.std::_Rb_tree_node_base"** %_M_left.i.i227, align 8, !tbaa !8
  %_M_header.i.i228 = getelementptr inbounds %struct.Error* %nodeErr, i64 0, i32 2, i32 0, i32 0, i32 1
  %cmp.i245310 = icmp eq %"struct.std::_Rb_tree_node_base"* %17, %_M_header.i.i228
  br i1 %cmp.i245310, label %if.end150, label %for.body20.preheader

for.body20.preheader:                             ; preds = %if.then9
  br label %for.body20

for.body20:                                       ; preds = %for.body20.preheader, %for.inc26
  %18 = phi %"struct.std::_Rb_tree_node_base"* [ %call.i261, %for.inc26 ], [ %17, %for.body20.preheader ]
  %_M_storage.i.i247 = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %18, i64 1
  %first22 = bitcast %"struct.std::_Rb_tree_node_base"* %_M_storage.i.i247 to i64*
  %19 = load i64* %first22, align 8, !tbaa !15
  %second24 = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %18, i64 1, i32 1
  %20 = bitcast %"struct.std::_Rb_tree_node_base"** %second24 to double*
  %21 = load double* %20, align 8, !tbaa !19
  invoke void @_Z13computeSubRecRSt3mapIldSt4lessIlESaISt4pairIKldEEES7_RSt6vectorI5ErrorSaIS9_EEldi(%"class.std::map"* dereferenceable(48) %result, %"class.std::map"* dereferenceable(48) %temp, %"class.std::vector"* dereferenceable(24) %parentErrs, i64 %19, double %21, i32 0)
          to label %for.inc26 unwind label %_ZNSt6vectorImSaImEED2Ev.exit222.nonloopexit.nonloopexit.nonloopexit.nonloopexit.nonloopexit.loopexit

for.inc26:                                        ; preds = %for.body20
  %call.i261 = call %"struct.std::_Rb_tree_node_base"* @_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"* %18) #10
  %cmp.i245 = icmp eq %"struct.std::_Rb_tree_node_base"* %call.i261, %_M_header.i.i228
  br i1 %cmp.i245, label %if.end150.loopexit320, label %for.body20

if.else29:                                        ; preds = %entry
  %cmp30.not = icmp ne i32 %typeID, 12
  %brmerge = or i1 %cmp30.not, %c
  br i1 %brmerge, label %if.else57, label %if.then31

if.then31:                                        ; preds = %if.else29
  %_M_start.i259 = getelementptr inbounds %"class.std::vector.3"* %bw, i64 0, i32 0, i32 0, i32 0
  %22 = load i64** %_M_start.i259, align 8, !tbaa !27
  %23 = load i64* %22, align 8, !tbaa !18
  %sh_prom = trunc i64 %23 to i32
  %shl = shl i32 1, %sh_prom
  %sub = add nsw i32 %shl, -1
  %conv = sext i32 %sub to i64
  store i64 %conv, i64* %max1, align 8, !tbaa !18
  %add.ptr.i258 = getelementptr inbounds i64* %22, i64 1
  %24 = load i64* %add.ptr.i258, align 8, !tbaa !18
  %sh_prom34 = trunc i64 %24 to i32
  %shl35 = shl i32 1, %sh_prom34
  %sub36 = add nsw i32 %shl35, -1
  %conv37 = sext i32 %sub36 to i64
  store i64 %conv37, i64* %max2, align 8, !tbaa !18
  %_M_left.i.i256 = getelementptr inbounds %struct.Error* %nodeErr, i64 0, i32 2, i32 0, i32 0, i32 1, i32 2
  %25 = load %"struct.std::_Rb_tree_node_base"** %_M_left.i.i256, align 8, !tbaa !8
  %_M_header.i.i255 = getelementptr inbounds %struct.Error* %nodeErr, i64 0, i32 2, i32 0, i32 0, i32 1
  %cmp.i254308 = icmp eq %"struct.std::_Rb_tree_node_base"* %25, %_M_header.i.i255
  br i1 %cmp.i254308, label %if.end150, label %for.body48.preheader

for.body48.preheader:                             ; preds = %if.then31
  br label %for.body48

for.body48:                                       ; preds = %for.body48.preheader, %for.inc54
  %26 = phi %"struct.std::_Rb_tree_node_base"* [ %call.i242, %for.inc54 ], [ %25, %for.body48.preheader ]
  %_M_storage.i.i251 = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %26, i64 1
  %first50 = bitcast %"struct.std::_Rb_tree_node_base"* %_M_storage.i.i251 to i64*
  %27 = load i64* %first50, align 8, !tbaa !15
  %second52 = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %26, i64 1, i32 1
  %28 = bitcast %"struct.std::_Rb_tree_node_base"** %second52 to double*
  %29 = load double* %28, align 8, !tbaa !19
  invoke void @_Z13computeMulRecRSt3mapIldSt4lessIlESaISt4pairIKldEEES7_RSt6vectorI5ErrorSaIS9_EERlSD_lldi(%"class.std::map"* dereferenceable(48) %result, %"class.std::map"* dereferenceable(48) %temp, %"class.std::vector"* dereferenceable(24) %parentErrs, i64* dereferenceable(8) %max1, i64* dereferenceable(8) %max2, i64 %27, i64 0, double %29, i32 0)
          to label %for.inc54 unwind label %_ZNSt6vectorImSaImEED2Ev.exit222.nonloopexit.nonloopexit.nonloopexit.loopexit

for.inc54:                                        ; preds = %for.body48
  %call.i242 = call %"struct.std::_Rb_tree_node_base"* @_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"* %26) #10
  %cmp.i254 = icmp eq %"struct.std::_Rb_tree_node_base"* %call.i242, %_M_header.i.i255
  br i1 %cmp.i254, label %if.end150.loopexit318, label %for.body48

if.else57:                                        ; preds = %if.else29
  %c.not = xor i1 %c, true
  %brmerge177 = or i1 %cmp30.not, %c.not
  br i1 %brmerge177, label %if.else83, label %if.then61

if.then61:                                        ; preds = %if.else57
  %_M_left.i.i240 = getelementptr inbounds %struct.Error* %nodeErr, i64 0, i32 2, i32 0, i32 0, i32 1, i32 2
  %30 = load %"struct.std::_Rb_tree_node_base"** %_M_left.i.i240, align 8, !tbaa !8
  %_M_header.i.i239 = getelementptr inbounds %struct.Error* %nodeErr, i64 0, i32 2, i32 0, i32 0, i32 1
  %cmp.i238307 = icmp eq %"struct.std::_Rb_tree_node_base"* %30, %_M_header.i.i239
  br i1 %cmp.i238307, label %if.end150, label %for.body72.lr.ph

for.body72.lr.ph:                                 ; preds = %if.then61
  %_M_start.i235 = getelementptr inbounds %"class.std::vector.3"* %bw, i64 0, i32 0, i32 0, i32 0
  br label %for.body72

for.body72:                                       ; preds = %for.body72.lr.ph, %for.inc80
  %31 = phi %"struct.std::_Rb_tree_node_base"* [ %30, %for.body72.lr.ph ], [ %call.i216, %for.inc80 ]
  %32 = load i64** %_M_start.i235, align 8, !tbaa !27
  %add.ptr.i234 = getelementptr inbounds i64* %32, i64 1
  %_M_storage.i.i232 = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %31, i64 1
  %first76 = bitcast %"struct.std::_Rb_tree_node_base"* %_M_storage.i.i232 to i64*
  %33 = load i64* %first76, align 8, !tbaa !15
  %second78 = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %31, i64 1, i32 1
  %34 = bitcast %"struct.std::_Rb_tree_node_base"** %second78 to double*
  %35 = load double* %34, align 8, !tbaa !19
  invoke void @_Z13computeMulRecRSt3mapIldSt4lessIlESaISt4pairIKldEEES7_RSt6vectorI5ErrorSaIS9_EERlSD_lldi(%"class.std::map"* dereferenceable(48) %result, %"class.std::map"* dereferenceable(48) %temp, %"class.std::vector"* dereferenceable(24) %parentErrs, i64* dereferenceable(8) %32, i64* dereferenceable(8) %add.ptr.i234, i64 %33, i64 0, double %35, i32 0)
          to label %for.inc80 unwind label %_ZNSt6vectorImSaImEED2Ev.exit222.nonloopexit.nonloopexit.loopexit

for.inc80:                                        ; preds = %for.body72
  %call.i216 = call %"struct.std::_Rb_tree_node_base"* @_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"* %31) #10
  %cmp.i238 = icmp eq %"struct.std::_Rb_tree_node_base"* %call.i216, %_M_header.i.i239
  br i1 %cmp.i238, label %if.end150.loopexit317, label %for.body72

if.else83:                                        ; preds = %if.else57
  %cmp84 = icmp eq i32 %typeID, 20
  br i1 %cmp84, label %if.then85, label %if.else114

if.then85:                                        ; preds = %if.else83
  %_M_start.i213 = getelementptr inbounds %"class.std::vector.3"* %bw, i64 0, i32 0, i32 0, i32 0
  %36 = load i64** %_M_start.i213, align 8, !tbaa !27
  %add.ptr.i214 = getelementptr inbounds i64* %36, i64 1
  %37 = load i64* %add.ptr.i214, align 8, !tbaa !18
  %sh_prom87 = trunc i64 %37 to i32
  %shl88 = shl i32 1, %sh_prom87
  %conv89 = sext i32 %shl88 to i64
  %_M_start.i212 = getelementptr inbounds %"class.std::vector"* %parentErrs, i64 0, i32 0, i32 0, i32 0
  %38 = load %struct.Error** %_M_start.i212, align 8, !tbaa !7
  %_M_left.i.i211 = getelementptr inbounds %struct.Error* %38, i64 0, i32 2, i32 0, i32 0, i32 1, i32 2
  %39 = load %"struct.std::_Rb_tree_node_base"** %_M_left.i.i211, align 8, !tbaa !8
  %_M_header.i.i209303 = getelementptr inbounds %struct.Error* %38, i64 0, i32 2, i32 0, i32 0, i32 1
  %cmp.i208304 = icmp eq %"struct.std::_Rb_tree_node_base"* %39, %_M_header.i.i209303
  br i1 %cmp.i208304, label %if.end150, label %for.body102.preheader

for.body102.preheader:                            ; preds = %if.then85
  br label %for.body102

for.body102:                                      ; preds = %for.body102.preheader, %invoke.cont109
  %40 = phi %"struct.std::_Rb_tree_node_base"* [ %call.i197, %invoke.cont109 ], [ %39, %for.body102.preheader ]
  %_M_storage.i.i205 = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %40, i64 1
  %second104 = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %40, i64 1, i32 1
  %41 = bitcast %"struct.std::_Rb_tree_node_base"** %second104 to double*
  %42 = load double* %41, align 8, !tbaa !19
  %first107 = bitcast %"struct.std::_Rb_tree_node_base"* %_M_storage.i.i205 to i64*
  %43 = load i64* %first107, align 8, !tbaa !15
  %mul108 = mul nsw i64 %43, %conv89
  store i64 %mul108, i64* %ref.tmp105, align 8, !tbaa !18
  %call110 = invoke dereferenceable(8) double* @_ZNSt3mapIldSt4lessIlESaISt4pairIKldEEEixEOl(%"class.std::map"* %result, i64* dereferenceable(8) %ref.tmp105)
          to label %invoke.cont109 unwind label %_ZNSt6vectorImSaImEED2Ev.exit222.loopexit

invoke.cont109:                                   ; preds = %for.body102
  store double %42, double* %call110, align 8, !tbaa !20
  %call.i197 = call %"struct.std::_Rb_tree_node_base"* @_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"* %40) #10
  %44 = load %struct.Error** %_M_start.i212, align 8, !tbaa !7
  %_M_header.i.i209 = getelementptr inbounds %struct.Error* %44, i64 0, i32 2, i32 0, i32 0, i32 1
  %cmp.i208 = icmp eq %"struct.std::_Rb_tree_node_base"* %call.i197, %_M_header.i.i209
  br i1 %cmp.i208, label %if.end150.loopexit, label %for.body102

if.else114:                                       ; preds = %if.else83
  %typeID.off = add i32 %typeID, -21
  %45 = icmp ult i32 %typeID.off, 2
  br i1 %45, label %if.then117, label %if.end150

if.then117:                                       ; preds = %if.else114
  %_M_start.i195 = getelementptr inbounds %"class.std::vector.3"* %bw, i64 0, i32 0, i32 0, i32 0
  %46 = load i64** %_M_start.i195, align 8, !tbaa !27
  %add.ptr.i = getelementptr inbounds i64* %46, i64 1
  %47 = load i64* %add.ptr.i, align 8, !tbaa !18
  %sh_prom119 = trunc i64 %47 to i32
  %shl120 = shl i32 1, %sh_prom119
  %conv121 = sext i32 %shl120 to i64
  %_M_start.i194 = getelementptr inbounds %"class.std::vector"* %parentErrs, i64 0, i32 0, i32 0, i32 0
  %48 = load %struct.Error** %_M_start.i194, align 8, !tbaa !7
  %_M_left.i.i193 = getelementptr inbounds %struct.Error* %48, i64 0, i32 2, i32 0, i32 0, i32 1, i32 2
  %49 = load %"struct.std::_Rb_tree_node_base"** %_M_left.i.i193, align 8, !tbaa !8
  %_M_header.i.i192305 = getelementptr inbounds %struct.Error* %48, i64 0, i32 2, i32 0, i32 0, i32 1
  %cmp.i191306 = icmp eq %"struct.std::_Rb_tree_node_base"* %49, %_M_header.i.i192305
  br i1 %cmp.i191306, label %if.end150, label %for.body134.preheader

for.body134.preheader:                            ; preds = %if.then117
  br label %for.body134

for.body134:                                      ; preds = %for.body134.preheader, %invoke.cont141
  %50 = phi %"struct.std::_Rb_tree_node_base"* [ %call.i, %invoke.cont141 ], [ %49, %for.body134.preheader ]
  %_M_storage.i.i188 = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %50, i64 1
  %second136 = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %50, i64 1, i32 1
  %51 = bitcast %"struct.std::_Rb_tree_node_base"** %second136 to double*
  %52 = load double* %51, align 8, !tbaa !19
  %first139 = bitcast %"struct.std::_Rb_tree_node_base"* %_M_storage.i.i188 to i64*
  %53 = load i64* %first139, align 8, !tbaa !15
  %div140 = sdiv i64 %53, %conv121
  store i64 %div140, i64* %ref.tmp137, align 8, !tbaa !18
  %call142 = invoke dereferenceable(8) double* @_ZNSt3mapIldSt4lessIlESaISt4pairIKldEEEixEOl(%"class.std::map"* %result, i64* dereferenceable(8) %ref.tmp137)
          to label %invoke.cont141 unwind label %_ZNSt6vectorImSaImEED2Ev.exit222.nonloopexit.loopexit

invoke.cont141:                                   ; preds = %for.body134
  %54 = load double* %call142, align 8, !tbaa !20
  %add = fadd double %52, %54
  store double %add, double* %call142, align 8, !tbaa !20
  %call.i = call %"struct.std::_Rb_tree_node_base"* @_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"* %50) #10
  %55 = load %struct.Error** %_M_start.i194, align 8, !tbaa !7
  %_M_header.i.i192 = getelementptr inbounds %struct.Error* %55, i64 0, i32 2, i32 0, i32 0, i32 1
  %cmp.i191 = icmp eq %"struct.std::_Rb_tree_node_base"* %call.i, %_M_header.i.i192
  br i1 %cmp.i191, label %if.end150.loopexit316, label %for.body134

if.end150.loopexit:                               ; preds = %invoke.cont109
  br label %if.end150

if.end150.loopexit316:                            ; preds = %invoke.cont141
  br label %if.end150

if.end150.loopexit317:                            ; preds = %for.inc80
  br label %if.end150

if.end150.loopexit318:                            ; preds = %for.inc54
  br label %if.end150

if.end150.loopexit319:                            ; preds = %for.inc
  br label %if.end150

if.end150.loopexit320:                            ; preds = %for.inc26
  br label %if.end150

if.end150:                                        ; preds = %if.end150.loopexit320, %if.end150.loopexit319, %if.end150.loopexit318, %if.end150.loopexit317, %if.end150.loopexit316, %if.end150.loopexit, %if.then9, %if.then, %if.then31, %if.then61, %if.then117, %if.then85, %if.else114
  %_M_t.i183 = getelementptr inbounds %struct.Error* %agg.result, i64 0, i32 2, i32 0
  %_M_t2.i = getelementptr inbounds %"class.std::map"* %result, i64 0, i32 0
  %call.i184 = invoke dereferenceable(48) %"class.std::_Rb_tree"* @_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EEaSERKS8_(%"class.std::_Rb_tree"* %_M_t.i183, %"class.std::_Rb_tree"* dereferenceable(48) %_M_t2.i)
          to label %_ZNSt6vectorImSaImEED2Ev.exit unwind label %_ZNSt6vectorImSaImEED2Ev.exit222.nonloopexit.nonloopexit.nonloopexit.nonloopexit.nonloopexit.nonloopexit

_ZNSt6vectorImSaImEED2Ev.exit:                    ; preds = %if.end150
  %_M_parent.i.i.i182 = getelementptr inbounds %"class.std::map"* %result, i64 0, i32 0, i32 0, i32 1, i32 1
  %56 = load %"struct.std::_Rb_tree_node_base"** %_M_parent.i.i.i182, align 8, !tbaa !21
  %57 = bitcast %"struct.std::_Rb_tree_node_base"* %56 to %"struct.std::_Rb_tree_node"*
  call void @_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E(%"class.std::_Rb_tree"* %_M_t2.i, %"struct.std::_Rb_tree_node"* %57) #1
  call void @llvm.lifetime.end(i64 48, i8* %2) #1
  %_M_t.i = getelementptr inbounds %"class.std::map"* %temp, i64 0, i32 0
  %_M_parent.i.i.i = getelementptr inbounds %"class.std::map"* %temp, i64 0, i32 0, i32 0, i32 1, i32 1
  %58 = load %"struct.std::_Rb_tree_node_base"** %_M_parent.i.i.i, align 8, !tbaa !21
  %59 = bitcast %"struct.std::_Rb_tree_node_base"* %58 to %"struct.std::_Rb_tree_node"*
  call void @_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E(%"class.std::_Rb_tree"* %_M_t.i, %"struct.std::_Rb_tree_node"* %59) #1
  call void @llvm.lifetime.end(i64 48, i8* %0) #1
  ret void
}

; Function Attrs: nounwind
declare void @llvm.lifetime.start(i64, i8* nocapture) #1

declare i32 @__gxx_personality_v0(...)

; Function Attrs: uwtable
define linkonce_odr dereferenceable(8) double* @_ZNSt3mapIldSt4lessIlESaISt4pairIKldEEEixEOl(%"class.std::map"* %this, i64* dereferenceable(8) %__k) #0 align 2 {
entry:
  %_M_parent.i.i.i = getelementptr inbounds %"class.std::map"* %this, i64 0, i32 0, i32 0, i32 1, i32 1
  %0 = load %"struct.std::_Rb_tree_node_base"** %_M_parent.i.i.i, align 8, !tbaa !21
  %_M_header.i.i.i = getelementptr inbounds %"class.std::map"* %this, i64 0, i32 0, i32 0, i32 1
  %cmp912.i.i.i = icmp eq %"struct.std::_Rb_tree_node_base"* %0, null
  br i1 %cmp912.i.i.i, label %if.then, label %while.body.lr.ph.lr.ph.i.i.i

while.body.lr.ph.lr.ph.i.i.i:                     ; preds = %entry
  %1 = load i64* %__k, align 8, !tbaa !18
  br label %while.body.lr.ph.i.i.i

while.body.lr.ph.i.i.i:                           ; preds = %if.then.i.i.i, %while.body.lr.ph.lr.ph.i.i.i
  %__x.addr.0.ph14.i.in.i.i = phi %"struct.std::_Rb_tree_node_base"* [ %0, %while.body.lr.ph.lr.ph.i.i.i ], [ %4, %if.then.i.i.i ]
  %__y.addr.0.ph13.i.i.i = phi %"struct.std::_Rb_tree_node_base"* [ %_M_header.i.i.i, %while.body.lr.ph.lr.ph.i.i.i ], [ %__x.addr.010.i.in.i.i.lcssa, %if.then.i.i.i ]
  br label %while.body.i.i.i

while.body.i.i.i:                                 ; preds = %if.else.i.i.i, %while.body.lr.ph.i.i.i
  %__x.addr.010.i.in.i.i = phi %"struct.std::_Rb_tree_node_base"* [ %__x.addr.0.ph14.i.in.i.i, %while.body.lr.ph.i.i.i ], [ %6, %if.else.i.i.i ]
  %_M_storage.i.i.i.i.i.i = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %__x.addr.010.i.in.i.i, i64 1
  %first.i.i.i.i.i = bitcast %"struct.std::_Rb_tree_node_base"* %_M_storage.i.i.i.i.i.i to i64*
  %2 = load i64* %first.i.i.i.i.i, align 8, !tbaa !18
  %cmp.i.i.i.i = icmp slt i64 %2, %1
  br i1 %cmp.i.i.i.i, label %if.else.i.i.i, label %if.then.i.i.i

if.then.i.i.i:                                    ; preds = %while.body.i.i.i
  %__x.addr.010.i.in.i.i.lcssa = phi %"struct.std::_Rb_tree_node_base"* [ %__x.addr.010.i.in.i.i, %while.body.i.i.i ]
  %3 = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %__x.addr.010.i.in.i.i.lcssa, i64 0, i32 2
  %4 = load %"struct.std::_Rb_tree_node_base"** %3, align 8, !tbaa !22
  %cmp9.i.i.i = icmp eq %"struct.std::_Rb_tree_node_base"* %4, null
  br i1 %cmp9.i.i.i, label %_ZNSt3mapIldSt4lessIlESaISt4pairIKldEEE11lower_boundERS3_.exit.loopexit43, label %while.body.lr.ph.i.i.i

if.else.i.i.i:                                    ; preds = %while.body.i.i.i
  %5 = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %__x.addr.010.i.in.i.i, i64 0, i32 3
  %6 = load %"struct.std::_Rb_tree_node_base"** %5, align 8, !tbaa !23
  %cmp.i.i.i = icmp eq %"struct.std::_Rb_tree_node_base"* %6, null
  br i1 %cmp.i.i.i, label %_ZNSt3mapIldSt4lessIlESaISt4pairIKldEEE11lower_boundERS3_.exit.loopexit, label %while.body.i.i.i

_ZNSt3mapIldSt4lessIlESaISt4pairIKldEEE11lower_boundERS3_.exit.loopexit: ; preds = %if.else.i.i.i
  %__y.addr.0.ph13.i.i.i.lcssa45 = phi %"struct.std::_Rb_tree_node_base"* [ %__y.addr.0.ph13.i.i.i, %if.else.i.i.i ]
  br label %_ZNSt3mapIldSt4lessIlESaISt4pairIKldEEE11lower_boundERS3_.exit

_ZNSt3mapIldSt4lessIlESaISt4pairIKldEEE11lower_boundERS3_.exit.loopexit43: ; preds = %if.then.i.i.i
  %__x.addr.010.i.in.i.i.lcssa.lcssa = phi %"struct.std::_Rb_tree_node_base"* [ %__x.addr.010.i.in.i.i.lcssa, %if.then.i.i.i ]
  br label %_ZNSt3mapIldSt4lessIlESaISt4pairIKldEEE11lower_boundERS3_.exit

_ZNSt3mapIldSt4lessIlESaISt4pairIKldEEE11lower_boundERS3_.exit: ; preds = %_ZNSt3mapIldSt4lessIlESaISt4pairIKldEEE11lower_boundERS3_.exit.loopexit43, %_ZNSt3mapIldSt4lessIlESaISt4pairIKldEEE11lower_boundERS3_.exit.loopexit
  %__y.addr.0.ph.lcssa.i.i.i = phi %"struct.std::_Rb_tree_node_base"* [ %__y.addr.0.ph13.i.i.i.lcssa45, %_ZNSt3mapIldSt4lessIlESaISt4pairIKldEEE11lower_boundERS3_.exit.loopexit ], [ %__x.addr.010.i.in.i.i.lcssa.lcssa, %_ZNSt3mapIldSt4lessIlESaISt4pairIKldEEE11lower_boundERS3_.exit.loopexit43 ]
  %cmp.i = icmp eq %"struct.std::_Rb_tree_node_base"* %__y.addr.0.ph.lcssa.i.i.i, %_M_header.i.i.i
  br i1 %cmp.i, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %_ZNSt3mapIldSt4lessIlESaISt4pairIKldEEE11lower_boundERS3_.exit
  %_M_storage.i.i25 = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %__y.addr.0.ph.lcssa.i.i.i, i64 1
  %first = bitcast %"struct.std::_Rb_tree_node_base"* %_M_storage.i.i25 to i64*
  %7 = load i64* %first, align 8, !tbaa !18
  %cmp.i28 = icmp slt i64 %1, %7
  br i1 %cmp.i28, label %if.then, label %if.end

if.then:                                          ; preds = %entry, %lor.lhs.false, %_ZNSt3mapIldSt4lessIlESaISt4pairIKldEEE11lower_boundERS3_.exit
  %__y.addr.0.ph.lcssa.i.i.i34 = phi %"struct.std::_Rb_tree_node_base"* [ %__y.addr.0.ph.lcssa.i.i.i, %lor.lhs.false ], [ %_M_header.i.i.i, %_ZNSt3mapIldSt4lessIlESaISt4pairIKldEEE11lower_boundERS3_.exit ], [ %_M_header.i.i.i, %entry ]
  %_M_t = getelementptr inbounds %"class.std::map"* %this, i64 0, i32 0
  %call2.i.i.i.i.i = tail call noalias i8* @_Znwm(i64 48)
  %_M_storage.i.i.i.i = getelementptr inbounds i8* %call2.i.i.i.i.i, i64 32
  %first.i.i.i.i.i.i.i = bitcast i8* %_M_storage.i.i.i.i to i64*
  %8 = load i64* %__k, align 8, !tbaa !18
  store i64 %8, i64* %first.i.i.i.i.i.i.i, align 8, !tbaa !15
  %9 = getelementptr inbounds i8* %call2.i.i.i.i.i, i64 40
  %10 = bitcast i8* %9 to double*
  store double 0.000000e+00, double* %10, align 8, !tbaa !19
  %call11.i = tail call { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* } @_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS2_ERS1_(%"class.std::_Rb_tree"* %_M_t, %"struct.std::_Rb_tree_node_base"* %__y.addr.0.ph.lcssa.i.i.i34, i64* dereferenceable(8) %first.i.i.i.i.i.i.i)
  %11 = extractvalue { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* } %call11.i, 0
  %12 = extractvalue { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* } %call11.i, 1
  %tobool.i = icmp eq %"struct.std::_Rb_tree_node_base"* %12, null
  br i1 %tobool.i, label %if.end.i, label %if.then.i

if.then.i:                                        ; preds = %if.then
  %cmp.i.i = icmp ne %"struct.std::_Rb_tree_node_base"* %11, null
  %cmp2.i.i = icmp eq %"struct.std::_Rb_tree_node_base"* %_M_header.i.i.i, %12
  %or.cond.i.i = or i1 %cmp.i.i, %cmp2.i.i
  br i1 %or.cond.i.i, label %_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE14_M_insert_nodeEPSt18_Rb_tree_node_baseSA_PSt13_Rb_tree_nodeIS2_E.exit.i, label %lor.rhs.i.i

lor.rhs.i.i:                                      ; preds = %if.then.i
  %_M_storage.i.i.i.i.i = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %12, i64 1
  %first.i.i.i.i = bitcast %"struct.std::_Rb_tree_node_base"* %_M_storage.i.i.i.i.i to i64*
  %13 = load i64* %first.i.i.i.i, align 8, !tbaa !18
  %cmp.i.i.i22 = icmp slt i64 %8, %13
  br label %_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE14_M_insert_nodeEPSt18_Rb_tree_node_baseSA_PSt13_Rb_tree_nodeIS2_E.exit.i

_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE14_M_insert_nodeEPSt18_Rb_tree_node_baseSA_PSt13_Rb_tree_nodeIS2_E.exit.i: ; preds = %lor.rhs.i.i, %if.then.i
  %14 = phi i1 [ true, %if.then.i ], [ %cmp.i.i.i22, %lor.rhs.i.i ]
  %15 = bitcast i8* %call2.i.i.i.i.i to %"struct.std::_Rb_tree_node_base"*
  tail call void @_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_(i1 zeroext %14, %"struct.std::_Rb_tree_node_base"* %15, %"struct.std::_Rb_tree_node_base"* %12, %"struct.std::_Rb_tree_node_base"* dereferenceable(32) %_M_header.i.i.i) #1
  %_M_node_count.i.i = getelementptr inbounds %"class.std::map"* %this, i64 0, i32 0, i32 0, i32 2
  %16 = load i64* %_M_node_count.i.i, align 8, !tbaa !24
  %inc.i.i = add i64 %16, 1
  store i64 %inc.i.i, i64* %_M_node_count.i.i, align 8, !tbaa !24
  br label %if.end

if.end.i:                                         ; preds = %if.then
  tail call void @_ZdlPv(i8* %call2.i.i.i.i.i) #1
  br label %if.end

if.end:                                           ; preds = %if.end.i, %_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE14_M_insert_nodeEPSt18_Rb_tree_node_baseSA_PSt13_Rb_tree_nodeIS2_E.exit.i, %lor.lhs.false
  %__y.addr.0.ph.lcssa.i.i.i32 = phi %"struct.std::_Rb_tree_node_base"* [ %__y.addr.0.ph.lcssa.i.i.i, %lor.lhs.false ], [ %11, %if.end.i ], [ %15, %_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE14_M_insert_nodeEPSt18_Rb_tree_node_baseSA_PSt13_Rb_tree_nodeIS2_E.exit.i ]
  %second = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %__y.addr.0.ph.lcssa.i.i.i32, i64 1, i32 1
  %17 = bitcast %"struct.std::_Rb_tree_node_base"** %second to double*
  ret double* %17
}

; Function Attrs: nounwind
declare void @llvm.lifetime.end(i64, i8* nocapture) #1

; Function Attrs: nounwind readnone uwtable
define i32 @_Z7getTypev() #2 {
entry:
  ret i32 0
}

; Function Attrs: uwtable
define void @_Z9getErrRepRSt3mapIldSt4lessIlESaISt4pairIKldEEE(%struct.Error* noalias sret %agg.result, %"class.std::map"* readonly dereferenceable(48) %pmf) #0 {
entry:
  %_M_header.i.i.i.i = getelementptr inbounds %struct.Error* %agg.result, i64 0, i32 2, i32 0, i32 0, i32 1
  %_M_left.i.i.i.i.i = getelementptr inbounds %struct.Error* %agg.result, i64 0, i32 2, i32 0, i32 0, i32 1, i32 2
  %0 = bitcast %"struct.std::_Rb_tree_node_base"* %_M_header.i.i.i.i to i8*
  tail call void @llvm.memset.p0i8.i64(i8* %0, i8 0, i64 40, i32 8, i1 false) #1
  %1 = bitcast %struct.Error* %agg.result to i8*
  tail call void @llvm.memset.p0i8.i64(i8* %1, i8 0, i64 16, i32 8, i1 false) #1
  store %"struct.std::_Rb_tree_node_base"* %_M_header.i.i.i.i, %"struct.std::_Rb_tree_node_base"** %_M_left.i.i.i.i.i, align 8, !tbaa !25
  %_M_right.i.i.i.i.i = getelementptr inbounds %struct.Error* %agg.result, i64 0, i32 2, i32 0, i32 0, i32 1, i32 3
  store %"struct.std::_Rb_tree_node_base"* %_M_header.i.i.i.i, %"struct.std::_Rb_tree_node_base"** %_M_right.i.i.i.i.i, align 8, !tbaa !26
  %_M_t.i = getelementptr inbounds %struct.Error* %agg.result, i64 0, i32 2, i32 0
  %_M_t2.i = getelementptr inbounds %"class.std::map"* %pmf, i64 0, i32 0
  %call.i3 = invoke dereferenceable(48) %"class.std::_Rb_tree"* @_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EEaSERKS8_(%"class.std::_Rb_tree"* %_M_t.i, %"class.std::_Rb_tree"* dereferenceable(48) %_M_t2.i)
          to label %invoke.cont unwind label %lpad

invoke.cont:                                      ; preds = %entry
  %2 = bitcast %struct.Error* %agg.result to i8*
  call void @llvm.memset.p0i8.i64(i8* %2, i8 0, i64 16, i32 8, i1 false)
  ret void

lpad:                                             ; preds = %entry
  %3 = landingpad { i8*, i32 } personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*)
          cleanup
  %_M_parent.i.i.i.i = getelementptr inbounds %struct.Error* %agg.result, i64 0, i32 2, i32 0, i32 0, i32 1, i32 1
  %4 = load %"struct.std::_Rb_tree_node_base"** %_M_parent.i.i.i.i, align 8, !tbaa !21
  %5 = bitcast %"struct.std::_Rb_tree_node_base"* %4 to %"struct.std::_Rb_tree_node"*
  tail call void @_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E(%"class.std::_Rb_tree"* %_M_t.i, %"struct.std::_Rb_tree_node"* %5) #1
  resume { i8*, i32 } %3
}

; Function Attrs: uwtable
define linkonce_odr dereferenceable(48) %"class.std::_Rb_tree"* @_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EEaSERKS8_(%"class.std::_Rb_tree"* %this, %"class.std::_Rb_tree"* readonly dereferenceable(48) %__x) #0 align 2 {
entry:
  %__roan = alloca %"struct.std::_Rb_tree<long, std::pair<const long, double>, std::_Select1st<std::pair<const long, double> >, std::less<long>, std::allocator<std::pair<const long, double> > >::_Reuse_or_alloc_node", align 8
  %cmp = icmp eq %"class.std::_Rb_tree"* %this, %__x
  br i1 %cmp, label %if.end21, label %if.then

if.then:                                          ; preds = %entry
  %_M_root.i = getelementptr inbounds %"struct.std::_Rb_tree<long, std::pair<const long, double>, std::_Select1st<std::pair<const long, double> >, std::less<long>, std::allocator<std::pair<const long, double> > >::_Reuse_or_alloc_node"* %__roan, i64 0, i32 0
  %_M_parent.i.i = getelementptr inbounds %"class.std::_Rb_tree"* %this, i64 0, i32 0, i32 1, i32 1
  %0 = load %"struct.std::_Rb_tree_node_base"** %_M_parent.i.i, align 8, !tbaa !30
  store %"struct.std::_Rb_tree_node_base"* %0, %"struct.std::_Rb_tree_node_base"** %_M_root.i, align 8, !tbaa !31
  %_M_nodes.i = getelementptr inbounds %"struct.std::_Rb_tree<long, std::pair<const long, double>, std::_Select1st<std::pair<const long, double> >, std::less<long>, std::allocator<std::pair<const long, double> > >::_Reuse_or_alloc_node"* %__roan, i64 0, i32 1
  %_M_right.i.i = getelementptr inbounds %"class.std::_Rb_tree"* %this, i64 0, i32 0, i32 1, i32 3
  %1 = load %"struct.std::_Rb_tree_node_base"** %_M_right.i.i, align 8, !tbaa !30
  store %"struct.std::_Rb_tree_node_base"* %1, %"struct.std::_Rb_tree_node_base"** %_M_nodes.i, align 8, !tbaa !33
  %2 = getelementptr inbounds %"struct.std::_Rb_tree<long, std::pair<const long, double>, std::_Select1st<std::pair<const long, double> >, std::less<long>, std::allocator<std::pair<const long, double> > >::_Reuse_or_alloc_node"* %__roan, i64 0, i32 2
  store %"class.std::_Rb_tree"* %this, %"class.std::_Rb_tree"** %2, align 8, !tbaa !34
  %tobool.i = icmp eq %"struct.std::_Rb_tree_node_base"* %0, null
  br i1 %tobool.i, label %if.else.i, label %if.then.i

if.then.i:                                        ; preds = %if.then
  %_M_parent.i = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %0, i64 0, i32 1
  store %"struct.std::_Rb_tree_node_base"* null, %"struct.std::_Rb_tree_node_base"** %_M_parent.i, align 8, !tbaa !35
  %_M_left.i = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %1, i64 0, i32 2
  %3 = load %"struct.std::_Rb_tree_node_base"** %_M_left.i, align 8, !tbaa !22
  %tobool6.i = icmp eq %"struct.std::_Rb_tree_node_base"* %3, null
  br i1 %tobool6.i, label %_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE20_Reuse_or_alloc_nodeC2ERS8_.exit, label %if.then7.i

if.then7.i:                                       ; preds = %if.then.i
  store %"struct.std::_Rb_tree_node_base"* %3, %"struct.std::_Rb_tree_node_base"** %_M_nodes.i, align 8, !tbaa !33
  br label %_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE20_Reuse_or_alloc_nodeC2ERS8_.exit

if.else.i:                                        ; preds = %if.then
  store %"struct.std::_Rb_tree_node_base"* null, %"struct.std::_Rb_tree_node_base"** %_M_nodes.i, align 8, !tbaa !33
  br label %_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE20_Reuse_or_alloc_nodeC2ERS8_.exit

_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE20_Reuse_or_alloc_nodeC2ERS8_.exit: ; preds = %if.then.i, %if.then7.i, %if.else.i
  %_M_header.i = getelementptr inbounds %"class.std::_Rb_tree"* %this, i64 0, i32 0, i32 1
  store %"struct.std::_Rb_tree_node_base"* null, %"struct.std::_Rb_tree_node_base"** %_M_parent.i.i, align 8, !tbaa !36
  %_M_left.i29 = getelementptr inbounds %"class.std::_Rb_tree"* %this, i64 0, i32 0, i32 1, i32 2
  store %"struct.std::_Rb_tree_node_base"* %_M_header.i, %"struct.std::_Rb_tree_node_base"** %_M_left.i29, align 8, !tbaa !25
  store %"struct.std::_Rb_tree_node_base"* %_M_header.i, %"struct.std::_Rb_tree_node_base"** %_M_right.i.i, align 8, !tbaa !26
  %_M_node_count.i = getelementptr inbounds %"class.std::_Rb_tree"* %this, i64 0, i32 0, i32 2
  store i64 0, i64* %_M_node_count.i, align 8, !tbaa !37
  %_M_parent.i33 = getelementptr inbounds %"class.std::_Rb_tree"* %__x, i64 0, i32 0, i32 1, i32 1
  %4 = load %"struct.std::_Rb_tree_node_base"** %_M_parent.i33, align 8, !tbaa !21
  %cmp5 = icmp eq %"struct.std::_Rb_tree_node_base"* %4, null
  br i1 %cmp5, label %if.end, label %if.then6

if.then6:                                         ; preds = %_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE20_Reuse_or_alloc_nodeC2ERS8_.exit
  %5 = bitcast %"struct.std::_Rb_tree_node_base"* %4 to %"struct.std::_Rb_tree_node"*
  %call10 = invoke %"struct.std::_Rb_tree_node"* @_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE7_M_copyINS8_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS2_EPKSC_PSt18_Rb_tree_node_baseRT_(%"class.std::_Rb_tree"* %this, %"struct.std::_Rb_tree_node"* %5, %"struct.std::_Rb_tree_node_base"* %_M_header.i, %"struct.std::_Rb_tree<long, std::pair<const long, double>, std::_Select1st<std::pair<const long, double> >, std::less<long>, std::allocator<std::pair<const long, double> > >::_Reuse_or_alloc_node"* dereferenceable(24) %__roan)
          to label %invoke.cont9 unwind label %lpad

invoke.cont9:                                     ; preds = %if.then6
  %6 = getelementptr inbounds %"struct.std::_Rb_tree_node"* %call10, i64 0, i32 0
  store %"struct.std::_Rb_tree_node_base"* %6, %"struct.std::_Rb_tree_node_base"** %_M_parent.i.i, align 8, !tbaa !30
  br label %while.cond.i.i40

while.cond.i.i40:                                 ; preds = %while.cond.i.i40, %invoke.cont9
  %__x.addr.0.i.i38 = phi %"struct.std::_Rb_tree_node_base"* [ %6, %invoke.cont9 ], [ %7, %while.cond.i.i40 ]
  %_M_left.i.i = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %__x.addr.0.i.i38, i64 0, i32 2
  %7 = load %"struct.std::_Rb_tree_node_base"** %_M_left.i.i, align 8, !tbaa !22
  %cmp.i.i39 = icmp eq %"struct.std::_Rb_tree_node_base"* %7, null
  br i1 %cmp.i.i39, label %_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE10_S_minimumEPSt18_Rb_tree_node_base.exit, label %while.cond.i.i40

_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE10_S_minimumEPSt18_Rb_tree_node_base.exit: ; preds = %while.cond.i.i40
  %__x.addr.0.i.i38.lcssa = phi %"struct.std::_Rb_tree_node_base"* [ %__x.addr.0.i.i38, %while.cond.i.i40 ]
  store %"struct.std::_Rb_tree_node_base"* %__x.addr.0.i.i38.lcssa, %"struct.std::_Rb_tree_node_base"** %_M_left.i29, align 8, !tbaa !30
  br label %while.cond.i.i

while.cond.i.i:                                   ; preds = %while.cond.i.i, %_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE10_S_minimumEPSt18_Rb_tree_node_base.exit
  %__x.addr.0.i.i = phi %"struct.std::_Rb_tree_node_base"* [ %6, %_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE10_S_minimumEPSt18_Rb_tree_node_base.exit ], [ %8, %while.cond.i.i ]
  %_M_right.i.i35 = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %__x.addr.0.i.i, i64 0, i32 3
  %8 = load %"struct.std::_Rb_tree_node_base"** %_M_right.i.i35, align 8, !tbaa !23
  %cmp.i.i = icmp eq %"struct.std::_Rb_tree_node_base"* %8, null
  br i1 %cmp.i.i, label %_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE10_S_maximumEPSt18_Rb_tree_node_base.exit, label %while.cond.i.i

_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE10_S_maximumEPSt18_Rb_tree_node_base.exit: ; preds = %while.cond.i.i
  %__x.addr.0.i.i.lcssa = phi %"struct.std::_Rb_tree_node_base"* [ %__x.addr.0.i.i, %while.cond.i.i ]
  store %"struct.std::_Rb_tree_node_base"* %__x.addr.0.i.i.lcssa, %"struct.std::_Rb_tree_node_base"** %_M_right.i.i, align 8, !tbaa !30
  %_M_node_count = getelementptr inbounds %"class.std::_Rb_tree"* %__x, i64 0, i32 0, i32 2
  %9 = load i64* %_M_node_count, align 8, !tbaa !24
  store i64 %9, i64* %_M_node_count.i, align 8, !tbaa !24
  %.pre = load %"struct.std::_Rb_tree_node_base"** %_M_root.i, align 8, !tbaa !31
  br label %if.end

lpad:                                             ; preds = %if.then6
  %10 = landingpad { i8*, i32 } personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*)
          cleanup
  %ref.i31 = load %"class.std::_Rb_tree"** %2, align 8, !tbaa !34
  %11 = load %"struct.std::_Rb_tree_node_base"** %_M_root.i, align 8, !tbaa !31
  %12 = bitcast %"struct.std::_Rb_tree_node_base"* %11 to %"struct.std::_Rb_tree_node"*
  call void @_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E(%"class.std::_Rb_tree"* %ref.i31, %"struct.std::_Rb_tree_node"* %12) #1
  resume { i8*, i32 } %10

if.end:                                           ; preds = %_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE20_Reuse_or_alloc_nodeC2ERS8_.exit, %_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE10_S_maximumEPSt18_Rb_tree_node_base.exit
  %13 = phi %"struct.std::_Rb_tree_node_base"* [ %0, %_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE20_Reuse_or_alloc_nodeC2ERS8_.exit ], [ %.pre, %_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE10_S_maximumEPSt18_Rb_tree_node_base.exit ]
  %ref.i = load %"class.std::_Rb_tree"** %2, align 8, !tbaa !34
  %14 = bitcast %"struct.std::_Rb_tree_node_base"* %13 to %"struct.std::_Rb_tree_node"*
  call void @_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E(%"class.std::_Rb_tree"* %ref.i, %"struct.std::_Rb_tree_node"* %14) #1
  br label %if.end21

if.end21:                                         ; preds = %entry, %if.end
  ret %"class.std::_Rb_tree"* %this
}

; Function Attrs: uwtable
define linkonce_odr %"struct.std::_Rb_tree_node"* @_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE7_M_copyINS8_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS2_EPKSC_PSt18_Rb_tree_node_baseRT_(%"class.std::_Rb_tree"* %this, %"struct.std::_Rb_tree_node"* nocapture readonly %__x, %"struct.std::_Rb_tree_node_base"* %__p, %"struct.std::_Rb_tree<long, std::pair<const long, double>, std::_Select1st<std::pair<const long, double> >, std::less<long>, std::allocator<std::pair<const long, double> > >::_Reuse_or_alloc_node"* dereferenceable(24) %__node_gen) #0 align 2 {
entry:
  %_M_storage.i.i = getelementptr inbounds %"struct.std::_Rb_tree_node"* %__x, i64 0, i32 1
  %_M_nodes.i.i.i = getelementptr inbounds %"struct.std::_Rb_tree<long, std::pair<const long, double>, std::_Select1st<std::pair<const long, double> >, std::less<long>, std::allocator<std::pair<const long, double> > >::_Reuse_or_alloc_node"* %__node_gen, i64 0, i32 1
  %0 = load %"struct.std::_Rb_tree_node_base"** %_M_nodes.i.i.i, align 8, !tbaa !33
  %tobool.i.i.i = icmp eq %"struct.std::_Rb_tree_node_base"* %0, null
  br i1 %tobool.i.i.i, label %if.end.i.i, label %if.end.i.i.i

if.end.i.i.i:                                     ; preds = %entry
  %_M_parent.i.i.i = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %0, i64 0, i32 1
  %1 = load %"struct.std::_Rb_tree_node_base"** %_M_parent.i.i.i, align 8, !tbaa !35
  store %"struct.std::_Rb_tree_node_base"* %1, %"struct.std::_Rb_tree_node_base"** %_M_nodes.i.i.i, align 8, !tbaa !33
  %tobool7.i.i.i = icmp eq %"struct.std::_Rb_tree_node_base"* %1, null
  br i1 %tobool7.i.i.i, label %_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE20_Reuse_or_alloc_node10_M_extractEv.exit.thread15.i.i, label %if.then8.i.i.i

if.then8.i.i.i:                                   ; preds = %if.end.i.i.i
  %_M_right.i.i.i = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %1, i64 0, i32 3
  %2 = load %"struct.std::_Rb_tree_node_base"** %_M_right.i.i.i, align 8, !tbaa !23
  %cmp.i.i.i = icmp eq %"struct.std::_Rb_tree_node_base"* %2, %0
  br i1 %cmp.i.i.i, label %if.then10.i.i.i, label %if.else.i.i.i

if.then10.i.i.i:                                  ; preds = %if.then8.i.i.i
  store %"struct.std::_Rb_tree_node_base"* null, %"struct.std::_Rb_tree_node_base"** %_M_right.i.i.i, align 8, !tbaa !23
  %_M_left.i.i.i = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %1, i64 0, i32 2
  %3 = load %"struct.std::_Rb_tree_node_base"** %_M_left.i.i.i, align 8, !tbaa !22
  %tobool14.i.i.i = icmp eq %"struct.std::_Rb_tree_node_base"* %3, null
  br i1 %tobool14.i.i.i, label %if.then.i.i, label %while.cond.i.i.i.preheader

while.cond.i.i.i.preheader:                       ; preds = %if.then10.i.i.i
  br label %while.cond.i.i.i

while.cond.i.i.i:                                 ; preds = %while.cond.i.i.i.preheader, %while.cond.i.i.i
  %storemerge.i.i.i = phi %"struct.std::_Rb_tree_node_base"* [ %4, %while.cond.i.i.i ], [ %3, %while.cond.i.i.i.preheader ]
  %_M_right20.i.i.i = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %storemerge.i.i.i, i64 0, i32 3
  %4 = load %"struct.std::_Rb_tree_node_base"** %_M_right20.i.i.i, align 8, !tbaa !23
  %tobool21.i.i.i = icmp eq %"struct.std::_Rb_tree_node_base"* %4, null
  br i1 %tobool21.i.i.i, label %while.end.i.i.i, label %while.cond.i.i.i

while.end.i.i.i:                                  ; preds = %while.cond.i.i.i
  %storemerge.i.i.i.lcssa = phi %"struct.std::_Rb_tree_node_base"* [ %storemerge.i.i.i, %while.cond.i.i.i ]
  %_M_left26.i.i.i = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %storemerge.i.i.i.lcssa, i64 0, i32 2
  %5 = load %"struct.std::_Rb_tree_node_base"** %_M_left26.i.i.i, align 8, !tbaa !22
  %tobool27.i.i.i = icmp eq %"struct.std::_Rb_tree_node_base"* %5, null
  %storemerge..i.i.i = select i1 %tobool27.i.i.i, %"struct.std::_Rb_tree_node_base"* %storemerge.i.i.i.lcssa, %"struct.std::_Rb_tree_node_base"* %5
  store %"struct.std::_Rb_tree_node_base"* %storemerge..i.i.i, %"struct.std::_Rb_tree_node_base"** %_M_nodes.i.i.i, align 8, !tbaa !33
  br label %if.then.i.i

if.else.i.i.i:                                    ; preds = %if.then8.i.i.i
  %_M_left35.i.i.i = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %1, i64 0, i32 2
  store %"struct.std::_Rb_tree_node_base"* null, %"struct.std::_Rb_tree_node_base"** %_M_left35.i.i.i, align 8, !tbaa !22
  br label %if.then.i.i

_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE20_Reuse_or_alloc_node10_M_extractEv.exit.thread15.i.i: ; preds = %if.end.i.i.i
  %_M_root.i.i.i = getelementptr inbounds %"struct.std::_Rb_tree<long, std::pair<const long, double>, std::_Select1st<std::pair<const long, double> >, std::less<long>, std::allocator<std::pair<const long, double> > >::_Reuse_or_alloc_node"* %__node_gen, i64 0, i32 0
  store %"struct.std::_Rb_tree_node_base"* null, %"struct.std::_Rb_tree_node_base"** %_M_root.i.i.i, align 8, !tbaa !31
  br label %if.then.i.i

if.then.i.i:                                      ; preds = %_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE20_Reuse_or_alloc_node10_M_extractEv.exit.thread15.i.i, %if.else.i.i.i, %while.end.i.i.i, %if.then10.i.i.i
  %6 = bitcast %"struct.std::_Rb_tree_node_base"* %0 to %"struct.std::_Rb_tree_node"*
  %7 = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %0, i64 1
  %8 = bitcast %"struct.std::_Rb_tree_node_base"* %7 to i8*
  %9 = getelementptr inbounds %"struct.__gnu_cxx::__aligned_membuf"* %_M_storage.i.i, i64 0, i32 0, i64 0
  tail call void @llvm.memcpy.p0i8.p0i8.i64(i8* %8, i8* %9, i64 16, i32 8, i1 false) #1, !tbaa.struct !38
  br label %_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE13_M_clone_nodeINS8_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS2_EPKSC_RT_.exit

if.end.i.i:                                       ; preds = %entry
  %call2.i.i.i.i.i.i = tail call noalias i8* @_Znwm(i64 48)
  %10 = bitcast i8* %call2.i.i.i.i.i.i to %"struct.std::_Rb_tree_node"*
  %11 = getelementptr inbounds i8* %call2.i.i.i.i.i.i, i64 32
  %12 = getelementptr inbounds %"struct.__gnu_cxx::__aligned_membuf"* %_M_storage.i.i, i64 0, i32 0, i64 0
  tail call void @llvm.memcpy.p0i8.p0i8.i64(i8* %11, i8* %12, i64 16, i32 8, i1 false) #1, !tbaa.struct !38
  br label %_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE13_M_clone_nodeINS8_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS2_EPKSC_RT_.exit

_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE13_M_clone_nodeINS8_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS2_EPKSC_RT_.exit: ; preds = %if.then.i.i, %if.end.i.i
  %retval.0.i.i = phi %"struct.std::_Rb_tree_node"* [ %6, %if.then.i.i ], [ %10, %if.end.i.i ]
  %_M_color.i = getelementptr inbounds %"struct.std::_Rb_tree_node"* %__x, i64 0, i32 0, i32 0
  %13 = load i32* %_M_color.i, align 4, !tbaa !39
  %_M_color3.i = getelementptr inbounds %"struct.std::_Rb_tree_node"* %retval.0.i.i, i64 0, i32 0, i32 0
  store i32 %13, i32* %_M_color3.i, align 4, !tbaa !39
  %_M_left.i = getelementptr inbounds %"struct.std::_Rb_tree_node"* %retval.0.i.i, i64 0, i32 0, i32 2
  %14 = bitcast %"struct.std::_Rb_tree_node_base"** %_M_left.i to i8*
  tail call void @llvm.memset.p0i8.i64(i8* %14, i8 0, i64 16, i32 8, i1 false)
  %15 = getelementptr inbounds %"struct.std::_Rb_tree_node"* %retval.0.i.i, i64 0, i32 0
  %_M_parent = getelementptr inbounds %"struct.std::_Rb_tree_node"* %retval.0.i.i, i64 0, i32 0, i32 1
  store %"struct.std::_Rb_tree_node_base"* %__p, %"struct.std::_Rb_tree_node_base"** %_M_parent, align 8, !tbaa !35
  %_M_right = getelementptr inbounds %"struct.std::_Rb_tree_node"* %__x, i64 0, i32 0, i32 3
  %16 = load %"struct.std::_Rb_tree_node_base"** %_M_right, align 8, !tbaa !23
  %tobool = icmp eq %"struct.std::_Rb_tree_node_base"* %16, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE13_M_clone_nodeINS8_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS2_EPKSC_RT_.exit
  %17 = bitcast %"struct.std::_Rb_tree_node_base"* %16 to %"struct.std::_Rb_tree_node"*
  %call3 = tail call %"struct.std::_Rb_tree_node"* @_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE7_M_copyINS8_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS2_EPKSC_PSt18_Rb_tree_node_baseRT_(%"class.std::_Rb_tree"* %this, %"struct.std::_Rb_tree_node"* %17, %"struct.std::_Rb_tree_node_base"* %15, %"struct.std::_Rb_tree<long, std::pair<const long, double>, std::_Select1st<std::pair<const long, double> >, std::less<long>, std::allocator<std::pair<const long, double> > >::_Reuse_or_alloc_node"* dereferenceable(24) %__node_gen)
  %18 = getelementptr inbounds %"struct.std::_Rb_tree_node"* %call3, i64 0, i32 0
  %_M_right4 = getelementptr inbounds %"struct.std::_Rb_tree_node"* %retval.0.i.i, i64 0, i32 0, i32 3
  store %"struct.std::_Rb_tree_node_base"* %18, %"struct.std::_Rb_tree_node_base"** %_M_right4, align 8, !tbaa !23
  br label %if.end

if.end:                                           ; preds = %_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE13_M_clone_nodeINS8_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS2_EPKSC_RT_.exit, %if.then
  %_M_left.i38 = getelementptr inbounds %"struct.std::_Rb_tree_node"* %__x, i64 0, i32 0, i32 2
  %.sink72 = load %"struct.std::_Rb_tree_node_base"** %_M_left.i38, align 8
  %cmp73 = icmp eq %"struct.std::_Rb_tree_node_base"* %.sink72, null
  br i1 %cmp73, label %while.end, label %while.body.lr.ph

while.body.lr.ph:                                 ; preds = %if.end
  %_M_root.i.i.i62 = getelementptr inbounds %"struct.std::_Rb_tree<long, std::pair<const long, double>, std::_Select1st<std::pair<const long, double> >, std::less<long>, std::allocator<std::pair<const long, double> > >::_Reuse_or_alloc_node"* %__node_gen, i64 0, i32 0
  br label %while.body

while.body:                                       ; preds = %while.body.lr.ph, %if.end14
  %.sink75 = phi %"struct.std::_Rb_tree_node_base"* [ %.sink72, %while.body.lr.ph ], [ %.sink, %if.end14 ]
  %__p.addr.074 = phi %"struct.std::_Rb_tree_node_base"* [ %15, %while.body.lr.ph ], [ %35, %if.end14 ]
  %19 = phi %"struct.std::_Rb_tree_node"* [ %retval.0.i.i, %while.body.lr.ph ], [ %retval.0.i.i67, %if.end14 ]
  %_M_storage.i.i40 = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %.sink75, i64 1
  %20 = load %"struct.std::_Rb_tree_node_base"** %_M_nodes.i.i.i, align 8, !tbaa !33
  %tobool.i.i.i42 = icmp eq %"struct.std::_Rb_tree_node_base"* %20, null
  br i1 %tobool.i.i.i42, label %if.end.i.i66, label %if.end.i.i.i45

if.end.i.i.i45:                                   ; preds = %while.body
  %_M_parent.i.i.i43 = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %20, i64 0, i32 1
  %21 = load %"struct.std::_Rb_tree_node_base"** %_M_parent.i.i.i43, align 8, !tbaa !35
  store %"struct.std::_Rb_tree_node_base"* %21, %"struct.std::_Rb_tree_node_base"** %_M_nodes.i.i.i, align 8, !tbaa !33
  %tobool7.i.i.i44 = icmp eq %"struct.std::_Rb_tree_node_base"* %21, null
  br i1 %tobool7.i.i.i44, label %_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE20_Reuse_or_alloc_node10_M_extractEv.exit.thread15.i.i63, label %if.then8.i.i.i48

if.then8.i.i.i48:                                 ; preds = %if.end.i.i.i45
  %_M_right.i.i.i46 = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %21, i64 0, i32 3
  %22 = load %"struct.std::_Rb_tree_node_base"** %_M_right.i.i.i46, align 8, !tbaa !23
  %cmp.i.i.i47 = icmp eq %"struct.std::_Rb_tree_node_base"* %22, %20
  br i1 %cmp.i.i.i47, label %if.then10.i.i.i51, label %if.else.i.i.i61

if.then10.i.i.i51:                                ; preds = %if.then8.i.i.i48
  store %"struct.std::_Rb_tree_node_base"* null, %"struct.std::_Rb_tree_node_base"** %_M_right.i.i.i46, align 8, !tbaa !23
  %_M_left.i.i.i49 = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %21, i64 0, i32 2
  %23 = load %"struct.std::_Rb_tree_node_base"** %_M_left.i.i.i49, align 8, !tbaa !22
  %tobool14.i.i.i50 = icmp eq %"struct.std::_Rb_tree_node_base"* %23, null
  br i1 %tobool14.i.i.i50, label %if.then.i.i64, label %while.cond.i.i.i55.preheader

while.cond.i.i.i55.preheader:                     ; preds = %if.then10.i.i.i51
  br label %while.cond.i.i.i55

while.cond.i.i.i55:                               ; preds = %while.cond.i.i.i55.preheader, %while.cond.i.i.i55
  %storemerge.i.i.i52 = phi %"struct.std::_Rb_tree_node_base"* [ %24, %while.cond.i.i.i55 ], [ %23, %while.cond.i.i.i55.preheader ]
  %_M_right20.i.i.i53 = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %storemerge.i.i.i52, i64 0, i32 3
  %24 = load %"struct.std::_Rb_tree_node_base"** %_M_right20.i.i.i53, align 8, !tbaa !23
  %tobool21.i.i.i54 = icmp eq %"struct.std::_Rb_tree_node_base"* %24, null
  br i1 %tobool21.i.i.i54, label %while.end.i.i.i59, label %while.cond.i.i.i55

while.end.i.i.i59:                                ; preds = %while.cond.i.i.i55
  %storemerge.i.i.i52.lcssa = phi %"struct.std::_Rb_tree_node_base"* [ %storemerge.i.i.i52, %while.cond.i.i.i55 ]
  %_M_left26.i.i.i56 = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %storemerge.i.i.i52.lcssa, i64 0, i32 2
  %25 = load %"struct.std::_Rb_tree_node_base"** %_M_left26.i.i.i56, align 8, !tbaa !22
  %tobool27.i.i.i57 = icmp eq %"struct.std::_Rb_tree_node_base"* %25, null
  %storemerge..i.i.i58 = select i1 %tobool27.i.i.i57, %"struct.std::_Rb_tree_node_base"* %storemerge.i.i.i52.lcssa, %"struct.std::_Rb_tree_node_base"* %25
  store %"struct.std::_Rb_tree_node_base"* %storemerge..i.i.i58, %"struct.std::_Rb_tree_node_base"** %_M_nodes.i.i.i, align 8, !tbaa !33
  br label %if.then.i.i64

if.else.i.i.i61:                                  ; preds = %if.then8.i.i.i48
  %_M_left35.i.i.i60 = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %21, i64 0, i32 2
  store %"struct.std::_Rb_tree_node_base"* null, %"struct.std::_Rb_tree_node_base"** %_M_left35.i.i.i60, align 8, !tbaa !22
  br label %if.then.i.i64

_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE20_Reuse_or_alloc_node10_M_extractEv.exit.thread15.i.i63: ; preds = %if.end.i.i.i45
  store %"struct.std::_Rb_tree_node_base"* null, %"struct.std::_Rb_tree_node_base"** %_M_root.i.i.i62, align 8, !tbaa !31
  br label %if.then.i.i64

if.then.i.i64:                                    ; preds = %_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE20_Reuse_or_alloc_node10_M_extractEv.exit.thread15.i.i63, %if.else.i.i.i61, %while.end.i.i.i59, %if.then10.i.i.i51
  %26 = bitcast %"struct.std::_Rb_tree_node_base"* %20 to %"struct.std::_Rb_tree_node"*
  %27 = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %20, i64 1
  %28 = bitcast %"struct.std::_Rb_tree_node_base"* %27 to i8*
  %29 = bitcast %"struct.std::_Rb_tree_node_base"* %_M_storage.i.i40 to i8*
  tail call void @llvm.memcpy.p0i8.p0i8.i64(i8* %28, i8* %29, i64 16, i32 8, i1 false) #1, !tbaa.struct !38
  br label %_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE13_M_clone_nodeINS8_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS2_EPKSC_RT_.exit71

if.end.i.i66:                                     ; preds = %while.body
  %call2.i.i.i.i.i.i65 = tail call noalias i8* @_Znwm(i64 48)
  %30 = bitcast i8* %call2.i.i.i.i.i.i65 to %"struct.std::_Rb_tree_node"*
  %31 = getelementptr inbounds i8* %call2.i.i.i.i.i.i65, i64 32
  %32 = bitcast %"struct.std::_Rb_tree_node_base"* %_M_storage.i.i40 to i8*
  tail call void @llvm.memcpy.p0i8.p0i8.i64(i8* %31, i8* %32, i64 16, i32 8, i1 false) #1, !tbaa.struct !38
  br label %_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE13_M_clone_nodeINS8_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS2_EPKSC_RT_.exit71

_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE13_M_clone_nodeINS8_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS2_EPKSC_RT_.exit71: ; preds = %if.then.i.i64, %if.end.i.i66
  %retval.0.i.i67 = phi %"struct.std::_Rb_tree_node"* [ %26, %if.then.i.i64 ], [ %30, %if.end.i.i66 ]
  %_M_color.i68 = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %.sink75, i64 0, i32 0
  %33 = load i32* %_M_color.i68, align 4, !tbaa !39
  %_M_color3.i69 = getelementptr inbounds %"struct.std::_Rb_tree_node"* %retval.0.i.i67, i64 0, i32 0, i32 0
  store i32 %33, i32* %_M_color3.i69, align 4, !tbaa !39
  %_M_left.i70 = getelementptr inbounds %"struct.std::_Rb_tree_node"* %retval.0.i.i67, i64 0, i32 0, i32 2
  %34 = bitcast %"struct.std::_Rb_tree_node_base"** %_M_left.i70 to i8*
  tail call void @llvm.memset.p0i8.i64(i8* %34, i8 0, i64 16, i32 8, i1 false)
  %35 = getelementptr inbounds %"struct.std::_Rb_tree_node"* %retval.0.i.i67, i64 0, i32 0
  %_M_left = getelementptr inbounds %"struct.std::_Rb_tree_node"* %19, i64 0, i32 0, i32 2
  store %"struct.std::_Rb_tree_node_base"* %35, %"struct.std::_Rb_tree_node_base"** %_M_left, align 8, !tbaa !22
  %_M_parent7 = getelementptr inbounds %"struct.std::_Rb_tree_node"* %retval.0.i.i67, i64 0, i32 0, i32 1
  store %"struct.std::_Rb_tree_node_base"* %__p.addr.074, %"struct.std::_Rb_tree_node_base"** %_M_parent7, align 8, !tbaa !35
  %36 = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %.sink75, i64 0, i32 3
  %37 = load %"struct.std::_Rb_tree_node_base"** %36, align 8, !tbaa !23
  %tobool9 = icmp eq %"struct.std::_Rb_tree_node_base"* %37, null
  br i1 %tobool9, label %if.end14, label %if.then10

if.then10:                                        ; preds = %_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE13_M_clone_nodeINS8_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS2_EPKSC_RT_.exit71
  %38 = bitcast %"struct.std::_Rb_tree_node_base"* %37 to %"struct.std::_Rb_tree_node"*
  %call12 = tail call %"struct.std::_Rb_tree_node"* @_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE7_M_copyINS8_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS2_EPKSC_PSt18_Rb_tree_node_baseRT_(%"class.std::_Rb_tree"* %this, %"struct.std::_Rb_tree_node"* %38, %"struct.std::_Rb_tree_node_base"* %35, %"struct.std::_Rb_tree<long, std::pair<const long, double>, std::_Select1st<std::pair<const long, double> >, std::less<long>, std::allocator<std::pair<const long, double> > >::_Reuse_or_alloc_node"* dereferenceable(24) %__node_gen)
  %39 = getelementptr inbounds %"struct.std::_Rb_tree_node"* %call12, i64 0, i32 0
  %_M_right13 = getelementptr inbounds %"struct.std::_Rb_tree_node"* %retval.0.i.i67, i64 0, i32 0, i32 3
  store %"struct.std::_Rb_tree_node_base"* %39, %"struct.std::_Rb_tree_node_base"** %_M_right13, align 8, !tbaa !23
  br label %if.end14

if.end14:                                         ; preds = %_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE13_M_clone_nodeINS8_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS2_EPKSC_RT_.exit71, %if.then10
  %_M_left.i37 = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %.sink75, i64 0, i32 2
  %.sink = load %"struct.std::_Rb_tree_node_base"** %_M_left.i37, align 8
  %cmp = icmp eq %"struct.std::_Rb_tree_node_base"* %.sink, null
  br i1 %cmp, label %while.end.loopexit, label %while.body

while.end.loopexit:                               ; preds = %if.end14
  br label %while.end

while.end:                                        ; preds = %while.end.loopexit, %if.end
  ret %"struct.std::_Rb_tree_node"* %retval.0.i.i
}

; Function Attrs: nounwind uwtable
define linkonce_odr void @_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E(%"class.std::_Rb_tree"* nocapture readnone %this, %"struct.std::_Rb_tree_node"* %__x) #3 align 2 {
entry:
  %cmp6 = icmp eq %"struct.std::_Rb_tree_node"* %__x, null
  br i1 %cmp6, label %while.end, label %while.body.preheader

while.body.preheader:                             ; preds = %entry
  br label %while.body

while.body:                                       ; preds = %while.body.preheader, %while.body
  %__x.addr.07 = phi %"struct.std::_Rb_tree_node"* [ %3, %while.body ], [ %__x, %while.body.preheader ]
  %_M_right.i = getelementptr inbounds %"struct.std::_Rb_tree_node"* %__x.addr.07, i64 0, i32 0, i32 3
  %0 = load %"struct.std::_Rb_tree_node_base"** %_M_right.i, align 8, !tbaa !23
  %1 = bitcast %"struct.std::_Rb_tree_node_base"* %0 to %"struct.std::_Rb_tree_node"*
  tail call void @_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E(%"class.std::_Rb_tree"* %this, %"struct.std::_Rb_tree_node"* %1)
  %_M_left.i = getelementptr inbounds %"struct.std::_Rb_tree_node"* %__x.addr.07, i64 0, i32 0, i32 2
  %2 = load %"struct.std::_Rb_tree_node_base"** %_M_left.i, align 8, !tbaa !22
  %3 = bitcast %"struct.std::_Rb_tree_node_base"* %2 to %"struct.std::_Rb_tree_node"*
  %4 = bitcast %"struct.std::_Rb_tree_node"* %__x.addr.07 to i8*
  tail call void @_ZdlPv(i8* %4) #1
  %cmp = icmp eq %"struct.std::_Rb_tree_node_base"* %2, null
  br i1 %cmp, label %while.end.loopexit, label %while.body

while.end.loopexit:                               ; preds = %while.body
  br label %while.end

while.end:                                        ; preds = %while.end.loopexit, %entry
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #4

; Function Attrs: nobuiltin
declare noalias i8* @_Znwm(i64) #5

; Function Attrs: nounwind
declare void @llvm.memcpy.p0i8.p0i8.i64(i8* nocapture, i8* nocapture readonly, i64, i32, i1) #1

; Function Attrs: nounwind readonly uwtable
define linkonce_odr { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* } @_ZNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS2_ERS1_(%"class.std::_Rb_tree"* %this, %"struct.std::_Rb_tree_node_base"* %__position.coerce, i64* nocapture readonly dereferenceable(8) %__k) #6 align 2 {
entry:
  %_M_header.i = getelementptr inbounds %"class.std::_Rb_tree"* %this, i64 0, i32 0, i32 1
  %cmp = icmp eq %"struct.std::_Rb_tree_node_base"* %_M_header.i, %__position.coerce
  br i1 %cmp, label %if.then, label %if.else12

if.then:                                          ; preds = %entry
  %_M_node_count.i = getelementptr inbounds %"class.std::_Rb_tree"* %this, i64 0, i32 0, i32 2
  %0 = load i64* %_M_node_count.i, align 8, !tbaa !24
  %cmp5 = icmp eq i64 %0, 0
  br i1 %cmp5, label %if.else, label %land.lhs.true

land.lhs.true:                                    ; preds = %if.then
  %_M_right.i134 = getelementptr inbounds %"class.std::_Rb_tree"* %this, i64 0, i32 0, i32 1, i32 3
  %1 = load %"struct.std::_Rb_tree_node_base"** %_M_right.i134, align 8, !tbaa !30
  %_M_storage.i.i.i145 = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %1, i64 1
  %first.i.i146 = bitcast %"struct.std::_Rb_tree_node_base"* %_M_storage.i.i.i145 to i64*
  %2 = load i64* %first.i.i146, align 8, !tbaa !18
  %3 = load i64* %__k, align 8, !tbaa !18
  %cmp.i189 = icmp slt i64 %2, %3
  br i1 %cmp.i189, label %return, label %if.else

if.else:                                          ; preds = %if.then, %land.lhs.true
  %_M_parent.i.i154 = getelementptr inbounds %"class.std::_Rb_tree"* %this, i64 0, i32 0, i32 1, i32 1
  %.in47.i156 = load %"struct.std::_Rb_tree_node_base"** %_M_parent.i.i154, align 8
  %cmp48.i157 = icmp eq %"struct.std::_Rb_tree_node_base"* %.in47.i156, null
  br i1 %cmp48.i157, label %if.then.i173, label %while.body.lr.ph.i158

while.body.lr.ph.i158:                            ; preds = %if.else
  %4 = load i64* %__k, align 8, !tbaa !18
  br label %while.body.i168

while.body.i168:                                  ; preds = %while.body.i168, %while.body.lr.ph.i158
  %.in49.i159 = phi %"struct.std::_Rb_tree_node_base"* [ %.in47.i156, %while.body.lr.ph.i158 ], [ %.in.i166, %while.body.i168 ]
  %_M_storage.i.i.i32.i160 = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %.in49.i159, i64 1
  %first.i.i33.i161 = bitcast %"struct.std::_Rb_tree_node_base"* %_M_storage.i.i.i32.i160 to i64*
  %5 = load i64* %first.i.i33.i161, align 8, !tbaa !18
  %cmp.i31.i162 = icmp slt i64 %4, %5
  %_M_left.i30.i163 = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %.in49.i159, i64 0, i32 2
  %_M_right.i.i164 = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %.in49.i159, i64 0, i32 3
  %.in.in.be.i165 = select i1 %cmp.i31.i162, %"struct.std::_Rb_tree_node_base"** %_M_left.i30.i163, %"struct.std::_Rb_tree_node_base"** %_M_right.i.i164
  %.in.i166 = load %"struct.std::_Rb_tree_node_base"** %.in.in.be.i165, align 8
  %cmp.i167 = icmp eq %"struct.std::_Rb_tree_node_base"* %.in.i166, null
  br i1 %cmp.i167, label %while.end.i169, label %while.body.i168

while.end.i169:                                   ; preds = %while.body.i168
  %cmp.i31.i162.lcssa = phi i1 [ %cmp.i31.i162, %while.body.i168 ]
  %.lcssa = phi i64 [ %5, %while.body.i168 ]
  %.in49.i159.lcssa = phi %"struct.std::_Rb_tree_node_base"* [ %.in49.i159, %while.body.i168 ]
  br i1 %cmp.i31.i162.lcssa, label %if.then.i173, label %if.end12.i183

if.then.i173:                                     ; preds = %while.end.i169, %if.else
  %_M_header.i44.lcssa53.i170 = phi %"struct.std::_Rb_tree_node_base"* [ %.in49.i159.lcssa, %while.end.i169 ], [ %__position.coerce, %if.else ]
  %_M_left.i.i171 = getelementptr inbounds %"class.std::_Rb_tree"* %this, i64 0, i32 0, i32 1, i32 2
  %6 = load %"struct.std::_Rb_tree_node_base"** %_M_left.i.i171, align 8, !tbaa !8
  %cmp.i28.i172 = icmp eq %"struct.std::_Rb_tree_node_base"* %_M_header.i44.lcssa53.i170, %6
  br i1 %cmp.i28.i172, label %return, label %if.else.i175

if.else.i175:                                     ; preds = %if.then.i173
  %call.i.i174 = tail call %"struct.std::_Rb_tree_node_base"* @_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"* %_M_header.i44.lcssa53.i170) #10
  %.pre = load i64* %__k, align 8, !tbaa !18
  %_M_storage.i.i.i.i178.phi.trans.insert = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %call.i.i174, i64 1
  %first.i.i.i179.phi.trans.insert = bitcast %"struct.std::_Rb_tree_node_base"* %_M_storage.i.i.i.i178.phi.trans.insert to i64*
  %.pre210 = load i64* %first.i.i.i179.phi.trans.insert, align 8, !tbaa !18
  br label %if.end12.i183

if.end12.i183:                                    ; preds = %if.else.i175, %while.end.i169
  %7 = phi i64 [ %.pre210, %if.else.i175 ], [ %.lcssa, %while.end.i169 ]
  %8 = phi i64 [ %.pre, %if.else.i175 ], [ %4, %while.end.i169 ]
  %_M_header.i44.lcssa54.i176 = phi %"struct.std::_Rb_tree_node_base"* [ %_M_header.i44.lcssa53.i170, %if.else.i175 ], [ %.in49.i159.lcssa, %while.end.i169 ]
  %__j.sroa.0.0.43.i177 = phi %"struct.std::_Rb_tree_node_base"* [ %call.i.i174, %if.else.i175 ], [ %.in49.i159.lcssa, %while.end.i169 ]
  %cmp.i.i180 = icmp slt i64 %7, %8
  %_M_header.i44..i181 = select i1 %cmp.i.i180, %"struct.std::_Rb_tree_node_base"* %_M_header.i44.lcssa54.i176, %"struct.std::_Rb_tree_node_base"* null
  %.__j.sroa.0.0.43.i182 = select i1 %cmp.i.i180, %"struct.std::_Rb_tree_node_base"* null, %"struct.std::_Rb_tree_node_base"* %__j.sroa.0.0.43.i177
  br label %return

if.else12:                                        ; preds = %entry
  %_M_storage.i.i.i152 = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %__position.coerce, i64 1
  %first.i.i153 = bitcast %"struct.std::_Rb_tree_node_base"* %_M_storage.i.i.i152 to i64*
  %9 = load i64* %__k, align 8, !tbaa !18
  %10 = load i64* %first.i.i153, align 8, !tbaa !18
  %cmp.i151 = icmp slt i64 %9, %10
  br i1 %cmp.i151, label %if.then18, label %if.else44

if.then18:                                        ; preds = %if.else12
  %_M_left.i150 = getelementptr inbounds %"class.std::_Rb_tree"* %this, i64 0, i32 0, i32 1, i32 2
  %11 = load %"struct.std::_Rb_tree_node_base"** %_M_left.i150, align 8, !tbaa !30
  %cmp21 = icmp eq %"struct.std::_Rb_tree_node_base"* %11, %__position.coerce
  br i1 %cmp21, label %return, label %if.else25

if.else25:                                        ; preds = %if.then18
  %call.i144 = tail call %"struct.std::_Rb_tree_node_base"* @_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"* %__position.coerce) #10
  %_M_storage.i.i.i141 = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %call.i144, i64 1
  %first.i.i142 = bitcast %"struct.std::_Rb_tree_node_base"* %_M_storage.i.i.i141 to i64*
  %12 = load i64* %first.i.i142, align 8, !tbaa !18
  %cmp.i140 = icmp slt i64 %12, %9
  br i1 %cmp.i140, label %if.then32, label %if.else42

if.then32:                                        ; preds = %if.else25
  %_M_right.i139 = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %call.i144, i64 0, i32 3
  %13 = load %"struct.std::_Rb_tree_node_base"** %_M_right.i139, align 8, !tbaa !23
  %cmp35 = icmp eq %"struct.std::_Rb_tree_node_base"* %13, null
  %call.i144.__position.coerce = select i1 %cmp35, %"struct.std::_Rb_tree_node_base"* %call.i144, %"struct.std::_Rb_tree_node_base"* %__position.coerce
  %.__position.coerce = select i1 %cmp35, %"struct.std::_Rb_tree_node_base"* null, %"struct.std::_Rb_tree_node_base"* %__position.coerce
  br label %return

if.else42:                                        ; preds = %if.else25
  %_M_parent.i.i99 = getelementptr inbounds %"class.std::_Rb_tree"* %this, i64 0, i32 0, i32 1, i32 1
  %.in47.i101 = load %"struct.std::_Rb_tree_node_base"** %_M_parent.i.i99, align 8
  %cmp48.i102 = icmp eq %"struct.std::_Rb_tree_node_base"* %.in47.i101, null
  br i1 %cmp48.i102, label %if.then.i118, label %while.body.i113.preheader

while.body.i113.preheader:                        ; preds = %if.else42
  br label %while.body.i113

while.body.i113:                                  ; preds = %while.body.i113.preheader, %while.body.i113
  %.in49.i104 = phi %"struct.std::_Rb_tree_node_base"* [ %.in.i111, %while.body.i113 ], [ %.in47.i101, %while.body.i113.preheader ]
  %_M_storage.i.i.i32.i105 = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %.in49.i104, i64 1
  %first.i.i33.i106 = bitcast %"struct.std::_Rb_tree_node_base"* %_M_storage.i.i.i32.i105 to i64*
  %14 = load i64* %first.i.i33.i106, align 8, !tbaa !18
  %cmp.i31.i107 = icmp slt i64 %9, %14
  %_M_left.i30.i108 = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %.in49.i104, i64 0, i32 2
  %_M_right.i.i109 = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %.in49.i104, i64 0, i32 3
  %.in.in.be.i110 = select i1 %cmp.i31.i107, %"struct.std::_Rb_tree_node_base"** %_M_left.i30.i108, %"struct.std::_Rb_tree_node_base"** %_M_right.i.i109
  %.in.i111 = load %"struct.std::_Rb_tree_node_base"** %.in.in.be.i110, align 8
  %cmp.i112 = icmp eq %"struct.std::_Rb_tree_node_base"* %.in.i111, null
  br i1 %cmp.i112, label %while.end.i114, label %while.body.i113

while.end.i114:                                   ; preds = %while.body.i113
  %cmp.i31.i107.lcssa = phi i1 [ %cmp.i31.i107, %while.body.i113 ]
  %.lcssa215 = phi i64 [ %14, %while.body.i113 ]
  %.in49.i104.lcssa = phi %"struct.std::_Rb_tree_node_base"* [ %.in49.i104, %while.body.i113 ]
  br i1 %cmp.i31.i107.lcssa, label %if.then.i118, label %if.end12.i128

if.then.i118:                                     ; preds = %while.end.i114, %if.else42
  %_M_header.i44.lcssa53.i115 = phi %"struct.std::_Rb_tree_node_base"* [ %.in49.i104.lcssa, %while.end.i114 ], [ %_M_header.i, %if.else42 ]
  %cmp.i28.i117 = icmp eq %"struct.std::_Rb_tree_node_base"* %_M_header.i44.lcssa53.i115, %11
  br i1 %cmp.i28.i117, label %return, label %if.else.i120

if.else.i120:                                     ; preds = %if.then.i118
  %call.i.i119 = tail call %"struct.std::_Rb_tree_node_base"* @_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"* %_M_header.i44.lcssa53.i115) #10
  %_M_storage.i.i.i.i123.phi.trans.insert = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %call.i.i119, i64 1
  %first.i.i.i124.phi.trans.insert = bitcast %"struct.std::_Rb_tree_node_base"* %_M_storage.i.i.i.i123.phi.trans.insert to i64*
  %.pre211 = load i64* %first.i.i.i124.phi.trans.insert, align 8, !tbaa !18
  br label %if.end12.i128

if.end12.i128:                                    ; preds = %if.else.i120, %while.end.i114
  %15 = phi i64 [ %.pre211, %if.else.i120 ], [ %.lcssa215, %while.end.i114 ]
  %_M_header.i44.lcssa54.i121 = phi %"struct.std::_Rb_tree_node_base"* [ %_M_header.i44.lcssa53.i115, %if.else.i120 ], [ %.in49.i104.lcssa, %while.end.i114 ]
  %__j.sroa.0.0.43.i122 = phi %"struct.std::_Rb_tree_node_base"* [ %call.i.i119, %if.else.i120 ], [ %.in49.i104.lcssa, %while.end.i114 ]
  %cmp.i.i125 = icmp slt i64 %15, %9
  %_M_header.i44..i126 = select i1 %cmp.i.i125, %"struct.std::_Rb_tree_node_base"* %_M_header.i44.lcssa54.i121, %"struct.std::_Rb_tree_node_base"* null
  %.__j.sroa.0.0.43.i127 = select i1 %cmp.i.i125, %"struct.std::_Rb_tree_node_base"* null, %"struct.std::_Rb_tree_node_base"* %__j.sroa.0.0.43.i122
  br label %return

if.else44:                                        ; preds = %if.else12
  %cmp.i96 = icmp slt i64 %10, %9
  br i1 %cmp.i96, label %if.then50, label %return

if.then50:                                        ; preds = %if.else44
  %_M_right.i95 = getelementptr inbounds %"class.std::_Rb_tree"* %this, i64 0, i32 0, i32 1, i32 3
  %16 = load %"struct.std::_Rb_tree_node_base"** %_M_right.i95, align 8, !tbaa !30
  %cmp53 = icmp eq %"struct.std::_Rb_tree_node_base"* %16, %__position.coerce
  br i1 %cmp53, label %return, label %if.else57

if.else57:                                        ; preds = %if.then50
  %call.i = tail call %"struct.std::_Rb_tree_node_base"* @_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"* %__position.coerce) #10
  %_M_storage.i.i.i = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %call.i, i64 1
  %first.i.i = bitcast %"struct.std::_Rb_tree_node_base"* %_M_storage.i.i.i to i64*
  %17 = load i64* %first.i.i, align 8, !tbaa !18
  %cmp.i90 = icmp slt i64 %9, %17
  br i1 %cmp.i90, label %if.then64, label %if.else74

if.then64:                                        ; preds = %if.else57
  %_M_right.i = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %__position.coerce, i64 0, i32 3
  %18 = load %"struct.std::_Rb_tree_node_base"** %_M_right.i, align 8, !tbaa !23
  %cmp67 = icmp eq %"struct.std::_Rb_tree_node_base"* %18, null
  %__position.coerce.call.i = select i1 %cmp67, %"struct.std::_Rb_tree_node_base"* %__position.coerce, %"struct.std::_Rb_tree_node_base"* %call.i
  %.call.i = select i1 %cmp67, %"struct.std::_Rb_tree_node_base"* null, %"struct.std::_Rb_tree_node_base"* %call.i
  br label %return

if.else74:                                        ; preds = %if.else57
  %_M_parent.i.i = getelementptr inbounds %"class.std::_Rb_tree"* %this, i64 0, i32 0, i32 1, i32 1
  %.in47.i = load %"struct.std::_Rb_tree_node_base"** %_M_parent.i.i, align 8
  %cmp48.i = icmp eq %"struct.std::_Rb_tree_node_base"* %.in47.i, null
  br i1 %cmp48.i, label %if.then.i, label %while.body.i.preheader

while.body.i.preheader:                           ; preds = %if.else74
  br label %while.body.i

while.body.i:                                     ; preds = %while.body.i.preheader, %while.body.i
  %.in49.i = phi %"struct.std::_Rb_tree_node_base"* [ %.in.i, %while.body.i ], [ %.in47.i, %while.body.i.preheader ]
  %_M_storage.i.i.i32.i = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %.in49.i, i64 1
  %first.i.i33.i = bitcast %"struct.std::_Rb_tree_node_base"* %_M_storage.i.i.i32.i to i64*
  %19 = load i64* %first.i.i33.i, align 8, !tbaa !18
  %cmp.i31.i = icmp slt i64 %9, %19
  %_M_left.i30.i = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %.in49.i, i64 0, i32 2
  %_M_right.i.i = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %.in49.i, i64 0, i32 3
  %.in.in.be.i = select i1 %cmp.i31.i, %"struct.std::_Rb_tree_node_base"** %_M_left.i30.i, %"struct.std::_Rb_tree_node_base"** %_M_right.i.i
  %.in.i = load %"struct.std::_Rb_tree_node_base"** %.in.in.be.i, align 8
  %cmp.i = icmp eq %"struct.std::_Rb_tree_node_base"* %.in.i, null
  br i1 %cmp.i, label %while.end.i, label %while.body.i

while.end.i:                                      ; preds = %while.body.i
  %cmp.i31.i.lcssa = phi i1 [ %cmp.i31.i, %while.body.i ]
  %.lcssa216 = phi i64 [ %19, %while.body.i ]
  %.in49.i.lcssa = phi %"struct.std::_Rb_tree_node_base"* [ %.in49.i, %while.body.i ]
  br i1 %cmp.i31.i.lcssa, label %if.then.i, label %if.end12.i

if.then.i:                                        ; preds = %while.end.i, %if.else74
  %_M_header.i44.lcssa53.i = phi %"struct.std::_Rb_tree_node_base"* [ %.in49.i.lcssa, %while.end.i ], [ %_M_header.i, %if.else74 ]
  %_M_left.i.i = getelementptr inbounds %"class.std::_Rb_tree"* %this, i64 0, i32 0, i32 1, i32 2
  %20 = load %"struct.std::_Rb_tree_node_base"** %_M_left.i.i, align 8, !tbaa !8
  %cmp.i28.i = icmp eq %"struct.std::_Rb_tree_node_base"* %_M_header.i44.lcssa53.i, %20
  br i1 %cmp.i28.i, label %return, label %if.else.i

if.else.i:                                        ; preds = %if.then.i
  %call.i.i = tail call %"struct.std::_Rb_tree_node_base"* @_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"* %_M_header.i44.lcssa53.i) #10
  %_M_storage.i.i.i.i.phi.trans.insert = getelementptr inbounds %"struct.std::_Rb_tree_node_base"* %call.i.i, i64 1
  %first.i.i.i.phi.trans.insert = bitcast %"struct.std::_Rb_tree_node_base"* %_M_storage.i.i.i.i.phi.trans.insert to i64*
  %.pre212 = load i64* %first.i.i.i.phi.trans.insert, align 8, !tbaa !18
  br label %if.end12.i

if.end12.i:                                       ; preds = %if.else.i, %while.end.i
  %21 = phi i64 [ %.pre212, %if.else.i ], [ %.lcssa216, %while.end.i ]
  %_M_header.i44.lcssa54.i = phi %"struct.std::_Rb_tree_node_base"* [ %_M_header.i44.lcssa53.i, %if.else.i ], [ %.in49.i.lcssa, %while.end.i ]
  %__j.sroa.0.0.43.i = phi %"struct.std::_Rb_tree_node_base"* [ %call.i.i, %if.else.i ], [ %.in49.i.lcssa, %while.end.i ]
  %cmp.i.i = icmp slt i64 %21, %9
  %_M_header.i44..i = select i1 %cmp.i.i, %"struct.std::_Rb_tree_node_base"* %_M_header.i44.lcssa54.i, %"struct.std::_Rb_tree_node_base"* null
  %.__j.sroa.0.0.43.i = select i1 %cmp.i.i, %"struct.std::_Rb_tree_node_base"* null, %"struct.std::_Rb_tree_node_base"* %__j.sroa.0.0.43.i
  br label %return

return:                                           ; preds = %if.end12.i, %if.then.i, %if.end12.i128, %if.then.i118, %if.end12.i183, %if.then.i173, %if.then64, %if.then32, %if.else44, %if.then50, %if.then18, %land.lhs.true
  %22 = phi %"struct.std::_Rb_tree_node_base"* [ %1, %land.lhs.true ], [ %__position.coerce, %if.then18 ], [ %__position.coerce, %if.then50 ], [ null, %if.else44 ], [ %call.i144.__position.coerce, %if.then32 ], [ %__position.coerce.call.i, %if.then64 ], [ %_M_header.i44..i181, %if.end12.i183 ], [ %_M_header.i44.lcssa53.i170, %if.then.i173 ], [ %_M_header.i44..i126, %if.end12.i128 ], [ %11, %if.then.i118 ], [ %_M_header.i44..i, %if.end12.i ], [ %_M_header.i44.lcssa53.i, %if.then.i ]
  %retval.sroa.0.0..fca.0.load209 = phi %"struct.std::_Rb_tree_node_base"* [ null, %land.lhs.true ], [ %__position.coerce, %if.then18 ], [ null, %if.then50 ], [ %__position.coerce, %if.else44 ], [ %.__position.coerce, %if.then32 ], [ %.call.i, %if.then64 ], [ %.__j.sroa.0.0.43.i182, %if.end12.i183 ], [ null, %if.then.i173 ], [ %.__j.sroa.0.0.43.i127, %if.end12.i128 ], [ null, %if.then.i118 ], [ %.__j.sroa.0.0.43.i, %if.end12.i ], [ null, %if.then.i ]
  %.fca.0.insert = insertvalue { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* } undef, %"struct.std::_Rb_tree_node_base"* %retval.sroa.0.0..fca.0.load209, 0
  %.fca.1.insert = insertvalue { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* } %.fca.0.insert, %"struct.std::_Rb_tree_node_base"* %22, 1
  ret { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* } %.fca.1.insert
}

; Function Attrs: nounwind
declare void @_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_(i1 zeroext, %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* dereferenceable(32)) #7

; Function Attrs: nounwind readonly
declare %"struct.std::_Rb_tree_node_base"* @_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"*) #8

; Function Attrs: nounwind
declare void @llvm.memset.p0i8.i64(i8* nocapture, i8, i64, i32, i1) #1

; Function Attrs: nounwind readonly
declare %"struct.std::_Rb_tree_node_base"* @_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"*) #8

; Function Attrs: nounwind readnone
declare double @fmax(double, double) #9

attributes #0 = { uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind }
attributes #2 = { nounwind readnone uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nobuiltin nounwind "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nobuiltin "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind readonly uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nounwind "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { nounwind readonly "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #9 = { nounwind readnone "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #10 = { nounwind readonly }
attributes #11 = { nounwind readnone }

!llvm.ident = !{!0}

!0 = metadata !{metadata !"clang version 3.5.0 (tags/RELEASE_350/final)"}
!1 = metadata !{metadata !2, metadata !4, i64 8}
!2 = metadata !{metadata !"_ZTSSt12_Vector_baseI5ErrorSaIS0_EE", metadata !3, i64 0}
!3 = metadata !{metadata !"_ZTSNSt12_Vector_baseI5ErrorSaIS0_EE12_Vector_implE", metadata !4, i64 0, metadata !4, i64 8, metadata !4, i64 16}
!4 = metadata !{metadata !"any pointer", metadata !5, i64 0}
!5 = metadata !{metadata !"omnipotent char", metadata !6, i64 0}
!6 = metadata !{metadata !"Simple C/C++ TBAA"}
!7 = metadata !{metadata !2, metadata !4, i64 0}
!8 = metadata !{metadata !9, metadata !4, i64 24}
!9 = metadata !{metadata !"_ZTSSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE", metadata !10, i64 0}
!10 = metadata !{metadata !"_ZTSNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE13_Rb_tree_implIS6_Lb1EEE", metadata !11, i64 0, metadata !12, i64 8, metadata !14, i64 40}
!11 = metadata !{metadata !"_ZTSSt4lessIlE"}
!12 = metadata !{metadata !"_ZTSSt18_Rb_tree_node_base", metadata !13, i64 0, metadata !4, i64 8, metadata !4, i64 16, metadata !4, i64 24}
!13 = metadata !{metadata !"_ZTSSt14_Rb_tree_color", metadata !5, i64 0}
!14 = metadata !{metadata !"long", metadata !5, i64 0}
!15 = metadata !{metadata !16, metadata !14, i64 0}
!16 = metadata !{metadata !"_ZTSSt4pairIKldE", metadata !14, i64 0, metadata !17, i64 8}
!17 = metadata !{metadata !"double", metadata !5, i64 0}
!18 = metadata !{metadata !14, metadata !14, i64 0}
!19 = metadata !{metadata !16, metadata !17, i64 8}
!20 = metadata !{metadata !17, metadata !17, i64 0}
!21 = metadata !{metadata !9, metadata !4, i64 16}
!22 = metadata !{metadata !12, metadata !4, i64 16}
!23 = metadata !{metadata !12, metadata !4, i64 24}
!24 = metadata !{metadata !9, metadata !14, i64 40}
!25 = metadata !{metadata !10, metadata !4, i64 24}
!26 = metadata !{metadata !10, metadata !4, i64 32}
!27 = metadata !{metadata !28, metadata !4, i64 0}
!28 = metadata !{metadata !"_ZTSSt12_Vector_baseIlSaIlEE", metadata !29, i64 0}
!29 = metadata !{metadata !"_ZTSNSt12_Vector_baseIlSaIlEE12_Vector_implE", metadata !4, i64 0, metadata !4, i64 8, metadata !4, i64 16}
!30 = metadata !{metadata !4, metadata !4, i64 0}
!31 = metadata !{metadata !32, metadata !4, i64 0}
!32 = metadata !{metadata !"_ZTSNSt8_Rb_treeIlSt4pairIKldESt10_Select1stIS2_ESt4lessIlESaIS2_EE20_Reuse_or_alloc_nodeE", metadata !4, i64 0, metadata !4, i64 8, metadata !5, i64 16}
!33 = metadata !{metadata !32, metadata !4, i64 8}
!34 = metadata !{metadata !5, metadata !5, i64 0}
!35 = metadata !{metadata !12, metadata !4, i64 8}
!36 = metadata !{metadata !10, metadata !4, i64 16}
!37 = metadata !{metadata !10, metadata !14, i64 40}
!38 = metadata !{i64 0, i64 8, metadata !18, i64 8, i64 8, metadata !20}
!39 = metadata !{metadata !12, metadata !13, i64 0}
