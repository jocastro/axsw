#include "PMFGen.h"

//std::map<int, std::set<int> > ApproxPredictor::predictionBits;
//std::map<int, std::set<int> > ApproxPredictor::carryOutBits;

void ApproxPredictor::jointPorbability(int L, vec &I, vec &S, vec &R, double &Pr_int, int64_t *V) {
	std::set<int> R_U;
	std::vector<std::set<int> > S_G;
	std::set<int> G_I;
	std::vector< std::set<int> > G_D;
	std::map<int, std::set<int> > predBits = getPredictionBits(L, S, R);
	for (vec::iterator it = I.begin(); it != I.end(); ++it) {
		std::set<int> pred = predBits[*it];
		for (std::set<int>::iterator sIt = pred.begin(); sIt != pred.end(); ++sIt) {
			R_U.insert(*sIt);
		}	
	}
	//Pr_int = 0; //??? Pr[P; R_U]
	Pr_int = 1 / std::pow(2, R_U.size());
	std::map<int, std::set<int> > g_I = getCarryOutBits(L, I, S, R);
	for (std::map<int, std::set<int> >::iterator it = g_I.begin(); it != g_I.end(); ++it) {
		//dunno if thats correct
		std::set<int> temp;
		std::set_difference(it->second.begin(), it->second.end(), R_U.begin(), R_U.end(), std::inserter(temp, temp.begin()));
		it->second.clear();
		std::move(temp.begin(), temp.end(), std::inserter(it->second, it->second.begin()));
		if (it == g_I.begin()) {
			std::copy(it->second.begin(), it->second.end(), std::inserter(G_I, G_I.begin()));
		}
		else {
			//intersectSets(G_I, bits);
			temp.clear();
			std::set_intersection(it->second.begin(), it->second.end(), G_I.begin(), G_I.end(), std::inserter(temp, temp.begin()));
			G_I.clear();
			std::move(temp.begin(), temp.end(), std::inserter(G_I, G_I.begin()));
		}
	}
	S_G.push_back(G_I);
	//update prInt = prInt x Pr[G0;G_I]
	double prt = (std::pow(2, G_I.size()) - 1) / std::pow(2, G_I.size() + 1);
	Pr_int *= prt;
	for (vec::iterator it = I.begin(); it != I.end(); ++it) {
		std::set<int> g_i = g_I[*it];
		std::set<int> g_d;
		std::set_difference(g_i.begin(), g_i.end(), G_I.begin(), G_I.end(), std::inserter(g_d, g_d.begin()));
		if (!g_d.empty())
			G_D.push_back(g_d);
	}
	while (!G_D.empty()) {
		G_I = *G_D.begin();
		for (std::vector<std::set<int> >::iterator it = G_D.begin() + 1; it != G_D.end(); ++it) {
			std::set<int> t = *it;
			std::set<int> temp;
			std::set_intersection(t.begin(), t.end(), G_I.begin(), G_I.end(), std::inserter(temp, temp.begin()));
			G_I.clear();
			std::move(temp.begin(), temp.end(), std::inserter(G_I, G_I.begin()));
		}
		//update prInt = prInt x PR[G1;G_I]
		prt = 1 / std::pow(2, G_I.size()) + (std::pow(2, G_I.size()) - 1) / std::pow(2, G_I.size() + 1);
		Pr_int *= prt;
		S_G.push_back(G_I);
		std::vector<std::set<int> > temp;
		for (std::vector<std::set<int> >::iterator it = G_D.begin(); it != G_D.end(); ++it) {
			std::set<int> t = *it;
			std::set<int> g_d;
			std::set_difference(t.begin(), t.end(), G_I.begin(), G_I.end(), std::inserter(g_d, g_d.begin())); 
			if (!g_d.empty())
				temp.push_back(g_d);
		}
		G_D = temp;
	}

	if (V == nullptr)
		return;
	vec s_is;
	for (vec::iterator it = I.begin(); it != I.end(); ++it) {
		int s_i = 0;
		for (int i = 1; i < *it; ++i) {
			s_i += S[i - 1];
		}
		s_is.push_back(s_i);
	}
	*V = 0;
	for (std::vector<std::set<int> >::iterator it = S_G.begin(); it != S_G.end(); ++it) {
		std::set<int>::iterator iter = it->end();
		iter--;
		int g = *iter;
		int min = INT_MAX;
		int s = 0;
		for (int i = 0; i < s_is.size(); ++i) {
			int t = s_is[i] - g;
			if (t > 0 && t < min) {
				min = t;
				s = s_is[i];
			}
		}
		*V += std::pow(2, s);
	}
}

void ApproxPredictor::evaluatePr_I(int L, vec &I, vec &S, vec &R, double &PR_i, const double P_1) {
	vec L_range(L - 1);
	std::iota(L_range.begin(), L_range.end(), 2);
	vec J = vector_diff(L_range, I);
	vecVec P_J;
	findPowerSet(P_J, J);
	std::sort(P_J.begin(), P_J.end(), compareSize);
	double P_2 = 0;
	for (int j = 1; j <= J.size(); ++j) {
		vecVec::iterator start = findFirstOcc(P_J, j);
		vecVec::iterator end = findFirstOcc(P_J, j + 1);
		for (vecVec::iterator it = start; it != end; ++it) {
			double P_21 = 0;
			vec unionIP_Ji = vector_union(I, *it);
			jointPorbability(L, unionIP_Ji, S, R, P_21);
			P_2 += std::pow(-1, j + 1) * P_21;
		}
	}
	PR_i = P_1 - P_2;
}

double ApproxPredictor::evaluateProbabilityPMF(int L, vec &S, vec &R, std::map<int64_t, double> *PMF) {
	double errorProb = 0;
	vecVec powerSet;
	findPowerSet(powerSet, 2, L);
	std::sort(powerSet.begin(), powerSet.end(), compareSize);
	for (int i = 1; i < L; i++) {
		vecVec::iterator start = findFirstOcc(powerSet, i);
		vecVec::iterator end = findFirstOcc(powerSet, i + 1);
		for (vecVec::iterator it = start; it != end; ++it) {
			double probInt = 0;
			int64_t error = 0;
			if (PMF != nullptr) {
				jointPorbability(L, *it, S, R, probInt, &error);
				double PR_I = 0;
				evaluatePr_I(L, *it, S, R, PR_I, probInt);
				(*PMF)[error] += PR_I;
			}
			else {
				jointPorbability(L, *it, S, R, probInt);
			}
			errorProb += std::pow(-1, i + 1) * probInt; //ProbIntern
		}
	}
	if (PMF != nullptr)
		(*PMF)[0] = 1.0 - std::accumulate(PMF->begin(), PMF->end(), 0.0,
			[](const double previous, const std::pair<int64_t, double> &p) {return previous + p.second; });
	//predictionBits.clear();
	//carryOutBits.clear();
	return errorProb;
}