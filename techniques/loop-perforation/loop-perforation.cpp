#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <unistd.h>
#include <map>
#include <fstream>

#include <memory>
#include <vector>

#include "DFG.h"
#include "utils.h"
#include "utilsTabu.h"
#include "TabuSearch.h"
#include "ApproxSolution.h"
#include "ApproxUnit.h"

#include "json/value.h"
#include "json/reader.h"
#include "json/writer.h"

#include "llvm/IR/Module.h"
#include "llvm/ADT/IntrusiveRefCntPtr.h"
#include "llvm/ADT/StringMap.h"
#include "llvm/ADT/SmallVector.h"
#include "llvm/ADT/StringRef.h"

#include "llvm/IRReader/IRReader.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/Support/SourceMgr.h"
#include "llvm/Support/TargetSelect.h"
#include "llvm/Support/raw_ostream.h"



#include "clang/CodeGen/CodeGenAction.h"
#include "clang/Basic/DiagnosticOptions.h"
#include "clang/Driver/Compilation.h"
#include "clang/Driver/Driver.h"
#include "clang/Driver/Tool.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/CompilerInvocation.h"
#include "clang/Frontend/FrontendDiagnostic.h"
#include "clang/Frontend/TextDiagnosticPrinter.h"

#include "llvm/IR/LegacyPassManager.h"
#include "llvm/Transforms/IPO/PassManagerBuilder.h"
#include "llvm/Transforms/IPO/InlinerPass.h"
#include "llvm/Analysis/Passes.h"
#include "llvm/Transforms/IPO.h"
#include "llvm/Support/Host.h"

Json::Value apuJson;
static std::string jsonFileName = "approxUnits.json";

struct globalArgs_t {
    const char *inputFileName;  /* -i option */
    FILE *inputFile;
    const char *functionName;   /* -f option */
    int bitwidth;               /* -b option */
    const char *configFile;     /* -c option */
} globalArgs;

static const char *optString = "i:f:b:c:h?";

void display_usage( void )
{
    puts( "USAGE:\n");
    puts( "./PP -i <fileName> -f <functionName> -b <bitwidth> -c <configFileName>");

    exit( EXIT_FAILURE );
}

/*
* Custom Compiler optimization:
* It's almost an -03
*/

void printDFG(DFG &dfg, std::string fn, bool approx) {
	std::ofstream output(fn.c_str());

	if (output.is_open()) {
		dfg.DFG::printDFGDot(output, approx);
	}
	output.close();
}

bool parseJsonFile(std::string fileName) {
  std::ifstream ifile(fileName);
  std::string errors;
  std::stringstream buffer;
  buffer << ifile.rdbuf();
  std::string jsonText = buffer.str();
  Json::CharReaderBuilder builder;
  Json::CharReader *reader = builder.newCharReader();
  bool parsingSuccessful = false;
  parsingSuccessful = reader->parse(jsonText.c_str(), jsonText.c_str() + jsonText.size(), &apuJson, &errors);
  delete reader;
  if ( !parsingSuccessful )
  {
    std::cout << jsonText << std::endl;
    std::cout << errors << std::endl;
  }
  return parsingSuccessful;
}

void createApproxUnits(std::vector<std::shared_ptr<ApproxUnit> >& apuList, std::shared_ptr<ApproxUnit>& correctAdd) {
	apuList.push_back(std::make_shared<ApproxUnit>());
	correctAdd = apuList[0];

  if (!parseJsonFile(jsonFileName))
    std::cout << "COULD NOT READ JSON\n";
  for( Json::Value::const_iterator jsonItera = apuJson.begin() ; jsonItera != apuJson.end() ; jsonItera++) {
    Json::Value tmp = *jsonItera;
    std::string name = tmp["Name"].asString();
    int L = tmp["L"].asInt();
    int type = tmp["Type"].asInt();
    double i64_Delay = tmp["Speed"].asDouble();
    double i64_Area = tmp["Area"].asDouble();
    double i64_DynPower = tmp["Energy"].asDouble();
    double i64_StatPower = tmp["Energy"].asDouble();
    std::vector<int> paramsTempVector;
    for (Json::Value::ArrayIndex paramsItera = 0 ; paramsItera != tmp["Params"].size() ; paramsItera++){
      int paramsTemp = tmp["Params"][paramsItera].asInt();
      paramsTempVector.push_back(paramsTemp);
    }
    Json::Value pmfTmp = tmp["PMF"];
    //std::map<int64_t,double> pmf;
    std::shared_ptr<ApproxUnit> tmpAPU (new ApproxUnit(name, L, paramsTempVector, type, i64_Delay, i64_Area, i64_DynPower, i64_StatPower));
    for (Json::Value::ArrayIndex int64Itera = 0 ; int64Itera != pmfTmp["int64_t"].size() ; int64Itera++){
       int64_t intTemp = pmfTmp["int64_t"][int64Itera].asInt64();
       double doubleTemp = pmfTmp["double"][int64Itera].asDouble();
       tmpAPU->pmf.insert(std::pair<int64_t,double>(intTemp,doubleTemp));
    }
    //apuList.push_back(std::make_shared<ApproxUnit>(name, L, paramsTempVector, type, speed, area, energy, pmf));
    //std::shared_ptr<ApproxUnit> tmpShared(tmpAPU);
    apuList.push_back(tmpAPU);
  }

}

int customOptimization(llvm::Module& m) {
        llvm::legacy::PassManager pm;
        llvm::PassManagerBuilder pmb;
        pmb.OptLevel = 3;
        pmb.SizeLevel = 0;
        pmb.Inliner = llvm::createFunctionInliningPass(3, 0);
        pmb.LoopVectorize = true;
        pmb.SLPVectorize = true;
        pmb.populateModulePassManager(pm, true);
        pm.run(m);
        return 0;
}


std::unique_ptr<llvm::Module> compileToIR(std::string fileName) {
        std::string Path = "/home/moydan00/PP";
        std::string ClangPath = "/home/moydan00/clang3.5_bin/bin";
	std::string ClangIncludes = "home/moydan00/clang3.5_bin/include";

        // Arguments for C code compiling towards IR
	std::vector<const char *> args;
	args.push_back(ClangPath.c_str());
	args.push_back("-emit-llvm");
	args.push_back("-S");
	args.push_back("-o");
	args.push_back("compiled.bc");
	args.push_back("-fsyntax-only");
	args.push_back("-I");
	args.push_back(ClangIncludes.c_str());
	args.push_back(fileName.c_str());

    	/* Begins preparation for compiling options */
	llvm::IntrusiveRefCntPtr<clang::DiagnosticOptions> DiagOpts = new clang::DiagnosticOptions();
	clang::TextDiagnosticPrinter *DiagClient = new clang::TextDiagnosticPrinter(llvm::errs(), DiagOpts.get());
	llvm::IntrusiveRefCntPtr<clang::DiagnosticIDs> DiagID(new clang::DiagnosticIDs());
	clang::DiagnosticsEngine Diags(DiagID, DiagOpts.get(), DiagClient);

	std::string TripleStr = llvm::sys::getProcessTriple();
	llvm::Triple T(TripleStr);

	clang::driver::Driver TheDriver(Path, T.str(), Diags);
	TheDriver.setCheckInputsExist(false);
	std::unique_ptr<clang::driver::Compilation> compiler(TheDriver.BuildCompilation(args));
	if (!compiler)
		return 0;

	const clang::driver::JobList &Jobs = compiler->getJobs();
	if (Jobs.size() != 1 || !llvm::isa<clang::driver::Command>(*Jobs.begin())) {
		llvm::SmallString<256> Msg;
		llvm::raw_svector_ostream OS(Msg);
		Jobs.Print(OS, "; ", true);
		Diags.Report(clang::diag::err_fe_expected_compiler_job) << OS.str();
		return nullptr;
	}

	const clang::driver::Command *Cmd = llvm::cast<clang::driver::Command>(*Jobs.begin());
	if (llvm::StringRef(Cmd->getCreator().getName()) != "clang") {
		Diags.Report(clang::diag::err_fe_expected_clang_command);
		return nullptr;
	}
	/* Ends preparation for compiling options */


	// Initialize a compiler invocation object from the clang (-cc1) arguments.
	const clang::driver::ArgStringList &CCArgs = Cmd->getArguments();

	std::unique_ptr<clang::CompilerInvocation> compilerInv(new clang::CompilerInvocation);
	clang::CompilerInvocation::CreateFromArgs(*compilerInv, const_cast<const char **>(CCArgs.data()),
		const_cast<const char **>(CCArgs.data()) + CCArgs.size(), Diags);

	clang::CompilerInstance Clang;
	Clang.setInvocation(compilerInv.release());

	Clang.createDiagnostics();
	if (!Clang.hasDiagnostics())
		return nullptr;

	// Compile and return the corresponding Module
	std::unique_ptr<clang::CodeGenAction> Act(new clang::EmitLLVMOnlyAction(&llvm::getGlobalContext()));
	if (!Clang.ExecuteAction(*Act))
		return nullptr;
	std::unique_ptr<llvm::Module> t(Act->takeModule());
	t->materializeAll();
	return std::unique_ptr<llvm::Module>(std::move(t));
}


std::map<std::string, double> parseConfigFile(const char * i_filePath)
{
    std::ifstream in(i_filePath);
    std::string s, line;
    std::map<std::string, double> configParams;

    std::string COMMENT_START = "#";
    std::string SEPARATOR = "=";
    char * configKey;
    char * configVal;
    char * token;

    while(std::getline(in, line))
    {
        if (0 == line.find(COMMENT_START)) continue;

        char * lineChar = new char[line.length() + 1];
        strcpy(lineChar, line.c_str());

        configKey = strtok(lineChar, " =");
        configVal = strtok(NULL, " =");
        
        configParams[configKey] = atof(configVal);
    }
    
    // print configuration parameters
    /*for (std::map<std::string, double>::iterator it = configParams.begin();
           it != configParams.end(); ++it)
    {
        std::cout<< it->first << " : "<< it->second <<"\n";
    }*/
    
    return configParams;
}

int main(int argc, char *argv[]) {
  int opt = 0; // Current parsing option

  globalArgs.inputFileName = NULL;
  globalArgs.inputFile = NULL;
  globalArgs.functionName = NULL;
  globalArgs.bitwidth = 0; // False (no bitwidth given)
  globalArgs.configFile = NULL;

  while ((opt = getopt(argc, argv, optString)) != -1) {
    switch(opt) {
      case 'i':
        globalArgs.inputFileName = optarg;
        break;

      case 'f':
        globalArgs.functionName = optarg;
        break;

      case 'b':
        globalArgs.bitwidth = atoi(optarg);
        break;

      case 'c':
        globalArgs.configFile = optarg;
        break;

      case 'h':
      case '?': // Anything other than -i -f -b -c or -h
        display_usage();
        break;

      default: // NOT expected
        return 1;
    }
  }

  if (!globalArgs.inputFileName) {
    std::cerr << "Error: missing input <fileName>\n";
    display_usage();
    return 1;
  }

  std::cout << "File name: " << globalArgs.inputFileName << std::endl;

  if (!globalArgs.functionName) {
    std::cerr << "Error: missing input <functionName>\n";
    display_usage();
    return 1;
  }

  std::cout << "Function name: " << globalArgs.functionName << std::endl;

  if (!globalArgs.bitwidth) {
    std::cerr << "Error: missing input <bitwidth>\n";
    display_usage();
    return 1;
  }

  std::cout << "Bitwidth: " << globalArgs.bitwidth << std::endl;

  if (!globalArgs.configFile) {
    std::cerr << "Error: missing input <configFile>\n";
    display_usage();
    return 1;
  }
  std::cout << "Config File name: " << globalArgs.configFile << std::endl;

  std::map<std::string, double> configParams = parseConfigFile(globalArgs.configFile);

  /// Input function compilation with LLVM
  std::unique_ptr<llvm::Module> compiledPtr = nullptr;
  llvm::Module* compPtr = nullptr;

  compiledPtr = compileToIR(globalArgs.inputFileName);
  customOptimization(*compiledPtr.get());

  compPtr = compiledPtr.release();

  llvm::Function* func = getFunction(*compPtr, globalArgs.functionName);
  assert(func && "Function not loaded");

  // Data Function Graph (check DFG.cpp/.h)
  DFG dfg;

  // Link function arguments (used as abstract class llvm::Value) to bitwidths
  std::map<llvm::Value*, int> bitwidths;

  llvm::Function::arg_iterator argIt = func->arg_begin();
  argIt++;
  for (; argIt != func->arg_end(); ++argIt) {
    llvm::Value* val = llvm::dyn_cast<llvm::Value>(argIt);
    bitwidths[val] = globalArgs.bitwidth;
  }

  dfg.dfgGenerator(*func);
  dfg.computeBitwidths(bitwidths);

  //Genetic *ts = new Genetic(dfg, 50, 10);
  TabuSearch *ts = new TabuSearch(dfg, configParams["error"], configParams["error_min"]);
  createApproxUnits(ts->apuList, ts->correctAdd);
  
  ts->setApproxWeight(configParams["approx_weight"]);
  ts->setFUWeight(configParams["fu_weight"]);
  ts->setErrorWeight(configParams["error_weight"]);
  ts->setErrorMinWeight(configParams["error_min_weight"]);
  ts->setDelayWeight(configParams["delay_weight"]);
  ts->setAreaWeight(configParams["area_weight"]);
  ts->setDynPwrWeight(configParams["dyn_power_weight"]);
  ts->setStatPwrWeight(configParams["stat_power_weight"]);
  ts->setMinVisit(static_cast<int>(configParams["min_visit"]));
  ts->setVisitAF(static_cast<int>(configParams["visit_af"]));
  ts->setVisitIF(static_cast<int>(configParams["visit_if"]));
  ts->setMaxRun(static_cast<int64_t>(configParams["max_run"]));
  ts->setAfterBest(static_cast<int64_t>(configParams["after_best"]));

  ts->run();
  delete ts;

  printDFG(dfg, "DFG2.dot", true);

  return 0;
}

