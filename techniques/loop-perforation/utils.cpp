#include "utils.h"

// split() taken from:
// http://stackoverflow.com/questions/236129/splitting-a-string-in-c
std::vector<std::string> &split(const std::string &s, char delim,
	std::vector<std::string> &elems) {
	std::stringstream ss(s);
	std::string item;
	while (std::getline(ss, item, delim)) {
		elems.push_back(item);
	}
	return elems;
}

std::vector<std::string> split(const std::string &s, char delim) {
	std::vector<std::string> elems;
	split(s, delim, elems);
	return elems;
}

std::unique_ptr<llvm::Module> readIR(std::string name) {
	llvm::LLVMContext &c = llvm::getGlobalContext();
	llvm::SMDiagnostic Err;
	std::unique_ptr<llvm::Module> modPtr(llvm::ParseIRFile(name, Err, c));
	assert(modPtr != nullptr && "IR not found!");
	modPtr->materializeAll();
	bool veri = verifyModule(*modPtr);
	if (veri) {
		std::cerr << "Module " << name << " unverifiable\n";
		exit(1);
	}
	return std::unique_ptr<llvm::Module>(std::move(modPtr));
}

std::unique_ptr<llvm::ExecutionEngine> getExecutionEngine(llvm::Module *m,
	bool interpreter) {
	std::string ebError;
	llvm::EngineBuilder *eb = new llvm::EngineBuilder(m);
	assert(eb != nullptr && "No Enginbuilder Created");
	eb->setErrorStr(&ebError);
	if (!interpreter)
		eb->setUseMCJIT(true);
	else
		eb->setEngineKind(llvm::EngineKind::JIT);
	eb->setOptLevel(llvm::CodeGenOpt::Default);
	llvm::ExecutionEngine *ee = eb->create();
	assert(ee != nullptr && "Executionengine not created.");
	ee->finalizeObject();
	return std::unique_ptr<llvm::ExecutionEngine>(ee);
}

llvm::Function* getFunction(llvm::Module &m, std::string functionName) {
	llvm::Function *func = nullptr;
	std::string sFName = /*"_Z" + */std::to_string(functionName.length()) + functionName /*+ "PlS_"*/;
	for (llvm::Module::iterator it = m.begin(); it != m.end(); ++it) {
		llvm::Function *f = &*it;
		std::string fName(f->getName());
		if (fName.find(sFName) != std::string::npos || functionName == fName) {
			func = f;
			break;
		}
	}
	//func = m.getFunction(sFName);
	return func;
}

double computeTotalMean(std::map<int64_t, double> &pmf) {
	double sum = 0;
	for (std::map<int64_t, double>::iterator it = pmf.begin();
	it != pmf.end(); ++it) {
		sum += std::abs(it->first) * it->second;
	}
	return sum;
}

double computeErrorMean(std::map<int64_t, double> &pmf) {
	double errorProb = 1 - pmf[0];
	double sum = 0;
	std::map<int64_t, double>::iterator it = pmf.begin();
	for (; it != pmf.end(); ++it) {
		sum += std::abs(it->first) * (it->second / errorProb);
	}
	return sum;
}

/*void generatePMF(llvm::Instruction *instr/*std::shared_ptr<llvm::Instruction> &instr*//*, Error &err, int ErrType) {
	llvm::MDNode *meta = instr->getMetadata("approx");
	if (meta) {
		std::string type = llvm::dyn_cast<llvm::MDString>(meta->getOperand(0))->getString();
		int w = llvm::dyn_cast<llvm::ConstantInt>(meta->getOperand(1))->getSExtValue();
		int r = llvm::dyn_cast<llvm::ConstantInt>(meta->getOperand(2))->getSExtValue();
		int p = llvm::dyn_cast<llvm::ConstantInt>(meta->getOperand(3))->getSExtValue();
		std::vector<int> S;
		std::vector<int> R;
		int k;
		switch (str2int(type.c_str())) {
		case str2int("gear"):
			k = ((w - (r + p)) / r) + 1;
			S.push_back(r + p);
			for (int i = 1; i < k; ++i) {
				S.push_back(r);
				R.push_back(p);
			}
			break;
		}
		ApproxPredictor *approxer = new ApproxPredictor();
		approxer->evaluateProbabilityPMF(k, S, R, &err.pmf);
		err.err = 0;
		err.prob = 0;
		if (ErrType == 1) {
			err.err = computeTotalMean(err.pmf);
		}
		else if (ErrType == 2 ) {
			err.err = computeErrorMean(err.pmf);
			err.prob = 1 - err.pmf[0];
		}
		else if (ErrType == 3) {
			std::map<int64_t, double>::iterator it = err.pmf.end();
			it--;
			err.err = it->first;
			err.prob = 1 - err.pmf[0];
		}
		delete approxer;
	}
	else {
		err.err = 0;
		err.prob = 0;
		err.pmf[0] = 1;
	}
}*/
