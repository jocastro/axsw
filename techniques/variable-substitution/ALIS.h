#ifndef ALIS_H
#define ALIS_H

#include <iostream>
#include <cfloat>
#include <map>
#include "ApproxUnit.h"

class ALIS
{
    std::map<int, std::shared_ptr<ApproxUnit>> m_map_Solution;
    double m_i64_StatPowerAccum;
    double m_i64_DynPowerAccum;
    double m_i64_AreaAccum;
    double m_i64_DelayAccum;

    const double ai64_DYN_POWER_ESTIMATE_COEFFS[8] =
            {-11.51,
            176.30,
            -1083.00,
            3457.00,
            -6205.00,
            6301.00,
            -3371.00,
            737.20};
    
  public:
    ALIS(std::map<int, std::shared_ptr<ApproxUnit>> i_map_Solution);
    ~ALIS() {};
    
    double EstimateStatPower();
    double EstimateDynPower(double i_i64_DynPowerAccumPrec);
    double EstimateArea();
    double EstimateDelay();
    double GetDynPowerAccum();
};

#endif // #ifndef ALIS_H
