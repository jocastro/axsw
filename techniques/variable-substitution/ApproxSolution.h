#ifndef APPROX_SOLUTION_H
#define APPROX_SOLUTION_H

#include <map>
#include <vector>
#include <set>
#include "utils.h"
#include "utilsTabu.h"
#include "ApproxUnit.h"
#include <memory>
#include <iostream>



struct approxFUsEntry {
	int schedStep;
	int instr;
	approxFUsEntry(int sched, int instr) : schedStep(sched), instr(instr) {}
};

struct aFUsEcomp {
	bool operator() (const approxFUsEntry &lhs, const approxFUsEntry &rhs) const {
		return lhs.instr < rhs.instr;
	}
};

typedef std::set <approxFUsEntry, aFUsEcomp> approxFUset;

struct FU {
	approxFUset FUinstrs;
	int FU_ID;
};

class ApproxSolution {
private:
public:
	ApproxSolution():
	approximations(0), approxFU(0), hash(0), fitness(0), err(0) {}

	size_t hash;
	int approximations;
	int approxFU;
	std::vector<std::map<int64_t, double> > PMFs;
	//std::map<int, std::string> solution;
	std::map<int, std::shared_ptr<ApproxUnit> > solution;
	double fitness;
	double err;

    // Circuit parameters values in their respective units as provided by
    // the synthesis tool, estimated from the individual APU's circuit
    // parameters.
    // Dynamic power estimation is already normalized.
    double m_i64_Delay;
    double m_i64_Area;
    double m_i64_StatPower;
    double m_i64_DynPowerNorm;

    // Dynamic power accumulate used for approximate solution dynamic power
    // estimation
    double m_i64_DynPowerAccum;

	std::map<std::shared_ptr<ApproxUnit>, std::vector<FU> > approxFUs;

	std::map<std::shared_ptr<ApproxUnit>, int> apuIDs;

	std::vector<FU>::iterator getFU(std::shared_ptr<ApproxUnit> function, int FU_ID) {
		std::vector<FU> &fus = approxFUs[function];
		for (std::vector<FU>::iterator it = fus.begin();
			it != fus.end(); ++it) {
			if (it->FU_ID == FU_ID)
				return it;
		}
		return fus.end();
	}

	int getFUIDByInstr(std::shared_ptr<ApproxUnit> function, int instr) {
		std::vector<FU> &fus = approxFUs[function];
		for (std::vector<FU>::iterator it = fus.begin();
			it != fus.end(); ++it) {
			if (it->FUinstrs.find(approxFUsEntry(-1, instr)) != it->FUinstrs.end())
				return it->FU_ID;
		}
		return -1;
	}

	std::vector<FU>::iterator getFUitByInstr(std::shared_ptr<ApproxUnit> function, int instr) {
		std::vector<FU> &fus = approxFUs[function];
		for (std::vector<FU>::iterator it = fus.begin();
			it != fus.end(); ++it) {
			if (it->FUinstrs.find(approxFUsEntry(-1, instr)) != it->FUinstrs.end())
				return it;
		}
		return fus.end();
	}

	bool removeFromFU(std::shared_ptr<ApproxUnit> apu, int instr) {
		std::vector<FU>::iterator fu = getFUitByInstr(apu, instr);
		bool found;
		approxFUset::iterator instrIt = fu->FUinstrs.find(approxFUsEntry(-1, instr));
		if (instrIt != fu->FUinstrs.end())
			fu->FUinstrs.erase(instrIt);
		else
			return false;
		if (fu->FUinstrs.empty()) {
			approxFU--;
			approxFUs[apu].erase(fu);
		}
		return true;
	}
	
	bool addToFU(std::shared_ptr<ApproxUnit> apu, int sched, int instr, int FU_ID) {
		if (FU_ID == -1) {
			std::vector<FU> &FUs = approxFUs[apu];
			FU newFU;
			newFU.FU_ID = apuIDs[apu];
			apuIDs[apu]++;
			newFU.FUinstrs.emplace(sched, instr);
			FUs.push_back(newFU);
			approxFU++;
		}
		else {
			FU &fu = *getFU(apu, FU_ID);

			if (checkForSched(fu, sched))
				return false;

			fu.FUinstrs.emplace(sched, instr);
		}
		return true;
	}

	bool checkForSched(FU &fu, int schedStep) {
		for (approxFUset::iterator it = fu.FUinstrs.begin();
			it != fu.FUinstrs.end(); ++it) {
			if (it->schedStep == schedStep) {
				return true;
			}
		}
		return false;
	}

	bool operator==(const ApproxSolution &sol) const {
		return hash == sol.hash;
	}

	void printSolInfo(std::ostream& out = std::cout) {
		//out << "HASH: " << hash << " fitness: " << fitness << " err: "
	//		<< err << " FUs: " << approxFU << " Ap: " << approximations << " Delay: " << std::to_string(m_i64_Delay) << " Area: " <<
	//		std::to_string(m_i64_Area) << " Dynamic Power: " << std::to_string(m_i64_DynPower) <<"\n";
	}

	void printAllocation(std::ostream& out = std::cout) {
		for (std::map<std::shared_ptr<ApproxUnit>, std::vector<FU> >::iterator ait = approxFUs.begin();
			ait != approxFUs.end(); ++ait) {
			if (ait->second.size() == 0)
				continue;

			out << ait->first->getName();
			out << ":\n";

			for (std::vector<FU>::iterator FUit = ait->second.begin();
				FUit != ait->second.end(); FUit++) {
				out << FUit->FU_ID << ": ";
				for (std::set<approxFUsEntry, aFUsEcomp>::iterator it = FUit->FUinstrs.begin();
					it != FUit->FUinstrs.end(); ++it)
					out << it->instr << "/" << it->schedStep << ",";
				out << "\n";
			}
			out << "\n";
		}
	}

	void printAPUs() {
		for (std::map<std::shared_ptr<ApproxUnit>, std::vector<FU> >::iterator ait = approxFUs.begin();
			ait != approxFUs.end(); ++ait) {
			if (ait->second.size() == 0)
				continue;

			std::cout << ait->first->getName();
			std::cout << " L: " << ait->first->L;
			std::cout << " type: " << ait->first->type;
			std::cout << " Delay: " << ait->first->m_i64_Delay;
			std::cout << " Area: " << ait->first->m_i64_Area;
			std::cout << " Dynamic Power: " << ait->first->m_i64_DynPower;
            std::cout << " Static Power: " << ait->first->m_i64_StatPower;
			std::cout << "\n";
		}
	}
};

template<class T> class ApproxSolutionHash;
template<>
class ApproxSolutionHash<ApproxSolution> {
	public:
	size_t operator()(const ApproxSolution &as) const {
		//TODO Hashfunctions!
		size_t h = 0;

		MyTemplatePointerHash1<ApproxUnit> apu_h;

		hash_combine<int>(h, as.approximations);
		hash_combine<int>(h, as.approxFU);

		size_t sol_hash;
		bool first = true;
		for (std::map<int, std::shared_ptr<ApproxUnit> >::const_iterator it = as.solution.begin();
			it != as.solution.end(); ++it) {
			hash_combine<int>(h, it->first);
			hash_combine<std::shared_ptr<ApproxUnit> >(h, it->second);
		}
		return h;
	}
};

#endif // APPROX_SOLUTION_H 
