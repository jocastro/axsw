#ifndef APPROX_PREDICTION_H
#define APPROX_PREDICTION_H

#include <vector>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <map>
#include <set>
#include <iterator>
#include <string>
#include <climits>
#include <numeric>

class ApproxPredictor{
	typedef std::vector<int> vec;
	typedef std::vector<vec> vecVec;

public:

	double evaluateProbabilityPMF(int L, vec &S, vec &R, std::map<int64_t, double> *PMF = nullptr);
	~ApproxPredictor() {
		predictionBits.clear();
		carryOutBits.clear();
	}

private:
	void evaluatePr_I(int L, vec &I, vec &S, vec &R, double &PR_i, const double P_1);
	void jointPorbability(int L, vec &I, vec &S, vec &R, double &Pr_int, int64_t *V = nullptr);

	std::map<int, std::set<int> > predictionBits;
	std::map<int, std::set<int> > carryOutBits;

	std::map<int, std::set<int> > getPredictionBits(int L, vec &S, vec &R) {
		if (!predictionBits.empty()) 
			return predictionBits;
		std::map<int, std::set<int> > temp;
		for (int i = 2; i <= L; ++i) {
			int sumL = 0;
			int sumH = 0;
			for (int k = 1; k < i; ++k) {
				sumL += S[k - 1];
			}
			sumH = sumL - 1;
			sumL -= R[i - 2];
			std::set<int> bits;
			for (int x = sumL; x <= sumH; ++x) {
				bits.insert(x);
			}
			temp[i] = bits;
		}
		predictionBits = temp;
		return predictionBits;
	}

	std::map<int, std::set<int> > getCarryOutBits(int L, vec& I, vec &S, vec&R) {
		if (carryOutBits.empty()) {
			std::map<int, std::set<int> > temp;
			for (int i = 2; i <= L; ++i) {
				std::set<int> bits;
				int sumSK = 0;
				for (int k = 1; k < i; ++k) {
					sumSK += S[k - 1];
				}
				std::set<int> pred = predictionBits[i];
				//dunno if thats correct
				for (int k = 0; k </*<=*/ sumSK; ++k) {
					if (pred.find(k) == pred.end()) {
						bits.insert(k);
					}
				}
				temp[i] = bits;
			}
			carryOutBits = temp;
		}
		std::map<int, std::set<int> > result;
		for (vec::iterator it = I.begin(); it != I.end(); ++it) {
			result[*it] = carryOutBits[*it];
		}
		return result;
	}

	struct {
		bool operator()(const vec &a, const vec &b) {
			return a.size() < b.size();
		}
	}compareSize;

	vecVec::iterator findFirstOcc(vecVec &in, int size) {
		for (vecVec::iterator it = in.begin(); it != in.end(); ++it) {
			if (it->size() == size) {
				return it;
			}
		}
		return in.end();
	}

	char getBit(int64_t value, int pos) {
		if (pos < 0) return 0;
		return ((value >> pos) & 1u);
	}

	void findPowerSet(vecVec &result, int start, int end) {
		result.clear();
		int bits = end - (start - 1);
		int powerSetSize = std::pow(2, bits);
		for (int i = 1; i < powerSetSize; i++) {
			vec subset;
			for (int j = 0; j < bits; j++) {
				if (getBit(i, j) == 1) {
					subset.push_back(start + j);
				}
			}
			result.push_back(subset);
		}
	}

	void findPowerSet(vecVec &result, vec &s) {
		int bits = s.size();
		int powerSetSize = std::pow(2, bits);
		for (int i = 1; i < powerSetSize; i++) {
			vec subset;
			for (int j = 0; j < bits; j++) {
				if (getBit(i, j) == 1) {
					subset.push_back(s[j]);
				}
			}
			result.push_back(subset);
		}
	}

	vec vector_diff(const vec &tv1, const vec &tv2) {
		std::set<int> t1(tv1.begin(), tv1.end());
		std::set<int> t2(tv2.begin(), tv2.end());
		vec result;
		std::set_difference(t1.begin(), t1.end(), t2.begin(), t2.end(), std::back_inserter(result));
		return result;
	}

	vec vector_union(const vec &tv1, const vec &tv2) {
		std::set<int> t1(tv1.begin(), tv1.end());
		std::set<int> t2(tv2.begin(), tv2.end());
		vec result;
		std::set_union(t1.begin(), t1.end(), t2.begin(), t2.end(), std::back_inserter(result));
		return result;
	}

	int maxSet(std::set<int> &a) {
		int max = 0;
		for (std::set<int>::iterator it = a.begin(); it != a.end(); ++it) {
			max = *it > max ? *it : max;
		}
		return max;
	}
};


#endif