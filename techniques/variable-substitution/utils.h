#ifndef APPROX_UTILS_H
#define APPROX_UTILS_H
#include <vector>
#include <sstream>
#include <memory>
#include <iostream>

#include "llvm/IRReader/IRReader.h"
#include "llvm/IR/Verifier.h"
#include "llvm/Support/SourceMgr.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h"
//#include "llvm/ExecutionEngine/ExecutionEngine.h"
#include "llvm/ExecutionEngine/MCJIT.h"
#include "llvm/ExecutionEngine/JIT.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/LLVMContext.h"

//#include "PMFGen.h"


struct Error {
	double err = 0;
	double prob = 0;
	std::map<int64_t, double> pmf;
};
	// split() taken from:
	// http://stackoverflow.com/questions/236129/splitting-a-string-in-c
std::vector<std::string> &split(const std::string &s, char delim,
		std::vector<std::string> &elems);
std::vector<std::string> split(const std::string &s, char delim);

std::unique_ptr<llvm::Module> readIR(std::string name);

std::unique_ptr<llvm::ExecutionEngine> getExecutionEngine(llvm::Module *m, bool useInter = false);
llvm::Function* getFunction(llvm::Module &m, std::string functionName);

double computeTotalMean(std::map<int64_t, double> &pmf);
double computeErrorMean(std::map<int64_t, double> &pmf);

	//taken from http://stackoverflow.com/questions/16388510/evaluate-a-string-with-a-switch-in-c
constexpr unsigned int str2int(const char* str, int h = 0) {
		return !str[h] ? 5381 : (str2int(str, h + 1) * 33) ^ str[h];
	}

	//static void generatePMF(llvm::Instruction *instr/*std::shared_ptr<llvm::Instruction> &instr*/, Error &err, int ErrType);
#endif
