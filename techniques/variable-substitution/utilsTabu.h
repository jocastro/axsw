#ifndef UTILS_H
#define UTILS_H

#include <vector>
#include <sstream>
#include "ApproxUnit.h"
#include "PMFGen.h"
#include <cmath>



//from http://stackoverflow.com/questions/20953390/what-is-the-fastest-hash-function-for-pointers
template<typename Tval>
struct MyTemplatePointerHash1 {
	size_t operator()(const Tval* val) const {
		static const size_t shift = (size_t)log2(1 + sizeof(Tval));
		return (size_t)(val) >> shift;
	}
};

//http://stackoverflow.com/questions/2590677/how-do-i-combine-hash-values-in-c0x
template<class T>
inline void hash_combine(size_t& seed, const T& v) {
	std::hash<T> hasher;
	seed ^= hasher(v) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
}

/*static std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
	std::stringstream ss(s);
	std::string item;
	while (std::getline(ss, item, delim)) {
		elems.push_back(item);
	}
	return elems;
}*/

/*static std::vector<std::string> split(const std::string &s, char delim) {
	std::vector<std::string> elems;
	split(s, delim, elems);
	return elems;
}*/

//taken from http://stackoverflow.com/questions/16388510/evaluate-a-string-with-a-switch-in-c
/*constexpr unsigned int str2int(const char* str, int h = 0) {
	return !str[h] ? 5381 : (str2int(str, h + 1) * 33) ^ str[h];
}*/

/*static double computeTotalMean(std::map<int64_t, double> &pmf) {
	double sum = 0;
	for (std::map<int64_t, double>::iterator it = pmf.begin();
		it != pmf.end(); ++it) {
		sum += std::abs(it->first) * it->second;
	}
	return sum;
}

static double computeErrorMean(std::map<int64_t, double> &pmf) {
	double errorProb = 1 - pmf[0];
	double sum = 0;
	for (std::map<int64_t, double>::iterator it = pmf.begin()++;
		it != pmf.end(); ++it) {
		sum += std::abs(it->first) * it->second * errorProb;
	}
	return sum;
}*/

static double computeProbableMaxError(std::map<int64_t, double> &pmf) {
	std::map<int64_t, double> absPMF;
	for (std::map<int64_t, double>::iterator it = pmf.begin()++;
		it != pmf.end();  ++it) {
		absPMF[std::abs(it->first)] += it->second;
	}

	std::map<double, std::set<int64_t> > mpf;
	for (std::map<int64_t, double>::iterator it = absPMF.begin()++;
		it != absPMF.end();  ++it) {
		if (it->first == 0)
			continue;
		double prob = round(it->second * 100);
		mpf[prob].insert(it->first);
	}
	if (mpf.empty())
		return 0;
	std::map<double, std::set<int64_t> >::iterator bp = mpf.end();
	bp--;
	std::set<int64_t>::iterator value = bp->second.end();	
	value--;
	double err = *value * (1 - pmf[0]);
	return err;
}

#endif // !UTILS_H

